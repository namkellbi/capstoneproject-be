USE [ClinicManagement]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[AccountId] [int] NOT NULL,
	[UserName] [nchar](30) NULL,
	[Password] [nchar](100) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branch](
	[BranchId] [int] IDENTITY(1,1) NOT NULL,
	[BranchName] [nvarchar](max) NULL,
	[Address] [nvarchar](max) NOT NULL,
	[PhoneNumber] [nchar](50) NULL,
	[IsAction] [bit] NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ConsultingRoom]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ConsultingRoom](
	[ConsultingRoomId] [int] IDENTITY(1,1) NOT NULL,
	[RoomName] [nvarchar](100) NULL,
	[AbbreviationName] [nvarchar](50) NULL,
	[BranchId] [int] NULL,
	[IsAction] [bit] NULL,
 CONSTRAINT [PK_ConsultingRoom] PRIMARY KEY CLUSTERED 
(
	[ConsultingRoomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Contact]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Contact](
	[AccountId] [int] IDENTITY(1,1) NOT NULL,
	[FullName] [nvarchar](50) NULL,
	[Dob] [date] NULL,
	[Address] [nvarchar](200) NOT NULL,
	[Village] [nvarchar](200) NULL,
	[District] [nvarchar](200) NULL,
	[Province] [nvarchar](200) NULL,
	[Sex] [nvarchar](10) NULL,
	[IdentityCard] [nvarchar](50) NULL,
	[PhoneNumber] [nvarchar](50) NULL,
	[Ethnicity] [nvarchar](50) NULL,
	[Job] [nvarchar](200) NULL,
	[Email] [nvarchar](50) NULL,
	[Status] [bit] NULL,
	[RoleId] [int] NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Create_Receipt_Product]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Create_Receipt_Product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ReceiptId] [int] NOT NULL,
	[personInChargeId] [int] NULL,
	[dateReceipt] [date] NULL,
 CONSTRAINT [PK_Create_Receipt_Product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory](
	[InventoryId] [int] IDENTITY(1,1) NOT NULL,
	[PersonInCharge] [int] NULL,
	[petitioner] [int] NOT NULL,
	[StoreHouseId] [int] NOT NULL,
	[InventoryDate] [date] NULL,
	[Date] [date] NOT NULL,
	[StatusId] [int] NOT NULL,
 CONSTRAINT [PK_Inventory] PRIMARY KEY CLUSTERED 
(
	[InventoryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventory_Product]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory_Product](
	[InventoryId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[QuantityStock] [int] NULL,
	[ActualQuantity] [int] NULL,
	[Deviant] [int] NULL,
	[Note] [nvarchar](max) NULL,
	[AdjustmentAmount] [int] NULL,
 CONSTRAINT [PK_Inventory_Product] PRIMARY KEY CLUSTERED 
(
	[InventoryId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Invoice]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Invoice](
	[InvoiceId] [int] IDENTITY(1,1) NOT NULL,
	[StaffId] [int] NOT NULL,
	[MedicalReportId] [int] NULL,
	[TotalCost] [money] NOT NULL,
	[Date] [date] NOT NULL,
	[BranchId] [int] NULL,
	[Status] [bit] NULL,
 CONSTRAINT [PK_Invoice] PRIMARY KEY CLUSTERED 
(
	[InvoiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InvoiceDetail]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetail](
	[InvoiceDetailId] [int] IDENTITY(1,1) NOT NULL,
	[InvoiceId] [int] NOT NULL,
	[InvoiceTypeId] [int] NOT NULL,
 CONSTRAINT [PK_InvoiceDetail] PRIMARY KEY CLUSTERED 
(
	[InvoiceDetailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InvoiceDetail_Product]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetail_Product](
	[InvoiceDetailId] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
	[Amount] [int] NOT NULL,
 CONSTRAINT [PK_InvoiceDetail_Product] PRIMARY KEY CLUSTERED 
(
	[InvoiceDetailId] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InvoiceType]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceType](
	[InvoiceTypeId] [int] NOT NULL,
	[TaxCode] [float] NULL,
	[ProductTypeId] [int] NULL,
 CONSTRAINT [PK_InvoiceType] PRIMARY KEY CLUSTERED 
(
	[InvoiceTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Issue_Product]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Issue_Product](
	[IssueId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[NumberOfRequests] [int] NULL,
	[Amount] [int] NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_Issue_Product] PRIMARY KEY CLUSTERED 
(
	[IssueId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedicalRecord]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalRecord](
	[MedicalRecordId] [int] IDENTITY(1,1) NOT NULL,
	[AccountId] [int] NULL,
	[Circuit] [float] NULL,
	[BloodPressure] [float] NULL,
	[BMI] [float] NULL,
	[Height] [float] NULL,
	[Temperature] [float] NULL,
	[Weight] [float] NULL,
	[PastMedicalHistory] [nvarchar](max) NULL,
	[Allergies] [nvarchar](max) NULL,
	[Note] [nvarchar](max) NULL,
	[ICD10] [nvarchar](max) NULL,
	[ICD] [nvarchar](max) NULL,
	[Advice] [nvarchar](max) NULL,
	[Date] [date] NULL,
 CONSTRAINT [PK_MedicalRecord] PRIMARY KEY CLUSTERED 
(
	[MedicalRecordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedicalReport]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalReport](
	[MedicalReportId] [int] IDENTITY(1,1) NOT NULL,
	[MedicalRecordId] [int] NULL,
	[Date] [date] NULL,
	[AccountId] [int] NULL,
	[IsFinishedExamination] [bit] NULL,
 CONSTRAINT [PK_MedicalReport] PRIMARY KEY CLUSTERED 
(
	[MedicalReportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedicalReport_Consulting]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalReport_Consulting](
	[STT] [int] NULL,
	[MedicalReportId] [int] NOT NULL,
	[ConsultingRoomId] [int] NOT NULL,
	[MedicalServiceId] [int] NULL,
	[StatusId] [int] NULL,
 CONSTRAINT [PK_MedicalReport_Consulting] PRIMARY KEY CLUSTERED 
(
	[MedicalReportId] ASC,
	[ConsultingRoomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedicalReport_Service]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalReport_Service](
	[ReportServiceId] [int] IDENTITY(1,1) NOT NULL,
	[MedicalReportId] [int] NOT NULL,
	[MedicalServiceId] [int] NOT NULL,
	[TotalCost] [money] NULL,
	[InvoiceDetailId] [int] NULL,
	[Ispay] [bit] NULL,
	[IsUse] [bit] NULL,
 CONSTRAINT [PK_MedicalReport_Service] PRIMARY KEY CLUSTERED 
(
	[ReportServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedicalService]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalService](
	[MedicalServiceId] [int] IDENTITY(1,1) NOT NULL,
	[ConsultingRoomId] [int] NULL,
	[ServiceName] [nvarchar](100) NULL,
	[Price] [money] NULL,
 CONSTRAINT [PK_MedicalService] PRIMARY KEY CLUSTERED 
(
	[MedicalServiceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductID] [int] IDENTITY(1,1) NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[Price] [money] NULL,
	[UnitId] [int] NOT NULL,
	[Note] [nvarchar](100) NULL,
	[UserObject] [nvarchar](max) NULL,
	[Using] [nvarchar](max) NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductGroup]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductGroup](
	[ProductGroupId] [int] IDENTITY(1,1) NOT NULL,
	[ProductGroupName] [nvarchar](50) NULL,
	[ProductTypeID] [int] NULL,
	[IsUse] [bit] NULL,
 CONSTRAINT [PK_ProductGroup] PRIMARY KEY CLUSTERED 
(
	[ProductGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductGroup_Product]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductGroup_Product](
	[ProductId] [int] NOT NULL,
	[ProductGroupId] [int] NOT NULL,
 CONSTRAINT [PK_ProductGroup_Product] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[ProductGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductType]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductType](
	[ProductTypeId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_ProductType] PRIMARY KEY CLUSTERED 
(
	[ProductTypeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Receipt_Product]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Receipt_Product](
	[ReceiptId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[NumberOfRequests] [int] NULL,
	[EntryPrice] [money] NULL,
 CONSTRAINT [PK_Receipt_Product_1] PRIMARY KEY CLUSTERED 
(
	[ReceiptId] ASC,
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Receipt_product_2]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Receipt_product_2](
	[ProductId] [int] NOT NULL,
	[id] [int] NOT NULL,
	[Amount] [int] NULL,
	[Note] [nvarchar](max) NULL,
 CONSTRAINT [PK_Receipt_product_2] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC,
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReceiptAndIssue]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReceiptAndIssue](
	[ReceiptIssueId] [int] IDENTITY(1,1) NOT NULL,
	[WarehouseTransferID] [int] NULL,
	[ReceivingWarehouseID] [int] NULL,
	[petitioner] [int] NULL,
	[PersonInCharge] [int] NULL,
	[TotalCost] [money] NULL,
	[ExpectedDate] [date] NULL,
	[Statusid] [int] NULL,
	[NOX] [bit] NULL,
	[SupplierId] [int] NULL,
	[Istranfer] [bit] NULL,
	[ExportDate] [date] NULL,
 CONSTRAINT [PK_ReceiptAndIssue] PRIMARY KEY CLUSTERED 
(
	[ReceiptIssueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](50) NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff](
	[StaffId] [int] NOT NULL,
	[Avatar] [nvarchar](200) NULL,
	[Education] [nvarchar](200) NULL,
	[Certificate] [nvarchar](200) NULL,
	[BranchId] [int] NULL,
 CONSTRAINT [PK_Staff] PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Staff_ConsultingRoom]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Staff_ConsultingRoom](
	[StaffId] [int] NOT NULL,
	[ConsultingRoomId] [int] NOT NULL,
	[Possition] [nvarchar](100) NULL,
 CONSTRAINT [PK_Staff_ConsultingRoom] PRIMARY KEY CLUSTERED 
(
	[StaffId] ASC,
	[ConsultingRoomId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Status]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Status](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](200) NULL,
 CONSTRAINT [PK_Status] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StoreHouse]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreHouse](
	[StoreHouseId] [int] IDENTITY(1,1) NOT NULL,
	[StoreHouseName] [nvarchar](200) NOT NULL,
	[AbbreviationName] [nchar](20) NOT NULL,
	[Address] [nvarchar](200) NOT NULL,
	[Location] [nvarchar](200) NULL,
	[Branchid] [int] NULL,
	[IsAction] [bit] NULL,
 CONSTRAINT [PK_StoreHouse] PRIMARY KEY CLUSTERED 
(
	[StoreHouseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StoreHouse_Product]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[StoreHouse_Product](
	[StoreHouseId] [int] NOT NULL,
	[ProductID] [int] NOT NULL,
 CONSTRAINT [PK_StoreHouse_Product] PRIMARY KEY CLUSTERED 
(
	[StoreHouseId] ASC,
	[ProductID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Supplier]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Supplier](
	[SupplierId] [int] IDENTITY(1,1) NOT NULL,
	[SupplierName] [nvarchar](100) NOT NULL,
	[AbbreviationName] [nchar](20) NOT NULL,
	[Address] [nvarchar](100) NOT NULL,
	[PhoneNumber] [nvarchar](50) NOT NULL,
	[IsUse] [bit] NULL,
	[mail] [nchar](20) NULL,
 CONSTRAINT [PK_Supplier] PRIMARY KEY CLUSTERED 
(
	[SupplierId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transfer]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transfer](
	[ReceiptId] [int] NOT NULL,
	[ExportId] [int] NOT NULL,
 CONSTRAINT [PK_Transfer] PRIMARY KEY CLUSTERED 
(
	[ReceiptId] ASC,
	[ExportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Unit]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unit](
	[UnitId] [int] IDENTITY(1,1) NOT NULL,
	[UnitName] [nvarchar](100) NOT NULL,
	[UnitGroupId] [int] NULL,
	[IsUse] [bit] NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[UnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UnitGroup]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UnitGroup](
	[UnitGroupId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsUse] [bit] NULL,
 CONSTRAINT [PK_UnitGroup] PRIMARY KEY CLUSTERED 
(
	[UnitGroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkSchedule]    Script Date: 02/02/2023 11:31:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkSchedule](
	[WorkScheduleId] [int] IDENTITY(1,1) NOT NULL,
	[Date] [date] NOT NULL,
	[Timestart] [datetime] NOT NULL,
	[Staffid] [int] NOT NULL,
	[TimeEnd] [datetime] NOT NULL,
	[ConsultingRoomId] [int] NOT NULL,
 CONSTRAINT [PK_WorkSchedule] PRIMARY KEY CLUSTERED 
(
	[WorkScheduleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (1, N'manager1                      ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (2, N'whstaff1                      ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (4, N'cashier1                      ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (17, N'doctorN                       ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (20, N'doctorQ                       ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (21, N'doctorA                       ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (27, N'ceo                           ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (28, N'minh                          ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (30, N'DoctorB                       ', N'123                                                                                                 ')
INSERT [dbo].[Account] ([AccountId], [UserName], [Password]) VALUES (32, N'manager2                      ', N'123                                                                                                 ')
GO
SET IDENTITY_INSERT [dbo].[Branch] ON 

INSERT [dbo].[Branch] ([BranchId], [BranchName], [Address], [PhoneNumber], [IsAction]) VALUES (1, N'Cơ Sở Tam Đàn', N'Hải Dương', N'0352102712                                        ', 1)
INSERT [dbo].[Branch] ([BranchId], [BranchName], [Address], [PhoneNumber], [IsAction]) VALUES (2, N'Cở Sở Tam Đàn 2', N'Hải Dương', N'0352102712                                        ', 1)
INSERT [dbo].[Branch] ([BranchId], [BranchName], [Address], [PhoneNumber], [IsAction]) VALUES (3, N'Cở Sở Đông Hải', N'Đông Hải Hải Dương', N'0352102712                                        ', 1)
SET IDENTITY_INSERT [dbo].[Branch] OFF
GO
SET IDENTITY_INSERT [dbo].[ConsultingRoom] ON 

INSERT [dbo].[ConsultingRoom] ([ConsultingRoomId], [RoomName], [AbbreviationName], [BranchId], [IsAction]) VALUES (1, N'Khám Nội', N'PKN', 1, 1)
INSERT [dbo].[ConsultingRoom] ([ConsultingRoomId], [RoomName], [AbbreviationName], [BranchId], [IsAction]) VALUES (2, N'Khám Ngoại', N'PKNg', 1, 1)
INSERT [dbo].[ConsultingRoom] ([ConsultingRoomId], [RoomName], [AbbreviationName], [BranchId], [IsAction]) VALUES (3, N'X Quang', N'PXQ', 1, 1)
INSERT [dbo].[ConsultingRoom] ([ConsultingRoomId], [RoomName], [AbbreviationName], [BranchId], [IsAction]) VALUES (4, N'Khám sản phụ', N'KSP', 1, 1)
INSERT [dbo].[ConsultingRoom] ([ConsultingRoomId], [RoomName], [AbbreviationName], [BranchId], [IsAction]) VALUES (5, N'Siêu Âm', N'PSA', 1, 1)
INSERT [dbo].[ConsultingRoom] ([ConsultingRoomId], [RoomName], [AbbreviationName], [BranchId], [IsAction]) VALUES (6, N'Khám Tổng Quát', N'KKTQ', 1, 1)
INSERT [dbo].[ConsultingRoom] ([ConsultingRoomId], [RoomName], [AbbreviationName], [BranchId], [IsAction]) VALUES (7, N'Nội Soi', N'PNS', 1, 1)
INSERT [dbo].[ConsultingRoom] ([ConsultingRoomId], [RoomName], [AbbreviationName], [BranchId], [IsAction]) VALUES (8, N'Xét Nghiệm', N'PXN', 1, 1)
SET IDENTITY_INSERT [dbo].[ConsultingRoom] OFF
GO
SET IDENTITY_INSERT [dbo].[Contact] ON 

INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (1, N'Nguyễn Tài Tuệ', CAST(N'2000-02-02' AS Date), N'Đội 5', N'Cấn Hữu ', N'Quốc Oai', N'Hà Nội', N'Nam', N'12312312322    ', N'0352012712                                        ', N'Kinh', N'Quản lý kho', N'nguyentaitue2000@gmail.com                        ', 1, 6)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (2, N'Nguyễn Đức Long', CAST(N'2000-02-02' AS Date), N'Đội 4 ', N'Tân Trào', N'Thường Tín ', N'Hà Nội', N'Nam', N'12312321322    ', N'0352102712                                        ', N'Kinh', N'Nhân Viên Kho', N'tuenthe141581@fpt.edu.vn                          ', 1, 5)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (4, N'Nguyễn Quang Vinh', CAST(N'2000-02-02' AS Date), N'Đội 3', N'Đình Tổ', N'Quốc Oai', N'Hà Nôi', N'Nam ', N'12312321312    ', N'0352102323                                        ', N'Kinh', N'Tiếp Tân', N'nguyentaitue2000@gmail.com                        ', 1, 4)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (15, N'Nguyễn Thị Huyền', CAST(N'2000-02-02' AS Date), N'Đội 4', N'Đĩnh Tú', N'Quốc Oai', N'Hà Nội', N'Nam', N'0352102712     ', N'0352102712                                        ', N'Kinh', N'Tự do', N'conanssc292@gmail.com                             ', 0, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (17, N'Nguyễn Quang Cường', CAST(N'2000-02-02' AS Date), N'Đội 5', N'Hoàng Xá ', N'Mỹ Đức ', N'Hà Nội', N'Nữ', N'02131231333    ', N'01235232313                                       ', N'Kinh', N'Bác sĩ', N'nguyentaitue2000@gmail.com                        ', 1, 3)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (18, N'Dương Văn Tuyên', CAST(N'2023-01-04' AS Date), N'Đội 4', N'Đĩnh Tú', N'Quốc Oai', N'Hà Nội', N'Nam', N'0352102712     ', N'12312312312                                       ', N'Kinh', N'Tự do', N'nhat@gmail.com                                    ', 1, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (19, N'Nguyễn Xuân Hòa', CAST(N'2023-01-04' AS Date), N'Đội 4', N'Đĩnh Tú', N'Quốc Oai', N'Hà Nội', N'Nam', N'0352102712     ', N'12343411211                                       ', N'Kinh', N'Tự do', N'nhat@gmail.com                                    ', 1, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (20, N'Nguyễn Anh Đức', CAST(N'2023-01-03' AS Date), N'Đội 5', N'Đĩnh Tú', N'Quốc Oai', N'Hà Nội', N'Nữ', N'0352102712     ', N'12343411211                                       ', N'Kinh', N'Tự do', N'nhat@gmail.com                                    ', 1, 3)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (21, N'Nguyễn Hoàng Anh', CAST(N'2023-01-03' AS Date), N'Đội 4', N'Đĩnh Tú', N'Quốc Oai', N'Hà Nội', N'Nữ', N'0352102712     ', N'12343411211                                       ', N'Kinh', N'Tự do', N'nhat@gmail.com                                    ', 1, 3)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (22, N'Nguyễn Tài Tuệ 1', CAST(N'2023-01-03' AS Date), N'Đội 4', N'Đĩnh Tú', N'Quốc Oai', N'Hà Nội', N'Nam', N'0352102712     ', N'0352102712                                        ', N'Kinh', N'Tự do', N'nguyentaitue2000@gmail.com                        ', 0, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (23, N'Hoàng minh minh', CAST(N'1999-10-09' AS Date), N'hoàng kê', N'thiên an', N'minh khoa', N'hà giang', N'nữ', N'634736367834   ', N'095776433                                         ', N'kinh', N'giáo vinh', N'minh@gmail.com                                    ', 1, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (24, N'Vũ Xuân Hoa', CAST(N'1987-10-02' AS Date), N'XCP5+H6G', N'Yên Quang', N'Thạch Thất', N'Hòa Bình', N'nữ', N'2834663787     ', N'0957333334                                        ', N'Kinh', N'nông dân', N'hoa@gmail.com                                     ', 0, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (25, N'Nguyễn Đức Hoa', CAST(N'2023-01-30' AS Date), N'Đội 4', N'Đĩnh Tú', N'Quốc Oai', N'Hà Nội', N'Nam', N'23131312223    ', N'0352102712                                        ', N'Kinh', N'Tự do', N'                                                  ', 1, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (26, N'Đào Thì Huyền', CAST(N'2023-01-29' AS Date), N'Đội 4', N'Đĩnh Tú', N'Quốc Oai', N'Hà Nội', N'Nữ', N'634736367834   ', N'0352102712                                        ', N'Kinh', N'Tự do', N'nguyentaitue2000@gmail.com                        ', 0, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (27, N'Phạm Ngọc Tuấn', CAST(N'1975-02-02' AS Date), N'Đồng Hoàng', N'Tứ Kì ', N'Minh Khai', N'Hải Dương', N'Nam', N'1231231232     ', N'123123123                                         ', N'Kinh', N'CEO', N'nguyentaitue2000@gmail.com                        ', 1, 2)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (28, N'Nguyễn bình minh', CAST(N'1988-12-01' AS Date), N'kinh liên', N'nam đàn', N'bình khoa', N'lạng sơn', N'nam', N'97474892734674 ', N'0947672238                                        ', N'mường', N'bác sĩ', N'MinhNB@gmail.com                                  ', 1, 3)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (29, N'vương hoàng minh', CAST(N'1988-05-09' AS Date), N'ĐT390', N'Tân An', N'Thanh Hà', N'Hải Dương', N'nam', N'476339988      ', N'09676744                                          ', N'hoa', N'công nhân', N'vuong@gmail.com                                   ', 0, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (30, N'Vương Minh Tú', CAST(N'1980-02-02' AS Date), N'V7RX+8RJ', N'Gia Xuyên', N'Gia Lộc', N'Hải Dương', N'Nam', N'958763988447   ', N'09686744                                          ', N'kinh', N'bác sĩ', N'TuVM@gmail.com                                    ', 1, 3)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (31, N'nguyễn huỳnh hoa', CAST(N'2009-03-04' AS Date), N'ĐT390', N'Đĩnh Tú', N'Quốc Oai', N'hà nội', N'nữ', N'               ', N'09847733                                          ', N'kinh', N'học sinh', NULL, 1, 1)
INSERT [dbo].[Contact] ([AccountId], [FullName], [Dob], [Address], [Village], [District], [Province], [Sex], [IdentityCard], [PhoneNumber], [Ethnicity], [Job], [Email], [Status], [RoleId]) VALUES (32, N'Tạ Thị Lân', CAST(N'2023-01-31' AS Date), N'504', N'Hoàng Mai', N'Hoàng Xá', N'Hà Nội', N'Nữ', N'1232131', N'0352102712', N'Kinh', N'Quản lý', N'nguyentaitue2000@gmail.com', 1, 6)
SET IDENTITY_INSERT [dbo].[Contact] OFF
GO
SET IDENTITY_INSERT [dbo].[Create_Receipt_Product] ON 

INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (1, 1, 2, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (2, 1, 2, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (3, 1, 2, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (4, 2, 2, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (5, 2, 2, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (6, 14, 2, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (7, 14, 2, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (8, 18, 2, CAST(N'2023-02-01' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (9, 18, 2, CAST(N'2023-02-01' AS Date))
INSERT [dbo].[Create_Receipt_Product] ([id], [ReceiptId], [personInChargeId], [dateReceipt]) VALUES (10, 26, 2, CAST(N'2023-02-02' AS Date))
SET IDENTITY_INSERT [dbo].[Create_Receipt_Product] OFF
GO
SET IDENTITY_INSERT [dbo].[Inventory] ON 

INSERT [dbo].[Inventory] ([InventoryId], [PersonInCharge], [petitioner], [StoreHouseId], [InventoryDate], [Date], [StatusId]) VALUES (1, 2, 1, 1, CAST(N'2023-01-24' AS Date), CAST(N'2023-02-01' AS Date), 2)
INSERT [dbo].[Inventory] ([InventoryId], [PersonInCharge], [petitioner], [StoreHouseId], [InventoryDate], [Date], [StatusId]) VALUES (2, 2, 1, 1, CAST(N'2023-01-24' AS Date), CAST(N'2023-02-01' AS Date), 2)
INSERT [dbo].[Inventory] ([InventoryId], [PersonInCharge], [petitioner], [StoreHouseId], [InventoryDate], [Date], [StatusId]) VALUES (3, 2, 1, 2, CAST(N'2023-02-24' AS Date), CAST(N'2023-02-07' AS Date), 2)
INSERT [dbo].[Inventory] ([InventoryId], [PersonInCharge], [petitioner], [StoreHouseId], [InventoryDate], [Date], [StatusId]) VALUES (4, NULL, 1, 1, NULL, CAST(N'2023-02-02' AS Date), 1)
SET IDENTITY_INSERT [dbo].[Inventory] OFF
GO
INSERT [dbo].[Inventory_Product] ([InventoryId], [ProductId], [QuantityStock], [ActualQuantity], [Deviant], [Note], [AdjustmentAmount]) VALUES (1, 1, 36, 12, 24, N'mất', 24)
INSERT [dbo].[Inventory_Product] ([InventoryId], [ProductId], [QuantityStock], [ActualQuantity], [Deviant], [Note], [AdjustmentAmount]) VALUES (2, 1, 1092, 1000, 92, N'bị hỏng', 92)
INSERT [dbo].[Inventory_Product] ([InventoryId], [ProductId], [QuantityStock], [ActualQuantity], [Deviant], [Note], [AdjustmentAmount]) VALUES (2, 3, 111, 111, 0, NULL, 0)
INSERT [dbo].[Inventory_Product] ([InventoryId], [ProductId], [QuantityStock], [ActualQuantity], [Deviant], [Note], [AdjustmentAmount]) VALUES (3, 3, 1500, 1500, 0, N'đủ', 0)
GO
SET IDENTITY_INSERT [dbo].[Invoice] ON 

INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (11, 4, 11, 27940000.0000, CAST(N'2023-01-31' AS Date), 1, 1)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (12, 4, 12, 0.0000, CAST(N'2023-01-31' AS Date), 1, 0)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (13, 4, 13, 0.0000, CAST(N'2023-01-31' AS Date), 1, 0)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (14, 4, 14, 27940000.0000, CAST(N'2023-01-31' AS Date), 1, 1)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (15, 4, 15, 0.0000, CAST(N'2023-02-01' AS Date), 1, 0)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (16, 4, 16, 27500000.0000, CAST(N'2023-02-01' AS Date), 1, 1)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (17, 4, 17, 0.0000, CAST(N'2023-02-01' AS Date), 1, 0)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (18, 4, 18, 0.0000, CAST(N'2023-02-01' AS Date), 1, 1)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (19, 4, 19, 27500000.0000, CAST(N'2023-02-02' AS Date), 1, 1)
INSERT [dbo].[Invoice] ([InvoiceId], [StaffId], [MedicalReportId], [TotalCost], [Date], [BranchId], [Status]) VALUES (20, 4, 20, 27500000.0000, CAST(N'2023-02-02' AS Date), 1, 0)
SET IDENTITY_INSERT [dbo].[Invoice] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetail] ON 

INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (11, 11, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (12, 12, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (13, 13, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (14, 14, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (15, 15, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (16, 16, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (17, 17, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (18, 18, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (19, 19, 1)
INSERT [dbo].[InvoiceDetail] ([InvoiceDetailId], [InvoiceId], [InvoiceTypeId]) VALUES (20, 20, 1)
SET IDENTITY_INSERT [dbo].[InvoiceDetail] OFF
GO
INSERT [dbo].[InvoiceType] ([InvoiceTypeId], [TaxCode], [ProductTypeId]) VALUES (1, 10, 1)
INSERT [dbo].[InvoiceType] ([InvoiceTypeId], [TaxCode], [ProductTypeId]) VALUES (2, 5, 2)
GO
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (3, 1, 4, 4, N'oke')
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (5, 2, 12, NULL, NULL)
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (7, 3, 23, 23, N'')
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (9, 1, 9, NULL, NULL)
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (11, 1, 8, NULL, NULL)
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (13, 1, 7, NULL, NULL)
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (15, 1, 120, 120, N'')
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (15, 3, 12, 12, N'')
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (17, 2, 100, NULL, NULL)
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (20, 1, 1222, NULL, NULL)
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (23, 6, 100, 100, N'')
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (24, 6, 1000, 100000, N'')
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (25, 3, 300, NULL, NULL)
INSERT [dbo].[Issue_Product] ([IssueId], [ProductId], [NumberOfRequests], [Amount], [Note]) VALUES (27, 7, 300, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[MedicalRecord] ON 

INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (11, 15, NULL, NULL, NULL, NULL, NULL, NULL, N'Không', N'Không', N'null', NULL, NULL, NULL, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (12, 18, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'undefined', NULL, NULL, NULL, NULL, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (13, 19, NULL, NULL, NULL, NULL, NULL, NULL, N'Không', N'Không', NULL, NULL, NULL, NULL, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (14, 22, NULL, NULL, NULL, NULL, NULL, NULL, N'Đau đầu', N'Phấn Hoa', N'null', NULL, NULL, NULL, CAST(N'2023-01-31' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (15, 23, NULL, NULL, NULL, NULL, NULL, NULL, N'không có', N'không có', N'null', NULL, NULL, NULL, CAST(N'2023-02-01' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (16, 24, NULL, NULL, NULL, NULL, NULL, NULL, N'không có', N'dị ứng phấn hoa', N'null', NULL, NULL, NULL, CAST(N'2023-02-01' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (17, 25, NULL, NULL, NULL, NULL, NULL, NULL, N'Không', N'Không', NULL, NULL, NULL, NULL, CAST(N'2023-02-01' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (18, 26, NULL, NULL, NULL, NULL, NULL, NULL, N'Không', N'Không', NULL, NULL, NULL, NULL, CAST(N'2023-02-01' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (19, 29, NULL, NULL, NULL, NULL, NULL, NULL, N'không có', N'không có', N'null', NULL, NULL, NULL, CAST(N'2023-02-02' AS Date))
INSERT [dbo].[MedicalRecord] ([MedicalRecordId], [AccountId], [Circuit], [BloodPressure], [BMI], [Height], [Temperature], [Weight], [PastMedicalHistory], [Allergies], [Note], [ICD10], [ICD], [Advice], [Date]) VALUES (20, 31, NULL, NULL, NULL, NULL, NULL, NULL, N'không có ', N'không có', N'null', NULL, NULL, NULL, CAST(N'2023-02-02' AS Date))
SET IDENTITY_INSERT [dbo].[MedicalRecord] OFF
GO
SET IDENTITY_INSERT [dbo].[MedicalReport] ON 

INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (11, 11, CAST(N'2023-01-31' AS Date), 15, 1)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (12, 12, CAST(N'2023-01-31' AS Date), 18, 0)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (13, 13, CAST(N'2023-01-31' AS Date), 19, 0)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (14, 14, CAST(N'2023-01-31' AS Date), 22, 1)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (15, 15, CAST(N'2023-02-01' AS Date), 23, 0)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (16, 16, CAST(N'2023-02-01' AS Date), 24, 1)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (17, 17, CAST(N'2023-02-01' AS Date), 25, 0)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (18, 18, CAST(N'2023-02-01' AS Date), 26, 1)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (19, 19, CAST(N'2023-02-02' AS Date), 29, 1)
INSERT [dbo].[MedicalReport] ([MedicalReportId], [MedicalRecordId], [Date], [AccountId], [IsFinishedExamination]) VALUES (20, 20, CAST(N'2023-02-02' AS Date), 31, 1)
SET IDENTITY_INSERT [dbo].[MedicalReport] OFF
GO
INSERT [dbo].[MedicalReport_Consulting] ([STT], [MedicalReportId], [ConsultingRoomId], [MedicalServiceId], [StatusId]) VALUES (1, 12, 2, 2, 5)
INSERT [dbo].[MedicalReport_Consulting] ([STT], [MedicalReportId], [ConsultingRoomId], [MedicalServiceId], [StatusId]) VALUES (2, 15, 2, 2, 6)
INSERT [dbo].[MedicalReport_Consulting] ([STT], [MedicalReportId], [ConsultingRoomId], [MedicalServiceId], [StatusId]) VALUES (1, 15, 6, 6, 5)
GO
SET IDENTITY_INSERT [dbo].[MedicalReport_Service] ON 

INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (2, 11, 2, 27500000.0000, 11, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (3, 12, 2, 27500000.0000, 12, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (4, 13, 3, 330000.0000, 13, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (5, 11, 4, 440000.0000, 11, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (6, 14, 2, 27500000.0000, 14, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (7, 14, 4, 440000.0000, 14, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (8, 15, 2, 27500000.0000, 15, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (9, 15, 6, 330000.0000, 15, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (10, 16, 2, 27500000.0000, 16, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (11, 17, 2, 27500000.0000, 17, 0, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (12, 18, 4, 440000.0000, 18, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (13, 19, 2, 27500000.0000, 19, 1, 0)
INSERT [dbo].[MedicalReport_Service] ([ReportServiceId], [MedicalReportId], [MedicalServiceId], [TotalCost], [InvoiceDetailId], [Ispay], [IsUse]) VALUES (14, 20, 1, 27500000.0000, 20, 1, 0)
SET IDENTITY_INSERT [dbo].[MedicalReport_Service] OFF
GO
SET IDENTITY_INSERT [dbo].[MedicalService] ON 

INSERT [dbo].[MedicalService] ([MedicalServiceId], [ConsultingRoomId], [ServiceName], [Price]) VALUES (1, 1, N'Khám Nội', 25000000.0000)
INSERT [dbo].[MedicalService] ([MedicalServiceId], [ConsultingRoomId], [ServiceName], [Price]) VALUES (2, 2, N'Khám Ngoại', 25000000.0000)
INSERT [dbo].[MedicalService] ([MedicalServiceId], [ConsultingRoomId], [ServiceName], [Price]) VALUES (3, 3, N'Chụp X-Quang Phổi', 300000.0000)
INSERT [dbo].[MedicalService] ([MedicalServiceId], [ConsultingRoomId], [ServiceName], [Price]) VALUES (4, 3, N'Chụp X-Quang Kĩ Thuật Số', 400000.0000)
INSERT [dbo].[MedicalService] ([MedicalServiceId], [ConsultingRoomId], [ServiceName], [Price]) VALUES (5, 4, N'Khám Sản Phụ Định Kỳ', 1000000.0000)
INSERT [dbo].[MedicalService] ([MedicalServiceId], [ConsultingRoomId], [ServiceName], [Price]) VALUES (6, 6, N'Khám Tổng Quát', 300000.0000)
INSERT [dbo].[MedicalService] ([MedicalServiceId], [ConsultingRoomId], [ServiceName], [Price]) VALUES (7, 7, N'Nội Soi Tai Mũi Họng', 1000000.0000)
SET IDENTITY_INSERT [dbo].[MedicalService] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (1, N'Thuốc paradol', 0.0000, 4, N'không dành cho trẻ nhỏ', N'dễ gây buồn ngủ', N'uống với nước sau ăn')
INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (2, N'Thuốc tiphi', 0.0000, 4, N'k dành cho trẻ nhỏ', N'dễ gây mệt mỏi', N'pha lẫn với nước')
INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (3, N'Thuốc C', 0.0000, 4, NULL, N'Không', N'Pha với nước ')
INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (4, N'Thuốc con nhộng', 0.0000, 4, N'không dành cho trẻ dưới 3 tháng tuổi', N'dễ gây buồn ngủ', N'uống trực tiếp')
INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (5, N'thuốc Amoxil', 0.0000, 4, N'không có', N'không có ', N'uống sau khiu ăn 30 phút')
INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (6, N'Augmentin ES-600', 0.0000, 4, N'không có', N'không có', N'uống trước khi ăn')
INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (7, N'thuốc Decolgen', 0.0000, 4, N'không có', N'không có ', N'uống sau khi ăn')
INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (8, N'thuộc Atussin', 0.0000, 4, N' không có', N' không có', N'uống')
INSERT [dbo].[Product] ([ProductID], [ProductName], [Price], [UnitId], [Note], [UserObject], [Using]) VALUES (9, N'thuốc Aspirin', 0.0000, 4, N'không có', N'không có ', N'uống')
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductGroup] ON 

INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (3, N'Thuốc kháng sinh', 2, 1)
INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (4, N'Thuốc cảm cúm', 2, 1)
INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (5, N'Thuốc thuốc trống viêm', 2, 1)
INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (6, N'Thuốc đau đầu', 2, 1)
INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (7, N'Thuốc ', 2, 0)
INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (8, N'Thuốc cảm', 2, 0)
INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (9, N'thuốc bổ', 2, 0)
INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (10, N'Thuốc mỡ', 2, 0)
INSERT [dbo].[ProductGroup] ([ProductGroupId], [ProductGroupName], [ProductTypeID], [IsUse]) VALUES (11, N'Thuốc bổ', 2, 1)
SET IDENTITY_INSERT [dbo].[ProductGroup] OFF
GO
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (1, 3)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (1, 6)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (2, 4)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (3, 9)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (4, 3)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (5, 3)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (6, 3)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (7, 4)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (8, 4)
INSERT [dbo].[ProductGroup_Product] ([ProductId], [ProductGroupId]) VALUES (9, 5)
GO
SET IDENTITY_INSERT [dbo].[ProductType] ON 

INSERT [dbo].[ProductType] ([ProductTypeId], [Name]) VALUES (1, N'Dịch vụ')
INSERT [dbo].[ProductType] ([ProductTypeId], [Name]) VALUES (2, N'Thuốc')
SET IDENTITY_INSERT [dbo].[ProductType] OFF
GO
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (1, 1, 40, 1400000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (1, 2, 14, 420000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (2, 3, 123, 27060000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (4, 2, 12, NULL)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (6, 3, 23, NULL)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (8, 1, 9, NULL)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (10, 1, 8, NULL)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (12, 1, 7, NULL)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (14, 1, 1200, 6000000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (14, 2, 1100, 4400000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (16, 2, 100, NULL)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (18, 3, 1500, 7500000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (19, 1, 1222, NULL)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (21, 1, 300, 900000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (21, 4, 400, 2000000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (22, 2, 300, 1500000.0000)
INSERT [dbo].[Receipt_Product] ([ReceiptId], [ProductId], [NumberOfRequests], [EntryPrice]) VALUES (26, 7, 300, NULL)
GO
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (1, 1, 1, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (1, 2, 2, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (1, 3, 37, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (1, 6, 120, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (1, 7, 1080, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (2, 1, 1, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (2, 2, 2, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (2, 3, 11, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (2, 6, 130, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (2, 7, 970, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (3, 4, 1, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (3, 5, 122, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (3, 8, 500, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (3, 9, 1000, NULL)
INSERT [dbo].[Receipt_product_2] ([ProductId], [id], [Amount], [Note]) VALUES (7, 10, 300, N'Đủ')
GO
SET IDENTITY_INSERT [dbo].[ReceiptAndIssue] ON 

INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (1, NULL, 1, 1, NULL, 61880000.0000, CAST(N'2023-01-04' AS Date), 2, 1, 2, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (2, NULL, 1, 1, NULL, 3328380000.0000, CAST(N'2023-01-03' AS Date), 2, 1, 2, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (3, NULL, 1, 17, 2, NULL, CAST(N'2023-01-03' AS Date), 2, 0, NULL, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (4, 2, 1, 2, NULL, NULL, CAST(N'2023-01-16' AS Date), 2, 1, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (5, 2, 1, 2, NULL, NULL, CAST(N'2023-01-16' AS Date), 1, 0, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (6, 2, 1, 2, NULL, NULL, CAST(N'2023-01-30' AS Date), 2, 1, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (7, 2, 1, 2, 2, NULL, CAST(N'2023-01-30' AS Date), 2, 0, NULL, 1, CAST(N'2023-01-30' AS Date))
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (8, 2, 1, 2, NULL, NULL, CAST(N'2023-01-30' AS Date), 2, 1, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (9, 2, 1, 2, NULL, NULL, CAST(N'2023-01-30' AS Date), 1, 0, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (10, 1, 2, 2, NULL, NULL, CAST(N'2023-01-30' AS Date), 2, 1, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (11, 1, 2, 2, NULL, NULL, CAST(N'2023-01-30' AS Date), 1, 0, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (12, 2, 1, 2, NULL, NULL, CAST(N'2023-01-30' AS Date), 2, 1, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (13, 2, 1, 2, NULL, NULL, CAST(N'2023-01-30' AS Date), 1, 0, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (14, NULL, 1, 1, NULL, 12040000000.0000, CAST(N'2023-01-04' AS Date), 2, 1, 2, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (15, NULL, 1, 17, 2, NULL, CAST(N'2023-01-11' AS Date), 2, 0, NULL, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (16, 2, 1, 2, NULL, NULL, CAST(N'2023-01-24' AS Date), 2, 1, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (17, 2, 1, 2, NULL, NULL, CAST(N'2023-01-24' AS Date), 1, 0, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (18, NULL, 2, 1, NULL, 11250000000.0000, CAST(N'2023-02-22' AS Date), 2, 1, 3, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (19, 3, 2, 1, NULL, NULL, CAST(N'2023-02-02' AS Date), 2, 1, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (20, 3, 2, 1, NULL, NULL, CAST(N'2023-02-02' AS Date), 1, 0, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (21, NULL, 1, 1, NULL, 1070000000.0000, CAST(N'2023-02-02' AS Date), 1, 1, 2, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (22, NULL, 2, 1, NULL, 450000000.0000, CAST(N'2023-02-02' AS Date), 1, 1, 2, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (23, NULL, 1, 21, 2, NULL, CAST(N'2023-02-02' AS Date), 2, 0, NULL, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (24, NULL, 4, 21, 2, NULL, CAST(N'2023-02-02' AS Date), 2, 0, NULL, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (25, NULL, NULL, 21, NULL, NULL, CAST(N'2023-02-07' AS Date), 1, 0, NULL, 0, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (26, 2, 1, 2, NULL, NULL, CAST(N'2023-02-03' AS Date), 2, 1, NULL, 1, NULL)
INSERT [dbo].[ReceiptAndIssue] ([ReceiptIssueId], [WarehouseTransferID], [ReceivingWarehouseID], [petitioner], [PersonInCharge], [TotalCost], [ExpectedDate], [Statusid], [NOX], [SupplierId], [Istranfer], [ExportDate]) VALUES (27, 2, 1, 2, NULL, NULL, CAST(N'2023-02-03' AS Date), 1, 0, NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[ReceiptAndIssue] OFF
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (1, N'Người Dùng')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (2, N'CEO')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (3, N'Bác Sĩ')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (4, N'Tiếp Tân')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (5, N'Nhân viên kho')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (6, N'Quản Lý')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (1, N'http://res.cloudinary.com/dn3loygj6/image/upload/v1674899670/y9yiwf0ibyc0ojqpgakc.jpg', NULL, NULL, 1)
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (2, N'http://res.cloudinary.com/dn3loygj6/image/upload/v1674899670/y9yiwf0ibyc0ojqpgakc.jpg', NULL, NULL, 1)
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (4, N'http://res.cloudinary.com/dn3loygj6/image/upload/v1674899670/y9yiwf0ibyc0ojqpgakc.jpg', NULL, NULL, 1)
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (17, N'http://res.cloudinary.com/dn3loygj6/image/upload/v1674899670/y9yiwf0ibyc0ojqpgakc.jpg', NULL, NULL, 1)
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (20, N'http://res.cloudinary.com/dn3loygj6/image/upload/v1674899670/y9yiwf0ibyc0ojqpgakc.jpg', NULL, NULL, 1)
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (21, N'http://res.cloudinary.com/dn3loygj6/image/upload/v1674899670/y9yiwf0ibyc0ojqpgakc.jpg', NULL, NULL, 1)
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (28, N'Vinhcute', N'Vinhdz', N'TueNgu', 1)
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (30, N'Vinhcute', N'Vinhdz', N'TueNgu', 1)
INSERT [dbo].[Staff] ([StaffId], [Avatar], [Education], [Certificate], [BranchId]) VALUES (32, NULL, NULL, NULL, 2)
GO
INSERT [dbo].[Staff_ConsultingRoom] ([StaffId], [ConsultingRoomId], [Possition]) VALUES (17, 2, N'Trưởng Phòng')
INSERT [dbo].[Staff_ConsultingRoom] ([StaffId], [ConsultingRoomId], [Possition]) VALUES (20, 3, N'Phó Phòng')
INSERT [dbo].[Staff_ConsultingRoom] ([StaffId], [ConsultingRoomId], [Possition]) VALUES (21, 6, N'Phó Phòng')
INSERT [dbo].[Staff_ConsultingRoom] ([StaffId], [ConsultingRoomId], [Possition]) VALUES (28, 1, N'bác sĩ')
INSERT [dbo].[Staff_ConsultingRoom] ([StaffId], [ConsultingRoomId], [Possition]) VALUES (30, 1, N'bác sĩ')
GO
SET IDENTITY_INSERT [dbo].[Status] ON 

INSERT [dbo].[Status] ([StatusID], [name]) VALUES (1, N'Sẵn Sàng')
INSERT [dbo].[Status] ([StatusID], [name]) VALUES (2, N'Hoàn Thành')
INSERT [dbo].[Status] ([StatusID], [name]) VALUES (3, N'Hủy Bỏ')
INSERT [dbo].[Status] ([StatusID], [name]) VALUES (4, N'Chờ ')
INSERT [dbo].[Status] ([StatusID], [name]) VALUES (5, N'Đang Khám')
INSERT [dbo].[Status] ([StatusID], [name]) VALUES (6, N'Chờ Lấy Kết Quả')
SET IDENTITY_INSERT [dbo].[Status] OFF
GO
SET IDENTITY_INSERT [dbo].[StoreHouse] ON 

INSERT [dbo].[StoreHouse] ([StoreHouseId], [StoreHouseName], [AbbreviationName], [Address], [Location], [Branchid], [IsAction]) VALUES (1, N'Kho Cộng Hòa', N'KCH                 ', N'Hải Dương', N'Bình Thạnh-Hải Dương', 1, 1)
INSERT [dbo].[StoreHouse] ([StoreHouseId], [StoreHouseName], [AbbreviationName], [Address], [Location], [Branchid], [IsAction]) VALUES (2, N'Kho Tân Phú', N'KTP                 ', N'Hải Dương', N'Tân Hòa', 1, 1)
INSERT [dbo].[StoreHouse] ([StoreHouseId], [StoreHouseName], [AbbreviationName], [Address], [Location], [Branchid], [IsAction]) VALUES (3, N'Kho Bão Hòa', N'KBH                 ', N'Hải Dương', N'Bão Hòa', 2, 1)
INSERT [dbo].[StoreHouse] ([StoreHouseId], [StoreHouseName], [AbbreviationName], [Address], [Location], [Branchid], [IsAction]) VALUES (4, N'Kho Đông Hải', N'KDH                 ', N'Tứ Kì', N'Hải Dương', 1, 1)
SET IDENTITY_INSERT [dbo].[StoreHouse] OFF
GO
INSERT [dbo].[StoreHouse_Product] ([StoreHouseId], [ProductID]) VALUES (1, 1)
INSERT [dbo].[StoreHouse_Product] ([StoreHouseId], [ProductID]) VALUES (1, 2)
INSERT [dbo].[StoreHouse_Product] ([StoreHouseId], [ProductID]) VALUES (1, 3)
INSERT [dbo].[StoreHouse_Product] ([StoreHouseId], [ProductID]) VALUES (1, 7)
INSERT [dbo].[StoreHouse_Product] ([StoreHouseId], [ProductID]) VALUES (2, 3)
GO
SET IDENTITY_INSERT [dbo].[Supplier] ON 

INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [AbbreviationName], [Address], [PhoneNumber], [IsUse], [mail]) VALUES (2, N'Nhà Cung Cấp Đại Thành', N'NCCDT               ', N'Hải Dương', N'0352102712', 1, N'nguyentaitue        ')
INSERT [dbo].[Supplier] ([SupplierId], [SupplierName], [AbbreviationName], [Address], [PhoneNumber], [IsUse], [mail]) VALUES (3, N'Nhà Cung Cấp Tân Phú', N'NCCTP               ', N'Hải Dương', N'0384381739', 1, N'nguyenanhduc        ')
SET IDENTITY_INSERT [dbo].[Supplier] OFF
GO
INSERT [dbo].[Transfer] ([ReceiptId], [ExportId]) VALUES (4, 5)
INSERT [dbo].[Transfer] ([ReceiptId], [ExportId]) VALUES (6, 7)
INSERT [dbo].[Transfer] ([ReceiptId], [ExportId]) VALUES (8, 9)
INSERT [dbo].[Transfer] ([ReceiptId], [ExportId]) VALUES (10, 11)
INSERT [dbo].[Transfer] ([ReceiptId], [ExportId]) VALUES (12, 13)
INSERT [dbo].[Transfer] ([ReceiptId], [ExportId]) VALUES (16, 17)
INSERT [dbo].[Transfer] ([ReceiptId], [ExportId]) VALUES (19, 20)
INSERT [dbo].[Transfer] ([ReceiptId], [ExportId]) VALUES (26, 27)
GO
SET IDENTITY_INSERT [dbo].[Unit] ON 

INSERT [dbo].[Unit] ([UnitId], [UnitName], [UnitGroupId], [IsUse]) VALUES (1, N'lít', 1, 1)
INSERT [dbo].[Unit] ([UnitId], [UnitName], [UnitGroupId], [IsUse]) VALUES (2, N'g', 2, 1)
INSERT [dbo].[Unit] ([UnitId], [UnitName], [UnitGroupId], [IsUse]) VALUES (3, N'kg', 2, 1)
INSERT [dbo].[Unit] ([UnitId], [UnitName], [UnitGroupId], [IsUse]) VALUES (4, N'vỉ', 3, 1)
SET IDENTITY_INSERT [dbo].[Unit] OFF
GO
SET IDENTITY_INSERT [dbo].[UnitGroup] ON 

INSERT [dbo].[UnitGroup] ([UnitGroupId], [Name], [IsUse]) VALUES (1, N'Thể Tích', 1)
INSERT [dbo].[UnitGroup] ([UnitGroupId], [Name], [IsUse]) VALUES (2, N'Khối lượng', 1)
INSERT [dbo].[UnitGroup] ([UnitGroupId], [Name], [IsUse]) VALUES (3, N'Hộp', 1)
INSERT [dbo].[UnitGroup] ([UnitGroupId], [Name], [IsUse]) VALUES (4, N'abc', 0)
SET IDENTITY_INSERT [dbo].[UnitGroup] OFF
GO
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Contact] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Contact] ([AccountId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Contact]
GO
ALTER TABLE [dbo].[ConsultingRoom]  WITH CHECK ADD  CONSTRAINT [FK_ConsultingRoom_Branch] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branch] ([BranchId])
GO
ALTER TABLE [dbo].[ConsultingRoom] CHECK CONSTRAINT [FK_ConsultingRoom_Branch]
GO
ALTER TABLE [dbo].[Contact]  WITH CHECK ADD  CONSTRAINT [FK_Contact_Role] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Role] ([RoleID])
GO
ALTER TABLE [dbo].[Contact] CHECK CONSTRAINT [FK_Contact_Role]
GO
ALTER TABLE [dbo].[Create_Receipt_Product]  WITH CHECK ADD  CONSTRAINT [FK_Create_Receipt_Product_ReceiptAndIssue] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[ReceiptAndIssue] ([ReceiptIssueId])
GO
ALTER TABLE [dbo].[Create_Receipt_Product] CHECK CONSTRAINT [FK_Create_Receipt_Product_ReceiptAndIssue]
GO
ALTER TABLE [dbo].[Create_Receipt_Product]  WITH CHECK ADD  CONSTRAINT [FK_Create_Receipt_Product_Staff] FOREIGN KEY([personInChargeId])
REFERENCES [dbo].[Staff] ([StaffId])
GO
ALTER TABLE [dbo].[Create_Receipt_Product] CHECK CONSTRAINT [FK_Create_Receipt_Product_Staff]
GO
ALTER TABLE [dbo].[Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Staff] FOREIGN KEY([PersonInCharge])
REFERENCES [dbo].[Staff] ([StaffId])
GO
ALTER TABLE [dbo].[Inventory] CHECK CONSTRAINT [FK_Inventory_Staff]
GO
ALTER TABLE [dbo].[Inventory]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Staff1] FOREIGN KEY([petitioner])
REFERENCES [dbo].[Staff] ([StaffId])
GO
ALTER TABLE [dbo].[Inventory] CHECK CONSTRAINT [FK_Inventory_Staff1]
GO
ALTER TABLE [dbo].[Inventory_Product]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Product_Inventory] FOREIGN KEY([InventoryId])
REFERENCES [dbo].[Inventory] ([InventoryId])
GO
ALTER TABLE [dbo].[Inventory_Product] CHECK CONSTRAINT [FK_Inventory_Product_Inventory]
GO
ALTER TABLE [dbo].[Inventory_Product]  WITH CHECK ADD  CONSTRAINT [FK_Inventory_Product_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[Inventory_Product] CHECK CONSTRAINT [FK_Inventory_Product_Product]
GO
ALTER TABLE [dbo].[Invoice]  WITH CHECK ADD  CONSTRAINT [FK_Invoice_MedicalReport] FOREIGN KEY([MedicalReportId])
REFERENCES [dbo].[MedicalReport] ([MedicalReportId])
GO
ALTER TABLE [dbo].[Invoice] CHECK CONSTRAINT [FK_Invoice_MedicalReport]
GO
ALTER TABLE [dbo].[InvoiceDetail]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetail_Invoice] FOREIGN KEY([InvoiceId])
REFERENCES [dbo].[Invoice] ([InvoiceId])
GO
ALTER TABLE [dbo].[InvoiceDetail] CHECK CONSTRAINT [FK_InvoiceDetail_Invoice]
GO
ALTER TABLE [dbo].[InvoiceDetail]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetail_InvoiceType] FOREIGN KEY([InvoiceTypeId])
REFERENCES [dbo].[InvoiceType] ([InvoiceTypeId])
GO
ALTER TABLE [dbo].[InvoiceDetail] CHECK CONSTRAINT [FK_InvoiceDetail_InvoiceType]
GO
ALTER TABLE [dbo].[InvoiceDetail_Product]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetail_Product_InvoiceDetail] FOREIGN KEY([InvoiceDetailId])
REFERENCES [dbo].[InvoiceDetail] ([InvoiceDetailId])
GO
ALTER TABLE [dbo].[InvoiceDetail_Product] CHECK CONSTRAINT [FK_InvoiceDetail_Product_InvoiceDetail]
GO
ALTER TABLE [dbo].[InvoiceDetail_Product]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceDetail_Product_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[InvoiceDetail_Product] CHECK CONSTRAINT [FK_InvoiceDetail_Product_Product]
GO
ALTER TABLE [dbo].[InvoiceType]  WITH CHECK ADD  CONSTRAINT [FK_InvoiceType_ProductType] FOREIGN KEY([ProductTypeId])
REFERENCES [dbo].[ProductType] ([ProductTypeId])
GO
ALTER TABLE [dbo].[InvoiceType] CHECK CONSTRAINT [FK_InvoiceType_ProductType]
GO
ALTER TABLE [dbo].[Issue_Product]  WITH CHECK ADD  CONSTRAINT [FK_Issue_Product_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[Issue_Product] CHECK CONSTRAINT [FK_Issue_Product_Product]
GO
ALTER TABLE [dbo].[Issue_Product]  WITH CHECK ADD  CONSTRAINT [FK_Issue_Product_ReceiptAndIssue] FOREIGN KEY([IssueId])
REFERENCES [dbo].[ReceiptAndIssue] ([ReceiptIssueId])
GO
ALTER TABLE [dbo].[Issue_Product] CHECK CONSTRAINT [FK_Issue_Product_ReceiptAndIssue]
GO
ALTER TABLE [dbo].[MedicalRecord]  WITH CHECK ADD  CONSTRAINT [FK_MedicalRecord_Contact] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Contact] ([AccountId])
GO
ALTER TABLE [dbo].[MedicalRecord] CHECK CONSTRAINT [FK_MedicalRecord_Contact]
GO
ALTER TABLE [dbo].[MedicalReport]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_Contact] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Contact] ([AccountId])
GO
ALTER TABLE [dbo].[MedicalReport] CHECK CONSTRAINT [FK_MedicalReport_Contact]
GO
ALTER TABLE [dbo].[MedicalReport]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_MedicalRecord] FOREIGN KEY([MedicalRecordId])
REFERENCES [dbo].[MedicalRecord] ([MedicalRecordId])
GO
ALTER TABLE [dbo].[MedicalReport] CHECK CONSTRAINT [FK_MedicalReport_MedicalRecord]
GO
ALTER TABLE [dbo].[MedicalReport_Consulting]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_Consulting_ConsultingRoom] FOREIGN KEY([ConsultingRoomId])
REFERENCES [dbo].[ConsultingRoom] ([ConsultingRoomId])
GO
ALTER TABLE [dbo].[MedicalReport_Consulting] CHECK CONSTRAINT [FK_MedicalReport_Consulting_ConsultingRoom]
GO
ALTER TABLE [dbo].[MedicalReport_Consulting]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_Consulting_MedicalReport] FOREIGN KEY([MedicalReportId])
REFERENCES [dbo].[MedicalReport] ([MedicalReportId])
GO
ALTER TABLE [dbo].[MedicalReport_Consulting] CHECK CONSTRAINT [FK_MedicalReport_Consulting_MedicalReport]
GO
ALTER TABLE [dbo].[MedicalReport_Consulting]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_Consulting_MedicalService] FOREIGN KEY([MedicalServiceId])
REFERENCES [dbo].[MedicalService] ([MedicalServiceId])
GO
ALTER TABLE [dbo].[MedicalReport_Consulting] CHECK CONSTRAINT [FK_MedicalReport_Consulting_MedicalService]
GO
ALTER TABLE [dbo].[MedicalReport_Consulting]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_Consulting_Status] FOREIGN KEY([StatusId])
REFERENCES [dbo].[Status] ([StatusID])
GO
ALTER TABLE [dbo].[MedicalReport_Consulting] CHECK CONSTRAINT [FK_MedicalReport_Consulting_Status]
GO
ALTER TABLE [dbo].[MedicalReport_Service]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_Service_InvoiceDetail] FOREIGN KEY([InvoiceDetailId])
REFERENCES [dbo].[InvoiceDetail] ([InvoiceDetailId])
GO
ALTER TABLE [dbo].[MedicalReport_Service] CHECK CONSTRAINT [FK_MedicalReport_Service_InvoiceDetail]
GO
ALTER TABLE [dbo].[MedicalReport_Service]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_Service_MedicalReport] FOREIGN KEY([MedicalReportId])
REFERENCES [dbo].[MedicalReport] ([MedicalReportId])
GO
ALTER TABLE [dbo].[MedicalReport_Service] CHECK CONSTRAINT [FK_MedicalReport_Service_MedicalReport]
GO
ALTER TABLE [dbo].[MedicalReport_Service]  WITH CHECK ADD  CONSTRAINT [FK_MedicalReport_Service_MedicalService] FOREIGN KEY([MedicalServiceId])
REFERENCES [dbo].[MedicalService] ([MedicalServiceId])
GO
ALTER TABLE [dbo].[MedicalReport_Service] CHECK CONSTRAINT [FK_MedicalReport_Service_MedicalService]
GO
ALTER TABLE [dbo].[MedicalService]  WITH CHECK ADD  CONSTRAINT [FK_MedicalService_ConsultingRoom] FOREIGN KEY([ConsultingRoomId])
REFERENCES [dbo].[ConsultingRoom] ([ConsultingRoomId])
GO
ALTER TABLE [dbo].[MedicalService] CHECK CONSTRAINT [FK_MedicalService_ConsultingRoom]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Unit] FOREIGN KEY([UnitId])
REFERENCES [dbo].[Unit] ([UnitId])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Unit]
GO
ALTER TABLE [dbo].[ProductGroup]  WITH CHECK ADD  CONSTRAINT [FK_ProductGroup_ProductType] FOREIGN KEY([ProductTypeID])
REFERENCES [dbo].[ProductType] ([ProductTypeId])
GO
ALTER TABLE [dbo].[ProductGroup] CHECK CONSTRAINT [FK_ProductGroup_ProductType]
GO
ALTER TABLE [dbo].[ProductGroup_Product]  WITH CHECK ADD  CONSTRAINT [FK_ProductGroup_Product_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[ProductGroup_Product] CHECK CONSTRAINT [FK_ProductGroup_Product_Product]
GO
ALTER TABLE [dbo].[ProductGroup_Product]  WITH CHECK ADD  CONSTRAINT [FK_ProductGroup_Product_ProductGroup] FOREIGN KEY([ProductGroupId])
REFERENCES [dbo].[ProductGroup] ([ProductGroupId])
GO
ALTER TABLE [dbo].[ProductGroup_Product] CHECK CONSTRAINT [FK_ProductGroup_Product_ProductGroup]
GO
ALTER TABLE [dbo].[Receipt_Product]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_Product_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[Receipt_Product] CHECK CONSTRAINT [FK_Receipt_Product_Product]
GO
ALTER TABLE [dbo].[Receipt_Product]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_Product_ReceiptAndIssue] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[ReceiptAndIssue] ([ReceiptIssueId])
GO
ALTER TABLE [dbo].[Receipt_Product] CHECK CONSTRAINT [FK_Receipt_Product_ReceiptAndIssue]
GO
ALTER TABLE [dbo].[Receipt_product_2]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_product_2_Create_Receipt_Product] FOREIGN KEY([id])
REFERENCES [dbo].[Create_Receipt_Product] ([id])
GO
ALTER TABLE [dbo].[Receipt_product_2] CHECK CONSTRAINT [FK_Receipt_product_2_Create_Receipt_Product]
GO
ALTER TABLE [dbo].[Receipt_product_2]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_product_2_Product] FOREIGN KEY([ProductId])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[Receipt_product_2] CHECK CONSTRAINT [FK_Receipt_product_2_Product]
GO
ALTER TABLE [dbo].[ReceiptAndIssue]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptAndIssue_Staff] FOREIGN KEY([petitioner])
REFERENCES [dbo].[Staff] ([StaffId])
GO
ALTER TABLE [dbo].[ReceiptAndIssue] CHECK CONSTRAINT [FK_ReceiptAndIssue_Staff]
GO
ALTER TABLE [dbo].[ReceiptAndIssue]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptAndIssue_Staff1] FOREIGN KEY([PersonInCharge])
REFERENCES [dbo].[Staff] ([StaffId])
GO
ALTER TABLE [dbo].[ReceiptAndIssue] CHECK CONSTRAINT [FK_ReceiptAndIssue_Staff1]
GO
ALTER TABLE [dbo].[ReceiptAndIssue]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptAndIssue_Status] FOREIGN KEY([Statusid])
REFERENCES [dbo].[Status] ([StatusID])
GO
ALTER TABLE [dbo].[ReceiptAndIssue] CHECK CONSTRAINT [FK_ReceiptAndIssue_Status]
GO
ALTER TABLE [dbo].[ReceiptAndIssue]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptAndIssue_StoreHouse] FOREIGN KEY([WarehouseTransferID])
REFERENCES [dbo].[StoreHouse] ([StoreHouseId])
GO
ALTER TABLE [dbo].[ReceiptAndIssue] CHECK CONSTRAINT [FK_ReceiptAndIssue_StoreHouse]
GO
ALTER TABLE [dbo].[ReceiptAndIssue]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptAndIssue_StoreHouse1] FOREIGN KEY([ReceivingWarehouseID])
REFERENCES [dbo].[StoreHouse] ([StoreHouseId])
GO
ALTER TABLE [dbo].[ReceiptAndIssue] CHECK CONSTRAINT [FK_ReceiptAndIssue_StoreHouse1]
GO
ALTER TABLE [dbo].[ReceiptAndIssue]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptAndIssue_Supplier] FOREIGN KEY([SupplierId])
REFERENCES [dbo].[Supplier] ([SupplierId])
GO
ALTER TABLE [dbo].[ReceiptAndIssue] CHECK CONSTRAINT [FK_ReceiptAndIssue_Supplier]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD  CONSTRAINT [FK_Staff_Branch] FOREIGN KEY([BranchId])
REFERENCES [dbo].[Branch] ([BranchId])
GO
ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK_Staff_Branch]
GO
ALTER TABLE [dbo].[Staff]  WITH CHECK ADD  CONSTRAINT [FK_Staff_Contact] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Contact] ([AccountId])
GO
ALTER TABLE [dbo].[Staff] CHECK CONSTRAINT [FK_Staff_Contact]
GO
ALTER TABLE [dbo].[Staff_ConsultingRoom]  WITH CHECK ADD  CONSTRAINT [FK_Staff_ConsultingRoom_ConsultingRoom] FOREIGN KEY([ConsultingRoomId])
REFERENCES [dbo].[ConsultingRoom] ([ConsultingRoomId])
GO
ALTER TABLE [dbo].[Staff_ConsultingRoom] CHECK CONSTRAINT [FK_Staff_ConsultingRoom_ConsultingRoom]
GO
ALTER TABLE [dbo].[Staff_ConsultingRoom]  WITH CHECK ADD  CONSTRAINT [FK_Staff_ConsultingRoom_Staff] FOREIGN KEY([StaffId])
REFERENCES [dbo].[Staff] ([StaffId])
GO
ALTER TABLE [dbo].[Staff_ConsultingRoom] CHECK CONSTRAINT [FK_Staff_ConsultingRoom_Staff]
GO
ALTER TABLE [dbo].[StoreHouse]  WITH CHECK ADD  CONSTRAINT [FK_StoreHouse_Branch] FOREIGN KEY([Branchid])
REFERENCES [dbo].[Branch] ([BranchId])
GO
ALTER TABLE [dbo].[StoreHouse] CHECK CONSTRAINT [FK_StoreHouse_Branch]
GO
ALTER TABLE [dbo].[StoreHouse_Product]  WITH CHECK ADD  CONSTRAINT [FK_StoreHouse_Product_Product] FOREIGN KEY([ProductID])
REFERENCES [dbo].[Product] ([ProductID])
GO
ALTER TABLE [dbo].[StoreHouse_Product] CHECK CONSTRAINT [FK_StoreHouse_Product_Product]
GO
ALTER TABLE [dbo].[StoreHouse_Product]  WITH CHECK ADD  CONSTRAINT [FK_StoreHouse_Product_StoreHouse] FOREIGN KEY([StoreHouseId])
REFERENCES [dbo].[StoreHouse] ([StoreHouseId])
GO
ALTER TABLE [dbo].[StoreHouse_Product] CHECK CONSTRAINT [FK_StoreHouse_Product_StoreHouse]
GO
ALTER TABLE [dbo].[Transfer]  WITH CHECK ADD  CONSTRAINT [FK_Transfer_ReceiptAndIssue] FOREIGN KEY([ReceiptId])
REFERENCES [dbo].[ReceiptAndIssue] ([ReceiptIssueId])
GO
ALTER TABLE [dbo].[Transfer] CHECK CONSTRAINT [FK_Transfer_ReceiptAndIssue]
GO
ALTER TABLE [dbo].[Transfer]  WITH CHECK ADD  CONSTRAINT [FK_Transfer_ReceiptAndIssue1] FOREIGN KEY([ExportId])
REFERENCES [dbo].[ReceiptAndIssue] ([ReceiptIssueId])
GO
ALTER TABLE [dbo].[Transfer] CHECK CONSTRAINT [FK_Transfer_ReceiptAndIssue1]
GO
ALTER TABLE [dbo].[Unit]  WITH CHECK ADD  CONSTRAINT [FK_Unit_UnitGroup] FOREIGN KEY([UnitGroupId])
REFERENCES [dbo].[UnitGroup] ([UnitGroupId])
GO
ALTER TABLE [dbo].[Unit] CHECK CONSTRAINT [FK_Unit_UnitGroup]
GO
ALTER TABLE [dbo].[WorkSchedule]  WITH CHECK ADD  CONSTRAINT [FK_WorkSchedule_ConsultingRoom] FOREIGN KEY([ConsultingRoomId])
REFERENCES [dbo].[ConsultingRoom] ([ConsultingRoomId])
GO
ALTER TABLE [dbo].[WorkSchedule] CHECK CONSTRAINT [FK_WorkSchedule_ConsultingRoom]
GO
ALTER TABLE [dbo].[WorkSchedule]  WITH CHECK ADD  CONSTRAINT [FK_WorkSchedule_Staff] FOREIGN KEY([Staffid])
REFERENCES [dbo].[Staff] ([StaffId])
GO
ALTER TABLE [dbo].[WorkSchedule] CHECK CONSTRAINT [FK_WorkSchedule_Staff]
GO
