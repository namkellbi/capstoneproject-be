package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.entity.ReceiptAndIssue;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.entity.Supplier;
import fpt.edu.capstone.repository.SupplierRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class SupplierServiceImplTest {

    @Mock
    private SupplierRepository mockSupplierRepository;

    private SupplierServiceImpl supplierServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        supplierServiceImplUnderTest = new SupplierServiceImpl(mockSupplierRepository);
    }
    public ReceiptAndIssue receiptAndIssue(){
        receiptAndIssue().setReceiptIssueId(1);
        receiptAndIssue().setWareHouseTransferId(1);
        receiptAndIssue().setReceivingWareHouseId(1);
        receiptAndIssue().setPersonInChargeId(1);
        receiptAndIssue().setPetitionerId(1);
        receiptAndIssue().setTotalCost(0.0);
        receiptAndIssue().setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptAndIssue().setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 2).getTime());
        receiptAndIssue().setStatusId(1);
        receiptAndIssue().setNox(Boolean.valueOf("True"));
        receiptAndIssue().setSupplierId(1);
        receiptAndIssue().setTransfer(Boolean.valueOf("true"));
        return receiptAndIssue();
    }
    public Staff staff(){
        Staff staff = new Staff();
        staff.setStaffId(1);
        staff.setAvatar("avatar.com");
        staff.setCertificate("cert");
        staff.setEducation("FPT");
        staff.setBranchId(1);
        return staff;
    }
    public Supplier supplier(){
        supplier().setSupplierId(1);
        supplier().setSupplierName("supplierName");
        supplier().setAbbreviationName("abbreviationName");
        supplier().setAddress("address");
        supplier().setPhoneNumber("phoneNumber");
        supplier().setUse(Boolean.valueOf("true"));
        supplier().setMail("mail");
        return supplier();
    }
    @Test
    void testGetAllSupplier() {
        final List<Supplier> suppliers = Arrays.asList(
                supplier());
        when(mockSupplierRepository.getAllSuppliers()).thenReturn(suppliers);


        final List<Supplier> result = supplierServiceImplUnderTest.getAllSupplier();


    }

    @Test
    void testGetAllSupplier_SupplierRepositoryReturnsNoItems() {

        when(mockSupplierRepository.getAllSuppliers()).thenReturn(Collections.emptyList());


        final List<Supplier> result = supplierServiceImplUnderTest.getAllSupplier();


        assertThat(result).isEqualTo(Collections.emptyList());
    }
}
