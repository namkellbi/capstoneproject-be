package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.medicalreportservice.IReportServiceResponse;
import fpt.edu.capstone.dto.medicalreportservice.ServicesRequest;
import fpt.edu.capstone.dto.service.ReportServiceRequest;
import fpt.edu.capstone.entity.*;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.service.ExaminationService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedicalRecordServiceImplTest {

    @Mock
    private MedicalRecordRepository mockMedicalRecordRepository;
    @Mock
    private MedicalReportServiceRepository mockMrServiceRepository;
    @Mock
    private InvoiceRepository mockInvoiceRepository;
    @Mock
    private MedicalReportServiceRepository mockRServiceRepository;
    @Mock
    private ContactRepository mockContactRepository;
    @Mock
    private MedicalReportRepository mockMedicalReportRepository;
    @Mock
    private ModelMapper mockModelMapper;
    @Mock
    private BranchRepository mockBranchRepository;
    @Mock
    private ExaminationService mockExaminationService;

    private MedicalRecordServiceImpl medicalRecordServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        medicalRecordServiceImplUnderTest = new MedicalRecordServiceImpl(mockMedicalRecordRepository,
                mockMrServiceRepository, mockInvoiceRepository, mockRServiceRepository, mockContactRepository,
                mockMedicalReportRepository, mockModelMapper, mockBranchRepository, mockExaminationService);
    }
    public MedicalRecord medicalRecord(){
        medicalRecord().setMedicalRecordId(1);
        medicalRecord().setAccountId(1);
        medicalRecord().setCircuit(0);
        medicalRecord().setBloodPressure(0);
        medicalRecord().setBMI(0.0f);
        medicalRecord().setTemperature(0.0f);
        medicalRecord().setWeight(0.0f);
        medicalRecord().setPastMedicalHistory("pastMedicalHistory");
        medicalRecord().setAllergies("allergies");
        medicalRecord().setNote("note");
        medicalRecord().setIcd10("icd10");
        medicalRecord().setIcd("icd");
        medicalRecord().setAdvice("advice");
        medicalRecord().setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        return medicalRecord();
    }
    public MedicalReport medicalReport(){
        medicalReport().setMedicalReportId(1);
        medicalReport().setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        medicalReport().setAccountId(1);
        medicalReport().setFinishedExamination(Boolean.valueOf("false"));
        medicalReport().setMedicalRecordId(1);
        return medicalReport();
    }

    public Contact contact(){
        Contact contact = new Contact();
        contact.setAccountId(1);
        contact.setFullName("Nguyen Quang Vinh");
        contact.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        contact.setAddress("Hanoi");
        contact.setVillage("Cau Giay");
        contact.setDistrict("128 Cau Giay");
        contact.setProvince("Cau Giay");
        contact.setSex("Male");
        contact.setIdentityCard("415213212");
        contact.setPhoneNumber("1967445451");
        contact.setEthnicity("Eth");
        contact.setJob("job");
        contact.setStatus(true);
        contact.setRoleId(1);
        contact.setEmail("nqv@gmail.com");
        return contact;
    }
    public Invoice invoice(){
        invoice().setInvoiceId(1);
        invoice().setStaffId(1);
        invoice().setMedicalReportId(1);
        invoice().setTotalCost(0.0);
        invoice().setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        invoice().setBranchId(1);
        invoice().setStatus(Boolean.valueOf("False"));
        return invoice();
    }

    @Test
    void testUpdateNotePatient() {
                
                
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

                
        final MedicalRecord medicalRecord1 = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord1);

                
        medicalRecordServiceImplUnderTest.updateNotePatient(1, "note");

                
        verify(mockMedicalRecordRepository).save(any(MedicalRecord.class));
    }

    @Test
    void testUpdateNotePatient_MedicalRecordRepositoryFindMedicalRecordNewestByAccountIdReturnsNull() {
                
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(null);

                
        assertThatThrownBy(() -> medicalRecordServiceImplUnderTest.updateNotePatient(1, "note"))
                .isInstanceOf(ClinicException.class);
    }

    @Test
    void testGetReportServiceByIdPatient() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMrServiceRepository.getReportServicePatient(1)).thenReturn(Arrays.asList());

                
        final List<IReportServiceResponse> result = medicalRecordServiceImplUnderTest.getReportServiceByIdPatient(1);

                
    }

    @Test
    void testGetReportServiceByIdPatient_MedicalReportServiceRepositoryReturnsNull() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMrServiceRepository.getReportServicePatient(1)).thenReturn(null);

                
        assertThatThrownBy(() -> medicalRecordServiceImplUnderTest.getReportServiceByIdPatient(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetReportServiceByIdPatient_MedicalReportServiceRepositoryReturnsNoItems() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMrServiceRepository.getReportServicePatient(1)).thenReturn(Collections.emptyList());

                
        final List<IReportServiceResponse> result = medicalRecordServiceImplUnderTest.getReportServiceByIdPatient(1);

                
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetServiceByIdPatientInConsulting() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMrServiceRepository.getReportServicePatientAndRoom(1, 1)).thenReturn(Arrays.asList());

                
        final List<IReportServiceResponse> result = medicalRecordServiceImplUnderTest.getServiceByIdPatientInConsulting(
                1, 1);

                
    }

    @Test
    void testGetServiceByIdPatientInConsulting_MedicalReportServiceRepositoryReturnsNull() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMrServiceRepository.getReportServicePatientAndRoom(1, 1)).thenReturn(null);

                
        assertThatThrownBy(
                () -> medicalRecordServiceImplUnderTest.getServiceByIdPatientInConsulting(1, 1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetServiceByIdPatientInConsulting_MedicalReportServiceRepositoryReturnsNoItems() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMrServiceRepository.getReportServicePatientAndRoom(1, 1)).thenReturn(Collections.emptyList());

                
        final List<IReportServiceResponse> result = medicalRecordServiceImplUnderTest.getServiceByIdPatientInConsulting(
                0, 0);

                
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testPaymentService() {
                
        final ServicesRequest servicesRequest = new ServicesRequest();
        servicesRequest.setIdServices(Arrays.asList(1));

                
        final Optional<MedicalReportService> medicalReportService = Optional.of(
                new MedicalReportService(1, 1, 1, 0.0, true, 1, true));
        when(mockMrServiceRepository.findById(1)).thenReturn(medicalReportService);

                
        final MedicalReportService medicalReportService1 = new MedicalReportService(1, 1, 1, 0.0, true, 1, true);
        when(mockMrServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService1);

                
        medicalRecordServiceImplUnderTest.paymentService(servicesRequest);

                
        verify(mockMrServiceRepository).save(any(MedicalReportService.class));
        verify(mockExaminationService).addSTT(1, 1);
    }

    @Test
    void testPaymentService_MedicalReportServiceRepositoryFindByIdReturnsAbsent() {
                
        final ServicesRequest servicesRequest = new ServicesRequest();
        servicesRequest.setIdServices(Arrays.asList(1));

        when(mockMrServiceRepository.findById(1)).thenReturn(Optional.empty());

                
        assertThatThrownBy(() -> medicalRecordServiceImplUnderTest.paymentService(servicesRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testFinishPayment() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockRServiceRepository.getReportServicePatientIsPaid(1, false)).thenReturn(Arrays.asList());
        when(mockInvoiceRepository.getMedicalReportNewest(1)).thenReturn(null);

                
        final Optional<Invoice> invoice = Optional.of(
               invoice());
        when(mockInvoiceRepository.getByMedicalReportId(1)).thenReturn(invoice);

                
        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

                
        final Contact contact1 = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact1);

                
        medicalRecordServiceImplUnderTest.finishPayment(1);

                
        verify(mockInvoiceRepository).updateStatusInvoice(true, 1);
        verify(mockContactRepository).save(any(Contact.class));
    }

    @Test
    void testFinishPayment_MedicalReportServiceRepositoryReturnsNoItems() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockRServiceRepository.getReportServicePatientIsPaid(1, false)).thenReturn(Collections.emptyList());
        when(mockInvoiceRepository.getMedicalReportNewest(1)).thenReturn(null);

                
        final Optional<Invoice> invoice = Optional.of(
               invoice());
        when(mockInvoiceRepository.getByMedicalReportId(1)).thenReturn(invoice);

                
        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

                
        final Contact contact1 = contact();
        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact1);

                
        medicalRecordServiceImplUnderTest.finishPayment(1);

                
        verify(mockInvoiceRepository).updateStatusInvoice(true, 1);
        verify(mockContactRepository).save(any(Contact.class));
    }

    @Test
    void testFinishPayment_InvoiceRepositoryGetMedicalReportNewestReturnsNull() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockRServiceRepository.getReportServicePatientIsPaid(1, false)).thenReturn(Arrays.asList());
        when(mockInvoiceRepository.getMedicalReportNewest(1)).thenReturn(null);

                
        assertThatThrownBy(() -> medicalRecordServiceImplUnderTest.finishPayment(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testFinishPayment_InvoiceRepositoryGetByMedicalReportIdReturnsAbsent() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockRServiceRepository.getReportServicePatientIsPaid(1, false)).thenReturn(Arrays.asList());
        when(mockInvoiceRepository.getMedicalReportNewest(1)).thenReturn(null);
        when(mockInvoiceRepository.getByMedicalReportId(1)).thenReturn(Optional.empty());

                
        assertThatThrownBy(() -> medicalRecordServiceImplUnderTest.finishPayment(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testFinishPayment_ContactRepositoryFindByIdReturnsAbsent() {
                
                
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.getMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockRServiceRepository.getReportServicePatientIsPaid(1, false)).thenReturn(Arrays.asList());
        when(mockInvoiceRepository.getMedicalReportNewest(1)).thenReturn(null);

                
        final Optional<Invoice> invoice = Optional.of(
               invoice());
        when(mockInvoiceRepository.getByMedicalReportId(1)).thenReturn(invoice);

        when(mockContactRepository.findById(1)).thenReturn(Optional.empty());

                
        assertThatThrownBy(() -> medicalRecordServiceImplUnderTest.finishPayment(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetServicePaying() {
                
        final ReportServiceRequest reportServiceRequest = new ReportServiceRequest();
        reportServiceRequest.setReportServices(Arrays.asList(1));

        when(mockMrServiceRepository.getReportServiceById(1)).thenReturn(null);

                
        final List<IReportServiceResponse> result = medicalRecordServiceImplUnderTest.getServicePaying(
                reportServiceRequest);

                
    }

    @Test
    void testGetServicePaying_MedicalReportServiceRepositoryReturnsNull() {
                
        final ReportServiceRequest reportServiceRequest = new ReportServiceRequest();
        reportServiceRequest.setReportServices(Arrays.asList(1));

        when(mockMrServiceRepository.getReportServiceById(1)).thenReturn(null);

                
        assertThatThrownBy(() -> medicalRecordServiceImplUnderTest.getServicePaying(reportServiceRequest))
                .isInstanceOf(RuntimeException.class);
    }
}
