package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.patient.PatientResponse;
import fpt.edu.capstone.dto.service.RServiceDTO;
import fpt.edu.capstone.entity.*;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.service.ContactService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ExaminationServiceImplTest {

    @Mock
    private ContactRepository mockContactRepository;
    @Mock
    private MedicalReportRepository mockMedicalReportRepository;
    @Mock
    private ConsultingRoomRepository mockConsultingRoomRepository;
    @Mock
    private MedicalRecordRepository mockMedicalRecordRepository;
    @Mock
    private MedicalReportServiceRepository mockRServiceRepository;
    @Mock
    private MedicalServiceRepository mockMedicalServiceRepository;
    @Mock
    private InvoiceRepository mockInvoiceRepository;
    @Mock
    private InvoiceDetailRepository mockInvoiceDetailRepository;
    @Mock
    private StaffRepository mockStaffRepository;
    @Mock
    private BranchRepository mockBranchRepository;
    @Mock
    private ContactService mockContactService;
    @Mock
    private MedicalReportServiceRepository mockMrServiceRepository;
    @Mock
    private ModelMapper mockModelMapper;

    private ExaminationServiceImpl examinationServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        examinationServiceImplUnderTest = new ExaminationServiceImpl(mockContactRepository, mockMedicalReportRepository,
                mockConsultingRoomRepository, mockMedicalRecordRepository, mockRServiceRepository,
                mockMedicalServiceRepository, mockInvoiceRepository, mockInvoiceDetailRepository, mockStaffRepository,
                mockBranchRepository, mockContactService, mockMrServiceRepository, mockModelMapper);
    }
    public Contact contact(){
        Contact contact = new Contact();
        contact.setAccountId(1);
        contact.setFullName("Nguyen Quang Vinh");
        contact.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        contact.setAddress("Hanoi");
        contact.setVillage("Cau Giay");
        contact.setDistrict("128 Cau Giay");
        contact.setProvince("Cau Giay");
        contact.setSex("Male");
        contact.setIdentityCard("415213212");
        contact.setPhoneNumber("1967445451");
        contact.setEthnicity("Eth");
        contact.setJob("job");
        contact.setStatus(true);
        contact.setRoleId(1);
        contact.setEmail("nqv@gmail.com");
        return contact;
    }

    @Test
    void testFindPatientByConsultingRoom() {
             when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomId(1, 6, 1))
                .thenReturn(Arrays.asList());

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);

             final PatientResponse response = new PatientResponse(1, "fullName",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, "consultingRoomName", "sex", "address",
                "allergies", "pastMedicalHistory", "phoneNumber", "identityCard", "village", "district", "province",
                "email", 1, 1, 1, "statusName");
        when(mockModelMapper.map(any(Object.class), eq(PatientResponse.class))).thenReturn(response);

             final List<PatientResponse> result = examinationServiceImplUnderTest.findPatientByConsultingRoom(1, 1);

             
    }

    @Test
    void testFindPatientByConsultingRoom_MedicalReportRepositoryReturnsNoItems() {
             when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomId(1, 6, 1))
                .thenReturn(Collections.emptyList());

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);

             final PatientResponse response = new PatientResponse(1, "fullName",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, "consultingRoomName", "sex", "address",
                "allergies", "pastMedicalHistory", "phoneNumber", "identityCard", "village", "district", "province",
                "email", 1, 1, 1, "statusName");
        when(mockModelMapper.map(any(Object.class), eq(PatientResponse.class))).thenReturn(response);

             final List<PatientResponse> result = examinationServiceImplUnderTest.findPatientByConsultingRoom(1, 1);

             
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testFindPatientByConsultingRoom_ContactRepositoryReturnsAbsent() {
             when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomId(1, 6, 1))
                .thenReturn(Arrays.asList());
        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(Optional.empty());

             final PatientResponse response = new PatientResponse(1, "fullName",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, "consultingRoomName", "sex", "address",
                "allergies", "pastMedicalHistory", "phoneNumber", "identityCard", "village", "district", "province",
                "email", 1, 1, 1, "statusName");
        when(mockModelMapper.map(any(Object.class), eq(PatientResponse.class))).thenReturn(response);

             final List<PatientResponse> result = examinationServiceImplUnderTest.findPatientByConsultingRoom(1, 1);

             
    }

    @Test
    void testChangeStatusExamination() {
             final MedicalReport medicalReport = new MedicalReport(1,
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, false, 1);
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

             examinationServiceImplUnderTest.changeStatusExamination(1, 1);

             
        verify(mockMedicalReportRepository).changeStatusExamination(1, 1, 5);
    }

    @Test
    void testChangeStatusExamination_MedicalReportRepositoryFindMedicalReportNewestReturnsNull() {
             when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.changeStatusExamination(1, 1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testUpdateIndexPatient() {
             final MedicalReport medicalReport = new MedicalReport(1,
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, false, 1);
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());

             examinationServiceImplUnderTest.updateIndexPatient(1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
    }

    @Test
    void testUpdateIndexPatient_MedicalReportRepositoryFindMedicalReportNewestReturnsNull() {
             when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.updateIndexPatient(1, 1, 1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testUpdateIndexPatient_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
             final MedicalReport medicalReport = new MedicalReport(1,
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, false, 1);
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());

             examinationServiceImplUnderTest.updateIndexPatient(1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
    }
    public ContactRequest contactRequest (){
        ContactRequest contact = new ContactRequest();
        contact.setFullName("Nguyen Quang Vinh");
        contact.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
        contact.setAddress("Hanoi");
        contact.setVillage("Cau Giay");
        contact.setDistrict("128 Cau Giay");
        contact.setProvince("Cau Giay");
        contact.setSex("Male");
        contact.setIdentityCard("415213212");
        contact.setPhoneNumber("1967445451");
        contact.setEthnicity("Eth");
        contact.setJob("job");
        contact.setEmail("nqv@gmail.com");
        return contact;
    }
    public Staff staff(){
        Staff staff = new Staff();
        staff.setStaffId(1);
        staff.setAvatar("avatar.com");
        staff.setCertificate("cert");
        staff.setEducation("FPT");
        staff.setBranchId(1);
        return staff;
    }
    public ConsultingRoom consultingRoom(){
        ConsultingRoom consultingRoom = new ConsultingRoom();
        consultingRoom.setConsultingRoomId(1);
        consultingRoom.setRoomName("Kham ABC");
        consultingRoom.setAbbreviationName("ABC");
        consultingRoom.setBranchId(1);
        consultingRoom.setAction(true);
        return consultingRoom;
    }
    public Branch branch(){
        branch().setBranchId(1);
        branch().setBranchName("branchName");
        branch().setAction(Boolean.valueOf("false"));
        branch().setPhoneNumber("phoneNumber");
        branch().setAddress("address");
        return branch();
    }
    public MedicalRecord medicalRecord(){
        medicalRecord().setMedicalRecordId(1);
        medicalRecord().setAccountId(1);
        medicalRecord().setCircuit(0);
        medicalRecord().setBloodPressure(0);
        medicalRecord().setBMI(0.0f);
        medicalRecord().setTemperature(0.0f);
        medicalRecord().setWeight(0.0f);
        medicalRecord().setPastMedicalHistory("pastMedicalHistory");
        medicalRecord().setAllergies("allergies");
        medicalRecord().setNote("note");
        medicalRecord().setIcd10("icd10");
        medicalRecord().setIcd("icd");
        medicalRecord().setAdvice("advice");
        medicalRecord().setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        return medicalRecord();
    }
    public MedicalReport medicalReport(){
        medicalReport().setMedicalReportId(1);
        medicalReport().setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        medicalReport().setAccountId(1);
        medicalReport().setFinishedExamination(Boolean.valueOf("false"));
        medicalReport().setMedicalRecordId(1);
        return medicalReport();
    }
    public Invoice invoice(){
        invoice().setInvoiceId(1);
        invoice().setStaffId(1);
        invoice().setMedicalReportId(1);
        invoice().setTotalCost(0.0);
        invoice().setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        invoice().setBranchId(1);
        invoice().setStatus(Boolean.valueOf("False"));
        return invoice();
    }
    @Test
    void testStartExaminationCreate() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact = contact();
        when(mockContactService.ContactRequestToContact(any(ContactRequest.class))).thenReturn(contact);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(0)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndInWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             final Contact result = examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1, 1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testStartExaminationCreate_ConsultingRoomRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

        when(mockConsultingRoomRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1 ,1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationCreate_StaffRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationCreate_BranchRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

        when(mockBranchRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationCreate_MedicalRecordRepositoryFindMedicalReportServiceReturnsNull() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact = contact();
        when(mockContactService.ContactRequestToContact(any(ContactRequest.class))).thenReturn(contact);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationCreate_MedicalServiceRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact = contact();
        when(mockContactService.ContactRequestToContact(any(ContactRequest.class))).thenReturn(contact);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
        when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(ClinicException.class);
    }

    @Test
    void testStartExaminationCreate_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact = contact();
        when(mockContactService.ContactRequestToContact(any(ContactRequest.class))).thenReturn(contact);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(0)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.getByConsultingAndInWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             final Contact result = examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1  , 1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testStartExaminationCreate_MedicalReportRepositoryGetByConsultingAndInWaitReturnsNoItems() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact = contact();
        when(mockContactService.ContactRequestToContact(any(ContactRequest.class))).thenReturn(contact);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(0)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndInWait(1, 6)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             final Contact result = examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1, 1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testStartExaminationCreate_MedicalReportRepositoryExistPatientInConsultingReturnsNull() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact = contact();
        when(mockContactService.ContactRequestToContact(any(ContactRequest.class))).thenReturn(contact);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(0)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndInWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             final Contact result = examinationServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1, 1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testStartExaminationUpdate() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact1 = contact();
        when(mockContactService.updateContact(eq(1), any(ContactRequest.class))).thenReturn(contact1);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(0)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndInWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             final Contact result = examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1, 1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testStartExaminationUpdate_ContactRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

        when(mockContactRepository.findById(0)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationUpdate_ConsultingRoomRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

        when(mockConsultingRoomRepository.findById(0)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationUpdate_StaffRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationUpdate_BranchRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

        when(mockBranchRepository.findById(0)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationUpdate_MedicalRecordRepositoryFindMedicalReportServiceReturnsNull() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact1 = contact();
        when(mockContactService.updateContact(eq(1), any(ContactRequest.class))).thenReturn(contact1);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1,1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testStartExaminationUpdate_MedicalServiceRepositoryReturnsAbsent() {
             final ContactRequest contactRequest = contactRequest();

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

             final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

             final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

             final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

             final Contact contact1 = contact();
        when(mockContactService.updateContact(eq(1), any(ContactRequest.class))).thenReturn(contact1);

             final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

             final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1,1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
        when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(ClinicException.class);
    }

    @Test
    void testStartExaminationUpdate_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
             final ContactRequest contactRequest = contactRequest();

        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

        final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

        final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

        final Contact contact1 = contact();
        when(mockContactService.updateContact(eq(1), any(ContactRequest.class))).thenReturn(contact1);

        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

        final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1,1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.getByConsultingAndInWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             final Contact result = examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1,1,1,1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testStartExaminationUpdate_MedicalReportRepositoryGetByConsultingAndInWaitReturnsNoItems() {
             final ContactRequest contactRequest = contactRequest();

        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

        final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

        final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

        final Contact contact1 = contact();
        when(mockContactService.updateContact(eq(1), any(ContactRequest.class))).thenReturn(contact1);

        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

        final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1,1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndInWait(1, 6)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             final Contact result = examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1,1,1,1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testStartExaminationUpdate_MedicalReportRepositoryExistPatientInConsultingReturnsNull() {
             final ContactRequest contactRequest = contactRequest();

        final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findById(1)).thenReturn(contact);

        final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);

        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

        final Optional<Branch> branch = Optional.of(branch());
        when(mockBranchRepository.findById(1)).thenReturn(branch);

        final Contact contact1 = contact();
        when(mockContactService.updateContact(eq(1), any(ContactRequest.class))).thenReturn(contact1);

        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);

        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);

        final Invoice invoice = invoice();
        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);

        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1,1, 1));
        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndInWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             final Contact result = examinationServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
                "pastMedicalHistory", 1,1,1,1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testFinishExamination() {
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
        when(mockRServiceRepository.getSumCostService(1)).thenReturn(0.0);
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());

             examinationServiceImplUnderTest.finishExamination(1, 1, "note");

             
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
        verify(mockMedicalReportRepository).updateFinishExamination(1, 1);
        verify(mockInvoiceRepository).updateTotalCostInvoice(0.0, 1);
        verify(mockMedicalReportRepository).deletePatientIndex(1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
    }

    @Test
    void testFinishExamination_MedicalRecordRepositoryFindMedicalRecordNewestByAccountIdReturnsNull() {
             when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(null);

        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
        when(mockRServiceRepository.getSumCostService(1)).thenReturn(0.0);
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());

             examinationServiceImplUnderTest.finishExamination(1, 1, "note");

             
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
        verify(mockMedicalReportRepository).updateFinishExamination(1, 1);
        verify(mockInvoiceRepository).updateTotalCostInvoice(0.0, 1);
        verify(mockMedicalReportRepository).deletePatientIndex(1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
    }

    @Test
    void testFinishExamination_MedicalReportRepositoryFindMedicalReportNewestReturnsNull() {
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.finishExamination(1, 1, "note"))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testFinishExamination_MedicalRecordRepositoryFindMedicalReportServiceReturnsNull() {
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.finishExamination(1, 1, "note"))
                .isInstanceOf(RuntimeException.class);
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
    }

    @Test
    void testFinishExamination_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(0)).thenReturn(medicalRecord);

       
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
        when(mockRServiceRepository.getSumCostService(1)).thenReturn(0.0);
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());

             examinationServiceImplUnderTest.finishExamination(1, 1, "note");

             
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
        verify(mockMedicalReportRepository).updateFinishExamination(1, 1);
        verify(mockInvoiceRepository).updateTotalCostInvoice(0.0, 1);
        verify(mockMedicalReportRepository).deletePatientIndex(1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
    }

    @Test
    void testAddPatientService() {
          
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

       
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());

             examinationServiceImplUnderTest.addPatientService(1, "note", 1, 1, 1, 1);

             
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting3(1, 1, 6);
    }

    @Test
    void testAddPatientService_MedicalRecordRepositoryFindMedicalRecordNewestByAccountIdReturnsNull() {
             when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.addPatientService(1, "note", 1, 1, 1, 1))
                .isInstanceOf(ClinicException.class);
    }

    @Test
    void testAddPatientService_MedicalRecordRepositoryFindMedicalReportServiceReturnsNull() {
         
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.addPatientService(1, "note", 1, 1, 1, 1))
                .isInstanceOf(RuntimeException.class);
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
    }

    @Test
    void testAddPatientService_MedicalServiceRepositoryReturnsAbsent() {
           
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
        when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.addPatientService(1, "note", 1, 1, 1, 1))
                .isInstanceOf(ClinicException.class);
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
    }

    @Test
    void testAddPatientService_MedicalReportRepositoryFindMedicalReportNewestReturnsNull() {
           
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.addPatientService(1, "note", 1, 1, 1, 1))
                .isInstanceOf(RuntimeException.class);
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
    }

    @Test
    void testAddPatientService_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
            
        final MedicalRecord medicalRecord = medicalRecord();
        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);

        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

             final MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);

        
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());

             examinationServiceImplUnderTest.addPatientService(1, "note", 1, 1, 1, 1);

             
        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting3(1, 1, 6);
    }

    @Test
    void testChangeRoomService() {
            
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.changeRoomService(1, 1, 1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testChangeRoomService_MedicalReportRepositoryFindMedicalReportNewestReturnsNull() {
             when(mockMedicalReportRepository.findMedicalReportNewest(0)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.changeRoomService(0, 0, 0, 0, 0))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testChangeRoomService_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.changeRoomService(1, 1, 1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testChangeRoomService_MedicalReportRepositoryExistPatientInConsultingReturnsNull() {
             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.changeRoomService(1, 1, 1, 1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testFinishServiceExamination() {
             final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.findReport_Consulting(1)).thenReturn(null);

             examinationServiceImplUnderTest.finishServiceExamination(1, 1);

             
        verify(mockMedicalReportRepository).deletePatientIndex(1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting3(1, 1, 4);
    }

    @Test
    void testFinishServiceExamination_MedicalReportRepositoryFindMedicalReportNewestReturnsNull() {
             when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(null);

             assertThatThrownBy(() -> examinationServiceImplUnderTest.finishServiceExamination(1, 1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testFinishServiceExamination_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
             
        final MedicalReport medicalReport = medicalReport();
        when(mockMedicalReportRepository.findMedicalReportNewest(1)).thenReturn(medicalReport);

        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.findReport_Consulting(1)).thenReturn(null);

             examinationServiceImplUnderTest.finishServiceExamination(1, 1);

             
        verify(mockMedicalReportRepository).deletePatientIndex(1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting2(1, 1, 1);
        verify(mockMedicalReportRepository).updatePatientInConsulting3(1, 1, 4);
    }

    @Test
    void testAddSTT() {
                  final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

        when(mockMedicalReportRepository.getByConsultingAndIdConsulting(1, 1)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.addSTT(1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testAddSTT_MedicalServiceRepositoryReturnsAbsent() {
             when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.addSTT(1, 1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testAddSTT_MedicalReportRepositoryGetByConsultingAndIdConsultingReturnsNoItems() {
                  final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

        when(mockMedicalReportRepository.getByConsultingAndIdConsulting(1, 1)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.addSTT(1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testAddSTT_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
                  final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

        when(mockMedicalReportRepository.getByConsultingAndIdConsulting(1, 1)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.addSTT(1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testAddSTT_MedicalReportRepositoryExistPatientInConsultingReturnsNull() {
                  final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

        when(mockMedicalReportRepository.getByConsultingAndIdConsulting(1, 1)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.addSTT(1, 1);

             
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testAddSTT2() {
             final RServiceDTO rServiceDTO = new RServiceDTO();
        rServiceDTO.setIdReportService(0);
        rServiceDTO.setIdService(0);
        final List<RServiceDTO> rServiceDTOS = Arrays.asList(rServiceDTO);

             final Optional<MedicalReportService> medicalReportService = Optional.of(
                new MedicalReportService(1, 1, 1, 0.0, false, 1, false));
        when(mockMrServiceRepository.findById(0)).thenReturn(medicalReportService);

             final MedicalReportService medicalReportService1 = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockMrServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService1);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

        when(mockMedicalReportRepository.getByConsultingAndIdConsulting(1, 1)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.addSTT2(rServiceDTOS);

             
        verify(mockMrServiceRepository).save(any(MedicalReportService.class));
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testAddSTT2_MedicalReportServiceRepositoryFindByIdReturnsAbsent() {
             final RServiceDTO rServiceDTO = new RServiceDTO();
        rServiceDTO.setIdReportService(1);
        rServiceDTO.setIdService(1);
        final List<RServiceDTO> rServiceDTOS = Arrays.asList(rServiceDTO);
        when(mockMrServiceRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.addSTT2(rServiceDTOS))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testAddSTT2_MedicalServiceRepositoryReturnsAbsent() {
             final RServiceDTO rServiceDTO = new RServiceDTO();
        rServiceDTO.setIdReportService(1);
        rServiceDTO.setIdService(1);
        final List<RServiceDTO> rServiceDTOS = Arrays.asList(rServiceDTO);

             final Optional<MedicalReportService> medicalReportService = Optional.of(
                new MedicalReportService(1, 1, 1, 0.0, false, 1, false));
        when(mockMrServiceRepository.findById(0)).thenReturn(medicalReportService);

             final MedicalReportService medicalReportService1 = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockMrServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService1);

        when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());

             assertThatThrownBy(() -> examinationServiceImplUnderTest.addSTT2(rServiceDTOS))
                .isInstanceOf(RuntimeException.class);
        verify(mockMrServiceRepository).save(any(MedicalReportService.class));
    }

    @Test
    void testAddSTT2_MedicalReportRepositoryGetByConsultingAndIdConsultingReturnsNoItems() {
             final RServiceDTO rServiceDTO = new RServiceDTO();
        rServiceDTO.setIdReportService(0);
        rServiceDTO.setIdService(0);
        final List<RServiceDTO> rServiceDTOS = Arrays.asList(rServiceDTO);

             final Optional<MedicalReportService> medicalReportService = Optional.of(
                new MedicalReportService(1, 1, 1, 0.0, false, 1, false));
        when(mockMrServiceRepository.findById(1)).thenReturn(medicalReportService);

             final MedicalReportService medicalReportService1 = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockMrServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService1);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

        when(mockMedicalReportRepository.getByConsultingAndIdConsulting(1, 1)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.addSTT2(rServiceDTOS);

             
        verify(mockMrServiceRepository).save(any(MedicalReportService.class));
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testAddSTT2_MedicalReportRepositoryGetByConsultingAndWaitReturnsNoItems() {
             final RServiceDTO rServiceDTO = new RServiceDTO();
        rServiceDTO.setIdReportService(1);
        rServiceDTO.setIdService(1);
        final List<RServiceDTO> rServiceDTOS = Arrays.asList(rServiceDTO);

             final Optional<MedicalReportService> medicalReportService = Optional.of(
                new MedicalReportService(1, 1, 1, 0.0, false, 1, false));
        when(mockMrServiceRepository.findById(1)).thenReturn(medicalReportService);

             final MedicalReportService medicalReportService1 = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockMrServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService1);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

        when(mockMedicalReportRepository.getByConsultingAndIdConsulting(1, 1)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Collections.emptyList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.addSTT2(rServiceDTOS);

             
        verify(mockMrServiceRepository).save(any(MedicalReportService.class));
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testAddSTT2_MedicalReportRepositoryExistPatientInConsultingReturnsNull() {
             final RServiceDTO rServiceDTO = new RServiceDTO();
        rServiceDTO.setIdReportService(0);
        rServiceDTO.setIdService(0);
        final List<RServiceDTO> rServiceDTOS = Arrays.asList(rServiceDTO);

             final Optional<MedicalReportService> medicalReportService = Optional.of(
                new MedicalReportService(1, 1, 1, 0.0, false, 1, false));
        when(mockMrServiceRepository.findById(0)).thenReturn(medicalReportService);

             final MedicalReportService medicalReportService1 = new MedicalReportService(1, 1, 1, 0.0, false, 1, false);
        when(mockMrServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService1);

             final Optional<MedicalService> medicalService = Optional.of(new MedicalService(1, 1, "serviceName", 0.0));
        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);

        when(mockMedicalReportRepository.getByConsultingAndIdConsulting(1, 1)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.getByConsultingAndWait(1, 6)).thenReturn(Arrays.asList());
        when(mockMedicalReportRepository.existPatientInConsulting(1, 1)).thenReturn(null);

             examinationServiceImplUnderTest.addSTT2(rServiceDTOS);

             
        verify(mockMrServiceRepository).save(any(MedicalReportService.class));
        verify(mockMedicalReportRepository).updatePatientInConsulting(1, 1,1, 1, 1);
        verify(mockMedicalReportRepository).insertPatientInConsulting(1, 1, 1, 1, 1);
    }

    @Test
    void testFindPatientByConsultingRoomByStatus() {
             when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomIdAndStatus(1, 6, 1))
                .thenReturn(Arrays.asList());

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);

             final PatientResponse response = new PatientResponse(1, "fullName",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, "consultingRoomName", "sex", "address",
                "allergies", "pastMedicalHistory", "phoneNumber", "identityCard", "village", "district", "province",
                "email", 1, 1, 1, "statusName");
        when(mockModelMapper.map(any(Object.class), eq(PatientResponse.class))).thenReturn(response);

             final List<PatientResponse> result = examinationServiceImplUnderTest.findPatientByConsultingRoomByStatus(1, 1);

             
    }

    @Test
    void testFindPatientByConsultingRoomByStatus_MedicalReportRepositoryReturnsNoItems() {
             when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomIdAndStatus(1, 6, 1))
                .thenReturn(Collections.emptyList());

             final Optional<Contact> contact = Optional.of(
                contact());
        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);

             final PatientResponse response = new PatientResponse(1, "fullName",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, "consultingRoomName", "sex", "address",
                "allergies", "pastMedicalHistory", "phoneNumber", "identityCard", "village", "district", "province",
                "email", 1, 1, 1, "statusName");
        when(mockModelMapper.map(any(Object.class), eq(PatientResponse.class))).thenReturn(response);

             final List<PatientResponse> result = examinationServiceImplUnderTest.findPatientByConsultingRoomByStatus(1, 1);

             
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testFindPatientByConsultingRoomByStatus_ContactRepositoryReturnsAbsent() {
             when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomIdAndStatus(1, 6, 1))
                .thenReturn(Arrays.asList());
        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(Optional.empty());

             final PatientResponse response = new PatientResponse(1, "fullName",
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, "consultingRoomName", "sex", "address",
                "allergies", "pastMedicalHistory", "phoneNumber", "identityCard", "village", "district", "province",
                "email", 1, 1, 1, "statusName");
        when(mockModelMapper.map(any(Object.class), eq(PatientResponse.class))).thenReturn(response);

             final List<PatientResponse> result = examinationServiceImplUnderTest.findPatientByConsultingRoomByStatus(1, 1);

             
    }
}
