package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.inventory.*;
import fpt.edu.capstone.dto.storeHouse.ProductsInventoryRequest;
import fpt.edu.capstone.entity.Inventory;
import fpt.edu.capstone.repository.InventoryRepository;
import fpt.edu.capstone.repository.StaffRepository;
import fpt.edu.capstone.repository.StoreHouseRepository;
import fpt.edu.capstone.service.ExportService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class InventoryServiceImplTest {

    @Mock
    private InventoryRepository mockInventoryRepository;
    @Mock
    private StaffRepository mockStaffRepository;
    @Mock
    private StoreHouseRepository mockStoreHouseRepository;
    @Mock
    private ExportService mockExportService;
    @Mock
    private ModelMapper mockModelMapper;

    private InventoryServiceImpl inventoryServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        inventoryServiceImplUnderTest = new InventoryServiceImpl(mockInventoryRepository, mockStaffRepository,
                mockStoreHouseRepository, mockExportService, mockModelMapper);
    }
    public Inventory inventory(){
        inventory().setInventoryId(1);
        inventory().setPersonInCharge(1);
        inventory().setPetitionerId(1);
        inventory().setStoreHouseId(1);
        inventory().setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        inventory().setInventoryDate(new GregorianCalendar(2020, Calendar.JANUARY, 2).getTime());
        inventory().setStatusId(1);
        return inventory();
    }
    @Test
    void testCreateInventory() {
                
        final InventoryRequest inventoryRequest = new InventoryRequest();
        inventoryRequest.setStoreHouseId(1);
        inventoryRequest.setPetitionerId(1);
        inventoryRequest.setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());

        when(mockStaffRepository.existsById(0)).thenReturn(false);
        when(mockStoreHouseRepository.existsById(0)).thenReturn(false);

                
        final Inventory inventory =    inventory();
        when(mockModelMapper.map(any(Object.class), eq(Inventory.class))).thenReturn(inventory);

                
        final Inventory inventory1 =    inventory();
        when(mockInventoryRepository.save(any(Inventory.class))).thenReturn(inventory1);

                
        final Inventory result = inventoryServiceImplUnderTest.createInventory(inventoryRequest);

                
    }

    @Test
    void testGetAllInventory() {
                
        when(mockInventoryRepository.getByIdBranch(1)).thenReturn(Arrays.asList());

                
        final List<IInventoryResponse> result = inventoryServiceImplUnderTest.getAllInventory(0);

                
    }

    @Test
    void testGetAllInventory_InventoryRepositoryReturnsNoItems() {
                
        when(mockInventoryRepository.getByIdBranch(1)).thenReturn(Collections.emptyList());

                
        final List<IInventoryResponse> result = inventoryServiceImplUnderTest.getAllInventory(1);

                
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetInventoriesByStatus() {
                
        when(mockInventoryRepository.getByIdBranchByStatus(1, 1)).thenReturn(Arrays.asList());

                
        final List<IInventoryResponse> result = inventoryServiceImplUnderTest.getInventoriesByStatus(1, 1);

                
    }

    @Test
    void testGetInventoriesByStatus_InventoryRepositoryReturnsNoItems() {
                
        when(mockInventoryRepository.getByIdBranchByStatus(1, 1)).thenReturn(Collections.emptyList());

                
        final List<IInventoryResponse> result = inventoryServiceImplUnderTest.getInventoriesByStatus(1, 1);

                
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetInventoryByIdInventory() {
                
        when(mockInventoryRepository.existsById(1)).thenReturn(false);
        when(mockInventoryRepository.getFullInventory(1)).thenReturn(null);

                
        final IFullInventoryResponse result = inventoryServiceImplUnderTest.getInventoryByIdInventory(1);

                
    }

    @Test
    void testGetInventoryProduct() {
                
        when(mockInventoryRepository.getInventoriesProduct(1)).thenReturn(Arrays.asList());

                
        final List<IInventoryProductResponse> result = inventoryServiceImplUnderTest.getInventoryProduct(1);

                
    }

    @Test
    void testGetInventoryProduct_InventoryRepositoryReturnsNoItems() {
                
        when(mockInventoryRepository.getInventoriesProduct(1)).thenReturn(Collections.emptyList());

                
        final List<IInventoryProductResponse> result = inventoryServiceImplUnderTest.getInventoryProduct(1);

                
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testCancelInventory() {
                
                
        inventoryServiceImplUnderTest.cancelInventory(1);

                
        verify(mockInventoryRepository).updateInventoryStatus(3, 1);
    }

    @Test
    void testFinishInventory() {
                
        final InventoryProductRequest inventoryProductRequest = new InventoryProductRequest();
        inventoryProductRequest.setIdInventory(1);
        inventoryProductRequest.setIdPersonInCharge(1);
        inventoryProductRequest.setInventoryDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductsInventoryRequest productsInventoryRequest = new ProductsInventoryRequest();
        productsInventoryRequest.setIdProduct(1);
        productsInventoryRequest.setQuantityStock(1);
        productsInventoryRequest.setActualQuantity(1);
        productsInventoryRequest.setDeviant(1);
        productsInventoryRequest.setNote("note");
        inventoryProductRequest.setProductsInventoryRequestList(Arrays.asList(productsInventoryRequest));

                
        final Optional<Inventory> inventory = Optional.of(
                   inventory());
        when(mockInventoryRepository.findById(1)).thenReturn(inventory);

                
        final Inventory inventory1 =    inventory();
        when(mockInventoryRepository.save(any(Inventory.class))).thenReturn(inventory1);

                
        inventoryServiceImplUnderTest.finishInventory(inventoryProductRequest);

                
        verify(mockInventoryRepository).insertInventoryProduct(1, 1, 1, 1, 1, 0, "note");
        verify(mockInventoryRepository).updateInventoryStatus(2, 1);
        verify(mockInventoryRepository).save(any(Inventory.class));
    }

    @Test
    void testFinishInventory_InventoryRepositoryFindByIdReturnsAbsent() {
                
        final InventoryProductRequest inventoryProductRequest = new InventoryProductRequest();
        inventoryProductRequest.setIdInventory(1);
        inventoryProductRequest.setIdPersonInCharge(1);
        inventoryProductRequest.setInventoryDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductsInventoryRequest productsInventoryRequest = new ProductsInventoryRequest();
        productsInventoryRequest.setIdProduct(1);
        productsInventoryRequest.setQuantityStock(1);
        productsInventoryRequest.setActualQuantity(1);
        productsInventoryRequest.setDeviant(1);
        productsInventoryRequest.setNote("note");
        inventoryProductRequest.setProductsInventoryRequestList(Arrays.asList(productsInventoryRequest));

        when(mockInventoryRepository.findById(1)).thenReturn(Optional.empty());

                
        assertThatThrownBy(() -> inventoryServiceImplUnderTest.finishInventory(inventoryProductRequest))
                .isInstanceOf(RuntimeException.class);
    }
}
