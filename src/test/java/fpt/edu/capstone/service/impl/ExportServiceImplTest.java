package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.export.*;
import fpt.edu.capstone.dto.product.IProduct;
import fpt.edu.capstone.entity.ReceiptAndIssue;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.entity.StoreHouse;
import fpt.edu.capstone.repository.ReceiptIssueRepository;
import fpt.edu.capstone.repository.StaffRepository;
import fpt.edu.capstone.repository.StatusRepository;
import fpt.edu.capstone.repository.StoreHouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ExportServiceImplTest {

    @Mock
    private ReceiptIssueRepository mockReceiptIssueRepository;
    @Mock
    private StaffRepository mockStaffRepository;
    @Mock
    private StatusRepository mockStatusRepository;
    @Mock
    private StoreHouseRepository mockStoreHouseRepository;
    @Mock
    private ModelMapper mockModelMapper;

    private ExportServiceImpl exportServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        exportServiceImplUnderTest = new ExportServiceImpl(mockReceiptIssueRepository, mockStaffRepository,
                mockStatusRepository, mockStoreHouseRepository, mockModelMapper);
    }
    public Staff staff(){
        Staff staff = new Staff();
        staff.setStaffId(1);
        staff.setAvatar("avatar.com");
        staff.setCertificate("cert");
        staff.setEducation("FPT");
        staff.setBranchId(1);
        return staff;
    }
    public ReceiptAndIssue receiptAndIssue(){
        receiptAndIssue().setReceiptIssueId(1);
        receiptAndIssue().setWareHouseTransferId(1);
        receiptAndIssue().setReceivingWareHouseId(1);
        receiptAndIssue().setPersonInChargeId(1);
        receiptAndIssue().setPetitionerId(1);
        receiptAndIssue().setTotalCost(0.0);
        receiptAndIssue().setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptAndIssue().setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 2).getTime());
        receiptAndIssue().setStatusId(1);
        receiptAndIssue().setNox(Boolean.valueOf("True"));
        receiptAndIssue().setSupplierId(1);
        receiptAndIssue().setTransfer(Boolean.valueOf("true"));
        return receiptAndIssue();
    }

    @Test
    void testCreateExport() {
            
        final ExportRequest exportRequest = new ExportRequest();
        exportRequest.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        exportRequest.setPetitionerId(1);
        final ProductExRequest productExRequest = new ProductExRequest();
        productExRequest.setProductId(1);
        productExRequest.setNumberOfRequest(1);
        exportRequest.setProductExRequests(Arrays.asList(productExRequest));

            
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

            
        final ReceiptAndIssue receiptAndIssue = receiptAndIssue();
        when(mockModelMapper.map(any(Object.class), eq(ReceiptAndIssue.class))).thenReturn(receiptAndIssue);

            
        final ReceiptAndIssue receiptAndIssue1 = receiptAndIssue();
        when(mockReceiptIssueRepository.save(any(ReceiptAndIssue.class))).thenReturn(receiptAndIssue1);

            
        exportServiceImplUnderTest.createExport(exportRequest);

            
        verify(mockReceiptIssueRepository).insertIssue_Product(1, 1, 1);
    }

    @Test
    void testCreateExport_StaffRepositoryReturnsAbsent() {
            
        final ExportRequest exportRequest = new ExportRequest();
        exportRequest.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        exportRequest.setPetitionerId(1);
        final ProductExRequest productExRequest = new ProductExRequest();
        productExRequest.setProductId(1);
        productExRequest.setNumberOfRequest(1);
        exportRequest.setProductExRequests(Arrays.asList(productExRequest));

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

            
        assertThatThrownBy(() -> exportServiceImplUnderTest.createExport(exportRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetAllExport() {
            
        when(mockReceiptIssueRepository.getExportsByIdBranch(1, false)).thenReturn(Arrays.asList());

            
        final List<IShortExport> result = exportServiceImplUnderTest.getAllExport(1);

            
    }

    @Test
    void testGetAllExport_ReceiptIssueRepositoryReturnsNoItems() {
            
        when(mockReceiptIssueRepository.getExportsByIdBranch(1, false)).thenReturn(Collections.emptyList());

            
        final List<IShortExport> result = exportServiceImplUnderTest.getAllExport(1);

            
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetExportByStatus() {
            
        when(mockReceiptIssueRepository.getExportsByIdBranchAndStatus(1, true, 1)).thenReturn(Arrays.asList());

            
        final List<IShortExport> result = exportServiceImplUnderTest.getExportByStatus(1, 1);

            
    }

    @Test
    void testGetExportByStatus_ReceiptIssueRepositoryReturnsNoItems() {
            
        when(mockReceiptIssueRepository.getExportsByIdBranchAndStatus(1, true, 1)).thenReturn(Collections.emptyList());

            
        final List<IShortExport> result = exportServiceImplUnderTest.getExportByStatus(1, 1);

            
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetProductsQuantityById() {
            
        when(mockReceiptIssueRepository.getProductsQuantity(1)).thenReturn(Arrays.asList());

            
        final List<IProductQuantity> result = exportServiceImplUnderTest.getProductsQuantityById(1);

            
    }

    @Test
    void testGetProductsQuantityById_ReceiptIssueRepositoryReturnsNoItems() {
            
        when(mockReceiptIssueRepository.getProductsQuantity(1)).thenReturn(Collections.emptyList());

            
        final List<IProductQuantity> result = exportServiceImplUnderTest.getProductsQuantityById(1);

            
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetProductQuantityVyId() {
            
        when(mockReceiptIssueRepository.getProductQuantity(1, 1)).thenReturn(null);

            
        final IProductQuantity result = exportServiceImplUnderTest.getProductQuantityVyId(1, 1);

            
    }

    @Test
    void testGetProductsByIdStore() {
            
        when(mockReceiptIssueRepository.getProductsByIdStoreHouse(1)).thenReturn(Arrays.asList());

            
        final List<IProduct> result = exportServiceImplUnderTest.getProductsByIdStore(1);

            
    }

    @Test
    void testGetProductsByIdStore_ReceiptIssueRepositoryReturnsNoItems() {
            
        when(mockReceiptIssueRepository.getProductsByIdStoreHouse(1)).thenReturn(Collections.emptyList());

            
        final List<IProduct> result = exportServiceImplUnderTest.getProductsByIdStore(1);

            
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testExportProducts() {
            
        final ExportProductsRequest exportProductsRequest = new ExportProductsRequest();
        exportProductsRequest.setIdExport(1);
        exportProductsRequest.setStoreHouseId(1);
        exportProductsRequest.setPersonInChargeId(1);
        exportProductsRequest.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductExportRequest productExportRequest = new ProductExportRequest();
        productExportRequest.setIdProduct(1);
        productExportRequest.setAmount(1);
        productExportRequest.setNote("note");
        exportProductsRequest.setProductExportRequests(Arrays.asList(productExportRequest));

            
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(0)).thenReturn(receiptAndIssue);

            
        final Optional<StoreHouse> storeHouse = Optional.of(
                new StoreHouse(1, "storeHouseName", "abbreviationName", "address", false, 0, "location"));
        when(mockStoreHouseRepository.findById(1)).thenReturn(storeHouse);

            
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

            
        final ReceiptAndIssue receiptAndIssue1 = receiptAndIssue();
        when(mockReceiptIssueRepository.save(any(ReceiptAndIssue.class))).thenReturn(receiptAndIssue1);

            
        exportServiceImplUnderTest.exportProducts(exportProductsRequest);

            
        verify(mockReceiptIssueRepository).save(any(ReceiptAndIssue.class));
        verify(mockReceiptIssueRepository).updateIssue_Product(1, 1, 1, "note");
    }

    @Test
    void testExportProducts_ReceiptIssueRepositoryFindByIdReturnsAbsent() {
            
        final ExportProductsRequest exportProductsRequest = new ExportProductsRequest();
        exportProductsRequest.setIdExport(1);
        exportProductsRequest.setStoreHouseId(1);
        exportProductsRequest.setPersonInChargeId(1);
        exportProductsRequest.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductExportRequest productExportRequest = new ProductExportRequest();
        productExportRequest.setIdProduct(1);
        productExportRequest.setAmount(1);
        productExportRequest.setNote("note");
        exportProductsRequest.setProductExportRequests(Arrays.asList(productExportRequest));

        when(mockReceiptIssueRepository.findById(1)).thenReturn(Optional.empty());

            
        assertThatThrownBy(() -> exportServiceImplUnderTest.exportProducts(exportProductsRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testExportProducts_StoreHouseRepositoryReturnsAbsent() {
            
        final ExportProductsRequest exportProductsRequest = new ExportProductsRequest();
        exportProductsRequest.setIdExport(1);
        exportProductsRequest.setStoreHouseId(1);
        exportProductsRequest.setPersonInChargeId(1);
        exportProductsRequest.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductExportRequest productExportRequest = new ProductExportRequest();
        productExportRequest.setIdProduct(1);
        productExportRequest.setAmount(1);
        productExportRequest.setNote("note");
        exportProductsRequest.setProductExportRequests(Arrays.asList(productExportRequest));

            
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

        when(mockStoreHouseRepository.findById(1)).thenReturn(Optional.empty());

            
        assertThatThrownBy(() -> exportServiceImplUnderTest.exportProducts(exportProductsRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testExportProducts_StaffRepositoryReturnsAbsent() {
            
        final ExportProductsRequest exportProductsRequest = new ExportProductsRequest();
        exportProductsRequest.setIdExport(1);
        exportProductsRequest.setStoreHouseId(1);
        exportProductsRequest.setPersonInChargeId(1);
        exportProductsRequest.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductExportRequest productExportRequest = new ProductExportRequest();
        productExportRequest.setIdProduct(1);
        productExportRequest.setAmount(1);
        productExportRequest.setNote("note");
        exportProductsRequest.setProductExportRequests(Arrays.asList(productExportRequest));

            
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

            
        final Optional<StoreHouse> storeHouse = Optional.of(
                new StoreHouse(1, "storeHouseName", "abbreviationName", "address", true, 1, "location"));
        when(mockStoreHouseRepository.findById(1)).thenReturn(storeHouse);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

            
        assertThatThrownBy(() -> exportServiceImplUnderTest.exportProducts(exportProductsRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetExportShortById() {
            
        when(mockReceiptIssueRepository.getShortExport(1)).thenReturn(null);

            
        final ExportShortResponse exportShortResponse = new ExportShortResponse();
        exportShortResponse.setReceiptIssueId(1);
        exportShortResponse.setPetitionerName("petitionerName");
        exportShortResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        exportShortResponse.setIProductExportResponses(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ExportShortResponse.class))).thenReturn(exportShortResponse);

        when(mockReceiptIssueRepository.getProductsExportById(1)).thenReturn(Arrays.asList());

            
        final ExportShortResponse result = exportServiceImplUnderTest.getExportShortById(1);

            
    }

    @Test
    void testGetExportShortById_ReceiptIssueRepositoryGetShortExportReturnsNull() {
            
        when(mockReceiptIssueRepository.getShortExport(1)).thenReturn(null);

            
        assertThatThrownBy(() -> exportServiceImplUnderTest.getExportShortById(1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetExportShortById_ReceiptIssueRepositoryGetProductsExportByIdReturnsNoItems() {
            
        when(mockReceiptIssueRepository.getShortExport(1)).thenReturn(null);

            
        final ExportShortResponse exportShortResponse = new ExportShortResponse();
        exportShortResponse.setReceiptIssueId(1);
        exportShortResponse.setPetitionerName("petitionerName");
        exportShortResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        exportShortResponse.setIProductExportResponses(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ExportShortResponse.class))).thenReturn(exportShortResponse);

        when(mockReceiptIssueRepository.getProductsExportById(1)).thenReturn(Collections.emptyList());

            
        final ExportShortResponse result = exportServiceImplUnderTest.getExportShortById(1);

            
    }

    @Test
    void testGetExportDetailById() {
            
        when(mockReceiptIssueRepository.getFullExport(1)).thenReturn(null);

            
        final ExportDetailResponse exportDetailResponse = new ExportDetailResponse();
        exportDetailResponse.setReceiptIssueId(1);
        exportDetailResponse.setStoreHouseId(1);
        exportDetailResponse.setStoreHouseName("storeHouseName");
        exportDetailResponse.setPetitionerName("petitionerName");
        exportDetailResponse.setPersonInCharge("personInCharge");
        exportDetailResponse.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        exportDetailResponse.setIProductExportResponses(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ExportDetailResponse.class))).thenReturn(exportDetailResponse);

        when(mockReceiptIssueRepository.getProductsExportById(1)).thenReturn(Arrays.asList());

            
        final ExportDetailResponse result = exportServiceImplUnderTest.getExportDetailById(1);

            
    }

    @Test
    void testGetExportDetailById_ReceiptIssueRepositoryGetFullExportReturnsNull() {
            
        when(mockReceiptIssueRepository.getFullExport(1)).thenReturn(null);

            
        assertThatThrownBy(() -> exportServiceImplUnderTest.getExportDetailById(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetExportDetailById_ReceiptIssueRepositoryGetProductsExportByIdReturnsNoItems() {
            
        when(mockReceiptIssueRepository.getFullExport(1)).thenReturn(null);

            
        final ExportDetailResponse exportDetailResponse = new ExportDetailResponse();
        exportDetailResponse.setReceiptIssueId(1);
        exportDetailResponse.setStoreHouseId(1);
        exportDetailResponse.setStoreHouseName("storeHouseName");
        exportDetailResponse.setPetitionerName("petitionerName");
        exportDetailResponse.setPersonInCharge("personInCharge");
        exportDetailResponse.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        exportDetailResponse.setIProductExportResponses(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ExportDetailResponse.class))).thenReturn(exportDetailResponse);

        when(mockReceiptIssueRepository.getProductsExportById(1)).thenReturn(Collections.emptyList());

            
        final ExportDetailResponse result = exportServiceImplUnderTest.getExportDetailById(1);

            
    }
}
