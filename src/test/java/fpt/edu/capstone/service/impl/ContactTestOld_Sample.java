//package fpt.edu.capstone.service.impl;
//
//import fpt.edu.capstone.dto.account.ChangePwRequest;
//import fpt.edu.capstone.dto.account.ContactRequest;
//import fpt.edu.capstone.dto.account.LoginRequest;
//import fpt.edu.capstone.dto.contact.IContact;
//import fpt.edu.capstone.dto.patient.MedicalRecordResponse;
//import fpt.edu.capstone.dto.patient.PatientResponse;
//import fpt.edu.capstone.dto.staff.StaffRequest;
//import fpt.edu.capstone.entity.*;
//import fpt.edu.capstone.exception.ClinicException;
//import fpt.edu.capstone.repository.*;
//import fpt.edu.capstone.utils.ResponseDataPagination;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.modelmapper.ModelMapper;
//import org.springframework.data.domain.Page;
//import org.springframework.data.domain.PageImpl;
//import org.springframework.data.domain.Pageable;
//
//import java.util.*;
//
//import static org.assertj.core.api.Assertions.assertThat;
//import static org.assertj.core.api.Assertions.assertThatThrownBy;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(MockitoExtension.class)
//class ContactServiceImplTest {
//
//    @Mock
//    private ContactRepository mockContactRepository;
//    @Mock
//    private MedicalReportRepository mockMedicalReportRepository;
//    @Mock
//    private ConsultingRoomRepository mockConsultingRoomRepository;
//    @Mock
//    private MedicalRecordRepository mockMedicalRecordRepository;
//    @Mock
//    private MedicalReportServiceRepository mockRServiceRepository;
//    @Mock
//    private MedicalServiceRepository mockMedicalServiceRepository;
//    @Mock
//    private InvoiceRepository mockInvoiceRepository;
//    @Mock
//    private InvoiceDetailRepository mockInvoiceDetailRepository;
//    @Mock
//    private StaffRepository mockStaffRepository;
//    @Mock
//    private BranchRepository mockBranchRepository;
//    @Mock
//    private ModelMapper mockModelMapper;
//    @Mock
//    private AuthenticationRepository mockLoginRepository;
//
//    private ContactServiceImpl contactServiceImplUnderTest;
//
//    @BeforeEach
//    void setUp() {
//        contactServiceImplUnderTest = new ContactServiceImpl(mockContactRepository, mockMedicalReportRepository,
//                mockConsultingRoomRepository, mockMedicalRecordRepository, mockRServiceRepository,
//                mockMedicalServiceRepository, mockInvoiceRepository, mockInvoiceDetailRepository, mockStaffRepository,
//                mockBranchRepository, mockModelMapper, mockLoginRepository);
//    }
//    public Contact contact(){
//        Contact contact = new Contact();
//        contact.setAccountId(1);
//        contact.setFullName("Nguyen Quang Vinh");
//        contact.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
//        contact.setAddress("Hanoi");
//        contact.setVillage("Cau Giay");
//        contact.setDistrict("128 Cau Giay");
//        contact.setProvince("Cau Giay");
//        contact.setSex("Male");
//        contact.setIdentityCard("415213212");
//        contact.setPhoneNumber("1967445451");
//        contact.setEthnicity("Eth");
//        contact.setJob("job");
//        contact.setStatus(true);
//        contact.setRoleId(1);
//        contact.setEmail("nqv@gmail.com");
//        return contact;
//    }
//
//    public ConsultingRoom consultingRoom(){
//        ConsultingRoom consultingRoom = new ConsultingRoom();
//        consultingRoom.setConsultingRoomId(1);
//        consultingRoom.setRoomName("Kham ABC");
//        consultingRoom.setAbbreviationName("ABC");
//        consultingRoom.setBranchId(1);
//        consultingRoom.setAction(true);
//        return consultingRoom;
//    }
//
////    public MedicalReport medicalReport(){
////        MedicalReport medicalReport = new MedicalReport();
////        medicalReport.setMedicalReportId(1);
////        medicalReport.setDate(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
////        medicalReport.setAccountId(1);
////        medicalReport.setConsultingRoomId(1);
////        medicalReport.setFinishedExamination(true);
////        medicalReport.setMedicalRecordId(1);
////        return medicalReport;
////    }
//
//    public StaffRequest staffRequest (){
//        StaffRequest staffRequest = new StaffRequest();
//        staffRequest.setFullName("fullName");
//        staffRequest.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
//        staffRequest.setAddress("address");
//        staffRequest.setVillage("village");
//        staffRequest.setDistrict("district");
//        staffRequest.setProvince("province");
//        staffRequest.setSex("sex");
//        staffRequest.setIdentityCard("identityCard");
//        staffRequest.setPhoneNumber("phoneNumber");
//        staffRequest.setEthnicity("ethnicity");
//        staffRequest.setJob("job");
//        staffRequest.setEmail("email");
//        staffRequest.setUserName("username");
//        staffRequest.setPassWord("password");
//        staffRequest.setConfirmPassWord("password");
//        staffRequest.setBranchId(1);
//        staffRequest.setRoleId(1);
//        staffRequest.setIdConsultingRoom(1);
//        staffRequest.setPosition("position");
//        return staffRequest;
//    }
//
//    public ContactRequest contactRequest (){
//        ContactRequest contact = new ContactRequest();
//        contact.setFullName("Nguyen Quang Vinh");
//        contact.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
//        contact.setAddress("Hanoi");
//        contact.setVillage("Cau Giay");
//        contact.setDistrict("128 Cau Giay");
//        contact.setProvince("Cau Giay");
//        contact.setSex("Male");
//        contact.setIdentityCard("415213212");
//        contact.setPhoneNumber("1967445451");
//        contact.setEthnicity("Eth");
//        contact.setJob("job");
//        contact.setEmail("nqv@gmail.com");
//        return contact;
//    }
//
//    public Account account(){
//        Account account = new Account();
//        account.setUsername("username");
//        account.setPassword("123");
//        account.setAccountId(1);
//        return account;
//    }
//
//    public Staff staff(){
//        Staff staff = new Staff();
//        staff.setStaffId(1);
//        staff.setAvatar("avatar.com");
//        staff.setCertificate("cert");
//        staff.setEducation("FPT");
//        staff.setBranchId(1);
//        return staff;
//    }
//
//    public MedicalRecordResponse medicalRecordResponse(){
//        MedicalRecordResponse medicalRecordResponse = new MedicalRecordResponse();
//        medicalRecordResponse.setAccountId(1);
//        medicalRecordResponse.setFullName("fullName");
//        medicalRecordResponse.setDob(new GregorianCalendar(2022, Calendar.JANUARY, 1).getTime());
//        medicalRecordResponse.setAddress("address");
//        medicalRecordResponse.setVillage("village");
//        medicalRecordResponse.setDistrict("district");
//        medicalRecordResponse.setProvince("province");
//        medicalRecordResponse.setSex("sex");
//        medicalRecordResponse.setIdentityCard("identityCard");
//        medicalRecordResponse.setPhoneNumber("phoneNumber");
//        medicalRecordResponse.setEthnicity("ethnicity");
//        medicalRecordResponse.setJob("job");
//        medicalRecordResponse.setEmail("email");
//        medicalRecordResponse.setMedicalRecordId(1);
//        medicalRecordResponse.setCircuit(1);
//        return medicalRecordResponse;
//    }
//
////    @Test
////    void testFindPatientByConsultingRoom() {
////        final List<MedicalReport> medicalReports = Arrays.asList(medicalReport());
////        when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomId(1)).thenReturn(medicalReports);
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final List<PatientResponse> result = contactServiceImplUnderTest.findPatientByConsultingRoom(1);
////    }
////
////    @Test
////    void testFindPatientByConsultingRoom_MedicalReportRepositoryReturnsNoItems() {
////        when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomId(1))
////                .thenReturn(Collections.emptyList());
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final List<PatientResponse> result = contactServiceImplUnderTest.findPatientByConsultingRoom(1);
////        assertThat(result).isEqualTo(Collections.emptyList());
////    }
////
////    @Test
////    void testFindPatientByConsultingRoom_ContactRepositoryReturnsAbsent() {
////        final List<MedicalReport> medicalReports = Arrays.asList(medicalReport());
////        when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomId(1)).thenReturn(medicalReports);
////        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(Optional.empty());
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final List<PatientResponse> result = contactServiceImplUnderTest.findPatientByConsultingRoom(1);
////    }
//
////    @Test
////    void testFindPatientByConsultingRoom_ConsultingRoomRepositoryReturnsAbsent() {
////        final List<MedicalReport> medicalReports = Arrays.asList(medicalReport());
////        when(mockMedicalReportRepository.findByFinishedExaminationAndConsultingRoomId(1)).thenReturn(medicalReports);
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(Optional.empty());
////        final List<PatientResponse> result = contactServiceImplUnderTest.findPatientByConsultingRoom(1);
////    }
//
////    @Test
////    void testGetAllPatient() {
////        final List<MedicalReport> medicalReports = Arrays.asList(medicalReport());
////        when(mockMedicalReportRepository.findReportByFinishedExamination(false)).thenReturn(medicalReports);
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final List<PatientResponse> result = contactServiceImplUnderTest.getAllPatient();
////    }
//
//    @Test
//    void testGetAllPatient_MedicalReportRepositoryReturnsNoItems() {
//        when(mockMedicalReportRepository.findReportByFinishedExamination(false)).thenReturn(Collections.emptyList());
//        final Optional<Contact> contact = Optional.of(contact());
//        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);
//        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
//        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
//        final List<PatientResponse> result = contactServiceImplUnderTest.getAllPatient();
//        assertThat(result).isEqualTo(Collections.emptyList());
//    }
////
////    @Test
////    void testGetAllPatient_ContactRepositoryReturnsAbsent() {
////        final List<MedicalReport> medicalReports = Arrays.asList(medicalReport());
////        when(mockMedicalReportRepository.findReportByFinishedExamination(false)).thenReturn(medicalReports);
////
////        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(Optional.empty());
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final List<PatientResponse> result = contactServiceImplUnderTest.getAllPatient();
////    }
////
////    @Test
////    void testGetAllPatient_ConsultingRoomRepositoryReturnsAbsent() {
////        final List<MedicalReport> medicalReports = Arrays.asList(medicalReport());
////        when(mockMedicalReportRepository.findReportByFinishedExamination(false)).thenReturn(medicalReports);
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findByAccountIdAndStatus(1, true)).thenReturn(contact);
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(Optional.empty());
////        final List<PatientResponse> result = contactServiceImplUnderTest.getAllPatient();
////    }
//
//    @Test
//    void testFindContactByAccountId() {
//        final Optional<Contact> contact = Optional.of(contact());
//        when(mockContactRepository.findById(1)).thenReturn(contact);
//        final Contact result = contactServiceImplUnderTest.findContactByAccountId(1);
//    }
//
//    @Test
//    void testFindContactByAccountId_ContactRepositoryReturnsAbsent() {
//        when(mockContactRepository.findById(1)).thenReturn(Optional.empty());
//        assertThatThrownBy(() -> contactServiceImplUnderTest.findContactByAccountId(1))
//                .isInstanceOf(NoSuchElementException.class);
//    }
//
////    @Test
////    void testFindContactByRoleIdAndStatusT() {
////        final List<Contact> contactList = Arrays.asList(contact());
////        when(mockContactRepository.findContactByRoleIdAndStatusT(1)).thenReturn(contactList);
////        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleIdAndStatusT(1);
////    }
////
////    @Test
////    void testFindContactByRoleIdAndStatusT_ContactRepositoryReturnsNoItems() {
////        when(mockContactRepository.findContactByRoleIdAndStatusT(1)).thenReturn(Collections.emptyList());
////        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleIdAndStatusT(1);
////        assertThat(result).isEqualTo(Collections.emptyList());
////    }
//
//    @Test
//    void testFindContactByRoleIdAndStatusF() {
//        final List<Contact> contactList = Arrays.asList(contact());
//        when(mockContactRepository.findContactByRoleIdAndStatusF(1)).thenReturn(contactList);
//        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleIdAndStatusF(1);
//    }
//
//    @Test
//    void testFindContactByRoleIdAndStatusF_ContactRepositoryReturnsNoItems() {
//        when(mockContactRepository.findContactByRoleIdAndStatusF(1)).thenReturn(Collections.emptyList());
//        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleIdAndStatusF(1);
//        assertThat(result).isEqualTo(Collections.emptyList());
//    }
//
//    @Test
//    void testFindContactByRoleId() {
//        final List<Contact> contactList = Arrays.asList(contact());
//        when(mockContactRepository.findContactByRoleId(1)).thenReturn(contactList);
//        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleId(1);
//    }
//
//    @Test
//    void testFindContactByRoleId_ContactRepositoryReturnsNoItems() {
//        when(mockContactRepository.findContactByRoleId(1)).thenReturn(Collections.emptyList());
//        final List<Contact> result = contactServiceImplUnderTest.findContactByRoleId(1);
//        assertThat(result).isEqualTo(Collections.emptyList());
//    }
//
//    @Test
//    void testContactRequestToContact() {
//        final ContactRequest contactRequest = contactRequest();
//        final Contact contact = contact();
//        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);
//        final Contact result = contactServiceImplUnderTest.ContactRequestToContact(contactRequest);
//    }
//
//    @Test
//    void testContactRequestToStaff() {
//        final StaffRequest staffRequest = staffRequest();
//        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
//        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
//        final Optional<Account> account = Optional.of(account());
//        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(account);
//        final Contact contact = contact();
//        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);
//
//        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());
//        final Staff staff = staff();
//        when(mockStaffRepository.save(any(Staff.class))).thenReturn(staff);
//        contactServiceImplUnderTest.ContactRequestToStaff(staffRequest);
//        verify(mockLoginRepository).save(any(Account.class));
//        verify(mockStaffRepository).insertStaff_Consulting(1, 1, "position");
//    }
//
//    @Test
//    void testContactRequestToStaff_ConsultingRoomRepositoryReturnsAbsent() {
//        final StaffRequest staffRequest = staffRequest();
//        when(mockConsultingRoomRepository.findById(1)).thenReturn(Optional.empty());
//        assertThatThrownBy(() -> contactServiceImplUnderTest.ContactRequestToStaff(staffRequest))
//                .isInstanceOf(RuntimeException.class);
//    }
//
//    @Test
//    void testContactRequestToStaff_AuthenticationRepositoryFindAccountsByUsernameReturnsAbsent() {
//        final StaffRequest staffRequest = staffRequest();
//        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
//        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
//
//        when(mockLoginRepository.findAccountsByUsername("username")).thenReturn(Optional.empty());
//        final Contact contact = contact();
//        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);
//        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());
//        final Staff staff = staff();
//        when(mockStaffRepository.save(any(Staff.class))).thenReturn(staff);
//        contactServiceImplUnderTest.ContactRequestToStaff(staffRequest);
//        verify(mockLoginRepository).save(any(Account.class));
//        verify(mockStaffRepository).insertStaff_Consulting(1, 1, "position");
//    }
//
//    @Test
//    void testUpdateContact() {
//        final ContactRequest contactRequest = contactRequest();
//        final Contact contact = contact();
//        when(mockContactRepository.getOne(1)).thenReturn(contact);
//        final Contact contact1 = contact();
//        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact1);
//        final Contact result = contactServiceImplUnderTest.updateContact(1, contactRequest);
//    }
//
//    @Test
//    void testUpdateStaff() {
//        final ContactRequest contactRequest = contactRequest();
//        final Contact contact = contact();
//        when(mockContactRepository.getOne(1)).thenReturn(contact);
//        final Contact contact1 = contact();
//        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact1);
//        final Contact result = contactServiceImplUnderTest.updateStaff(1, contactRequest);
//    }
//
//    public MedicalRecord medicalRecord(){
//        MedicalRecord medicalRecord = new MedicalRecord(1, 1, 1, 1, 1.1f,
//                1.1f, 1.1f, 1.1f, "pastMedicalHistory", "allergies", "note",
//                "icd11", "icd", "advice", new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime());
//        return medicalRecord;
//    }
//    @Test
//    void testFindPatientById() {
//        final Optional<Contact> contact = Optional.of(contact());
//        when(mockContactRepository.findById(1)).thenReturn(contact);
//        final Optional<MedicalRecord> medicalRecord = Optional.of(medicalRecord());
//        when(mockMedicalRecordRepository.findTopByAccountIdOrderByMedicalRecordIdDesc(1)).thenReturn(medicalRecord);
//        final MedicalRecordResponse medicalRecordResponse = medicalRecordResponse();
//        when(mockModelMapper.map(any(Object.class), eq(MedicalRecordResponse.class))).thenReturn(medicalRecordResponse);
//        final MedicalRecordResponse result = contactServiceImplUnderTest.findPatientById(1);
//    }
//
//    @Test
//    void testFindPatientById_ContactRepositoryReturnsAbsent() {
//        when(mockContactRepository.findById(1)).thenReturn(Optional.empty());
//        assertThatThrownBy(() -> contactServiceImplUnderTest.findPatientById(1)).isInstanceOf(ClinicException.class);
//    }
//
//    @Test
//    void testFindPatientById_MedicalRecordRepositoryReturnsAbsent() {
//        final Optional<Contact> contact = Optional.of(contact());
//        when(mockContactRepository.findById(1)).thenReturn(contact);
//        when(mockMedicalRecordRepository.findTopByAccountIdOrderByMedicalRecordIdDesc(1)).thenReturn(Optional.empty());
//
//        assertThatThrownBy(() -> contactServiceImplUnderTest.findPatientById(1)).isInstanceOf(ClinicException.class);
//    }
//
//    @Test
//    void testFindStaffByRoleId() {
//        final List<Contact> contactList = Arrays.asList(contact());
//        when(mockContactRepository.findStaffByRoleId(1)).thenReturn(contactList);
//        final List<Contact> result = contactServiceImplUnderTest.findStaffByRoleId(1);
//    }
//
//    @Test
//    void testFindStaffByRoleId_ContactRepositoryReturnsNoItems() {
//        when(mockContactRepository.findStaffByRoleId(1)).thenReturn(Collections.emptyList());
//        final List<Contact> result = contactServiceImplUnderTest.findStaffByRoleId(1);
//        assertThat(result).isEqualTo(Collections.emptyList());
//    }
//
////    @Test
////    void testGetDataDemo() {
////        when(mockContactRepository.getDataDemo(1)).thenReturn(null);
////        final IContact result = contactServiceImplUnderTest.getDataDemo(1);
////    }
////
////    @Test
////    void testFindStaffByRoleIdAndAccountId() {
////        final Contact contact = contact();
////        when(mockContactRepository.findStaffByRoleIdAndAccountId(1)).thenReturn(contact);
////        final Contact result = contactServiceImplUnderTest.findStaffByRoleIdAndAccountId(1);
////    }
//
//    @Test
//    void testFindStaffByName() {
//        final List<Contact> contactList = Arrays.asList(contact());
//        when(mockContactRepository.findStaffByName("fullName")).thenReturn(contactList);
//        final List<Contact> result = contactServiceImplUnderTest.findStaffByName("fullName");
//    }
//
//    @Test
//    void testFindStaffByName_ContactRepositoryReturnsNoItems() {
//        when(mockContactRepository.findStaffByName("fullName")).thenReturn(Collections.emptyList());
//        final List<Contact> result = contactServiceImplUnderTest.findStaffByName("fullName");
//        assertThat(result).isEqualTo(Collections.emptyList());
//    }
//
//    public MedicalService medicalService(){
//        MedicalService medicalService = new MedicalService(1, 1, "serviceName", 1.1);
//        return medicalService;
//    }
//
////    public MedicalReportService medicalReportService(){
////        MedicalReportService medicalReportService = new MedicalReportService(1, 1, 1, 1.1, false, 1);
////        return medicalReportService;
////    }
//
//    public Branch branch(){
//        Branch branch = new Branch(1, "address", "phoneNumber", false, "branchName");
//        return branch;
//    }
////    @Test
////    void testAddPatientService() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);
////
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        final Optional<MedicalService> medicalService = Optional.of(medicalService());
////        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);
////        final MedicalReportService medicalReportService = medicalReportService();
////        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);
////        contactServiceImplUnderTest.addPatientService(1, "note", 1, 1);
////        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
////        verify(mockMedicalReportRepository).updateConsultingId(1, 1);
////    }
////
////    @Test
////    void testAddPatientService_MedicalRecordRepositoryFindMedicalRecordNewestByAccountIdReturnsNull() {
////        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(null);
////        assertThatThrownBy(() -> contactServiceImplUnderTest.addPatientService(1, "note", 1, 1))
////                .isInstanceOf(ClinicException.class);
////    }
//
////    @Test
////    void testAddPatientService_MedicalRecordRepositoryFindMedicalReportServiceReturnsNull() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        assertThatThrownBy(() -> contactServiceImplUnderTest.addPatientService(1, "note", 1, 1))
////                .isInstanceOf(RuntimeException.class);
////        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
////    }
////
////    @Test
////    void testAddPatientService_MedicalServiceRepositoryReturnsAbsent() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.addPatientService(1, "note", 1, 1))
////                .isInstanceOf(ClinicException.class);
////        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
////    }
//
////    @Test
////    void testStartExaminationCreate() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final Optional<Staff> staff = Optional.of(staff());
////        when(mockStaffRepository.findById(1)).thenReturn(staff);
////        final Optional<Branch> branch = Optional.of(branch());
////        when(mockBranchRepository.findById(1)).thenReturn(branch);
////        final Contact contact = contact();
////        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);
////        final MedicalReport medicalReport = new MedicalReport(1,
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1, 1, false, 1);
////        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);
////        final Invoice invoice = new Invoice(1, 1, 1, 1.1, new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1,
////                false);
////        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);
////        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        final Optional<MedicalService> medicalService = Optional.of(medicalService());
////        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);
////        final MedicalReportService medicalReportService = medicalReportService();
////        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);
////        contactServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies", "pastMedicalHistory", 1, 1, 1,
////                1);
////    }
////
////    @Test
////    void testStartExaminationCreate_ConsultingRoomRepositoryReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationCreate_StaffRepositoryReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationCreate_BranchRepositoryReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final Optional<Staff> staff = Optional.of(staff());
////        when(mockStaffRepository.findById(1)).thenReturn(staff);
////        when(mockBranchRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationCreate_MedicalRecordRepositoryFindMedicalReportServiceReturnsNull() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final Optional<Staff> staff = Optional.of(staff());
////        when(mockStaffRepository.findById(1)).thenReturn(staff);
////        final Optional<Branch> branch = Optional.of(branch());
////        when(mockBranchRepository.findById(1)).thenReturn(branch);
////        final Contact contact = contact();
////        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);
////        final MedicalReport medicalReport = new MedicalReport(1,
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1, 1, false, 1);
////        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);
////        final Invoice invoice = new Invoice(1, 1, 1, 1.1, new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1,
////                false);
////        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);
////        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationCreate_MedicalServiceRepositoryReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final Optional<Staff> staff = Optional.of(staff());
////        when(mockStaffRepository.findById(1)).thenReturn(staff);
////        final Optional<Branch> branch = Optional.of(branch());
////        when(mockBranchRepository.findById(1)).thenReturn(branch);
////        final Contact contact = contact();
////        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact);
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);
////        final MedicalReport medicalReport = new MedicalReport(1,
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1, 1, false, 1);
////        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);
////        final Invoice invoice = new Invoice(1, 1, 1, 1.1, new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1,
////                false);
////        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);
////        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationCreate(contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(ClinicException.class);
////    }
////
////    @Test
////    void testStartExaminationUpdate() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findById(1)).thenReturn(contact);
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final Optional<Staff> staff = Optional.of(staff());
////        when(mockStaffRepository.findById(1)).thenReturn(staff);
////        final Optional<Branch> branch = Optional.of(branch());
////        when(mockBranchRepository.findById(1)).thenReturn(branch);
////        final Contact contact1 = contact();
////        when(mockContactRepository.getOne(1)).thenReturn(contact1);
////        final Contact contact2 = contact();
////        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact2);
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);
////        final MedicalReport medicalReport = new MedicalReport(1,
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1, 1, false, 1);
////        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);
////        final Invoice invoice = new Invoice(1, 1, 1, 1.1, new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1,
////                false);
////        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);
////        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        final Optional<MedicalService> medicalService = Optional.of(medicalService());
////        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);
////        final MedicalReportService medicalReportService = medicalReportService();
////        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);
////        contactServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies", "pastMedicalHistory", 1, 1,
////                1, 1);
////    }
////
////    @Test
////    void testStartExaminationUpdate_ContactRepositoryFindByIdReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        when(mockContactRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationUpdate_ConsultingRoomRepositoryReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findById(1)).thenReturn(contact);
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationUpdate_StaffRepositoryReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findById(1)).thenReturn(contact);
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationUpdate_BranchRepositoryReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findById(1)).thenReturn(contact);
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final Optional<Staff> staff = Optional.of(staff());
////        when(mockStaffRepository.findById(1)).thenReturn(staff);
////        when(mockBranchRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationUpdate_MedicalRecordRepositoryFindMedicalReportServiceReturnsNull() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findById(1)).thenReturn(contact);
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final Optional<Staff> staff = Optional.of(staff());
////        when(mockStaffRepository.findById(1)).thenReturn(staff);
////        final Optional<Branch> branch = Optional.of(branch());
////        when(mockBranchRepository.findById(1)).thenReturn(branch);
////        final Contact contact1 = contact();
////        when(mockContactRepository.getOne(1)).thenReturn(contact1);
////        final Contact contact2 = contact();
////        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact2);
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);
////        final MedicalReport medicalReport = new MedicalReport(1,
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1, 1, false, 1);
////        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);
////        final Invoice invoice = new Invoice(1, 1, 1, 1.1, new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1,
////                false);
////        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);
////        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testStartExaminationUpdate_MedicalServiceRepositoryReturnsAbsent() {
////        final ContactRequest contactRequest = contactRequest();
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findById(1)).thenReturn(contact);
////        final Optional<ConsultingRoom> consultingRoom = Optional.of(consultingRoom());
////        when(mockConsultingRoomRepository.findById(1)).thenReturn(consultingRoom);
////        final Optional<Staff> staff = Optional.of(staff());
////        when(mockStaffRepository.findById(1)).thenReturn(staff);
////        final Optional<Branch> branch = Optional.of(branch());
////        when(mockBranchRepository.findById(1)).thenReturn(branch);
////        final Contact contact1 = contact();
////        when(mockContactRepository.getOne(1)).thenReturn(contact1);
////        final Contact contact2 = contact();
////        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact2);
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.save(any(MedicalRecord.class))).thenReturn(medicalRecord);
////        final MedicalReport medicalReport = new MedicalReport(1,
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1, 1, false, 1);
////        when(mockMedicalReportRepository.save(any(MedicalReport.class))).thenReturn(medicalReport);
////        final Invoice invoice = new Invoice(1, 1, 1, 1.1, new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), 1,
////                false);
////        when(mockInvoiceRepository.save(any(Invoice.class))).thenReturn(invoice);
////        when(mockInvoiceDetailRepository.save(any(InvoiceDetail.class))).thenReturn(new InvoiceDetail(1, 1, 1));
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.startExaminationUpdate(1, contactRequest, "allergies",
////                "pastMedicalHistory", 1, 1, 1, 1)).isInstanceOf(ClinicException.class);
////    }
////
////    @Test
////    void testFinishExamination() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        final Optional<Contact> contact = Optional.of(contact());
////        when(mockContactRepository.findById(1)).thenReturn(contact);
////        final Contact contact1 = contact();
////        when(mockContactRepository.save(any(Contact.class))).thenReturn(contact1);
////        when(mockRServiceRepository.getSumCostService(1)).thenReturn(1.1);
////        contactServiceImplUnderTest.finishExamination(1, "note");
////        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
////        verify(mockMedicalReportRepository).updateFinishExamination(1, 1);
////        verify(mockContactRepository).save(any(Contact.class));
////        verify(mockInvoiceRepository).updateTotalCostInvoice(1.1, 1);
////    }
////
////    @Test
////    void testFinishExamination_MedicalRecordRepositoryFindMedicalRecordNewestByAccountIdReturnsNull() {
////        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(null);
////        assertThatThrownBy(() -> contactServiceImplUnderTest.finishExamination(1, "note"))
////                .isInstanceOf(ClinicException.class);
////    }
////
////    @Test
////    void testFinishExamination_MedicalRecordRepositoryFindMedicalReportServiceReturnsNull() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        assertThatThrownBy(() -> contactServiceImplUnderTest.finishExamination(1, "note"))
////                .isInstanceOf(RuntimeException.class);
////        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
////    }
////
////    @Test
////    void testFinishExamination_ContactRepositoryFindByIdReturnsAbsent() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalRecordNewestByAccountId(1)).thenReturn(medicalRecord);
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        when(mockContactRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.finishExamination(1, "note"))
////                .isInstanceOf(RuntimeException.class);
////        verify(mockMedicalRecordRepository).updatePastMedicalHistory("note", 1);
////        verify(mockMedicalReportRepository).updateFinishExamination(1, 1);
////    }
//
//    @Test
//    void testGetContactByNameAndStatus() {
//        final List<Contact> contactList = Arrays.asList(contact());
//        when(mockContactRepository.findContactByNameAndStatus("name", false)).thenReturn(contactList);
//        final List<Contact> result = contactServiceImplUnderTest.getContactByNameAndStatus("name", false);
//    }
//
//    @Test
//    void testGetContactByNameAndStatus_ContactRepositoryReturnsNoItems() {
//        when(mockContactRepository.findContactByNameAndStatus("name", false)).thenReturn(Collections.emptyList());
//        final List<Contact> result = contactServiceImplUnderTest.getContactByNameAndStatus("name", false);
//        assertThat(result).isEqualTo(Collections.emptyList());
//    }
////
////    @Test
////    void testGetContactsPaidByDate() {
////        final List<Contact> contactList = Arrays.asList(contact());
////        when(mockContactRepository.findContactPaymentByDate(
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime())).thenReturn(contactList);
////        final List<Contact> result = contactServiceImplUnderTest.getContactsPaidByDate(
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime());
////    }
////
////    @Test
////    void testGetContactsPaidByDate_ContactRepositoryReturnsNoItems() {
////        when(mockContactRepository.findContactPaymentByDate(
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime())).thenReturn(Collections.emptyList());
////        final List<Contact> result = contactServiceImplUnderTest.getContactsPaidByDate(
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime());
////        assertThat(result).isEqualTo(Collections.emptyList());
////    }
////
////    @Test
////    void testGetContactsPaidByDateAndName() {
////        final List<Contact> contactList = Arrays.asList(contact());
////        when(mockContactRepository.findContactPaymentByDateAndName(
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), "name")).thenReturn(contactList);
////        final List<Contact> result = contactServiceImplUnderTest.getContactsPaidByDateAndName(
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), "name");
////    }
////
////    @Test
////    void testGetContactsPaidByDateAndName_ContactRepositoryReturnsNoItems() {
////        when(mockContactRepository.findContactPaymentByDateAndName(
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), "name"))
////                .thenReturn(Collections.emptyList());
////        final List<Contact> result = contactServiceImplUnderTest.getContactsPaidByDateAndName(
////                new GregorianCalendar(2121, Calendar.JANUARY, 1).getTime(), "name");
////        assertThat(result).isEqualTo(Collections.emptyList());
////    }
//
//    @Test
//    void testGetListContact() {
//        final Page<Contact> contactPage = new PageImpl<>(Arrays.asList(contact()));
//        when(mockContactRepository.getAllContact(any(Pageable.class))).thenReturn(contactPage);
//        final ResponseDataPagination result = contactServiceImplUnderTest.getListContact(1, 1);
//    }
//
//    @Test
//    void testGetListContact_ContactRepositoryReturnsNoItems() {
//        when(mockContactRepository.getAllContact(any(Pageable.class)))
//                .thenReturn(new PageImpl<>(Collections.emptyList()));
//        final ResponseDataPagination result = contactServiceImplUnderTest.getListContact(1, 1);
//    }
//
//    @Test
//    void testSaveStaffAccount() {
//        final LoginRequest loginRequest = new LoginRequest();
//        loginRequest.setUsername("username");
//        loginRequest.setPassword("password");
//        when(mockLoginRepository.findAccountByUsername("username")).thenReturn(account());
//        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());
//        contactServiceImplUnderTest.saveStaffAccount(loginRequest);
//        verify(mockLoginRepository).save(any(Account.class));
//    }
//
//    @Test
//    void testSaveStaffAccount_AuthenticationRepositoryFindAccountByUsernameReturnsNull() {
//        final LoginRequest loginRequest = new LoginRequest();
//        loginRequest.setUsername("username");
//        loginRequest.setPassword("password");
//
//        when(mockLoginRepository.findAccountByUsername("username")).thenReturn(null);
//        assertThatThrownBy(() -> contactServiceImplUnderTest.saveStaffAccount(loginRequest))
//                .isInstanceOf(ClinicException.class);
//    }
//
//    @Test
//    void testChangePW() {
//        final ChangePwRequest changePwRequest = new ChangePwRequest();
//        changePwRequest.setUsername("username");
//        changePwRequest.setPassword("password");
//        changePwRequest.setNewPassword("password");
//        changePwRequest.setRenewPassword("renewPassword");
//        final Optional<Account> account = Optional.of(account());
//        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password")).thenReturn(account);
//        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());
//        contactServiceImplUnderTest.changePW(changePwRequest);
//        verify(mockLoginRepository).save(any(Account.class));
//    }
//
//    @Test
//    void testChangePW_AuthenticationRepositoryFindAccountsByUsernameAndPasswordReturnsNull() {
//        final ChangePwRequest changePwRequest = new ChangePwRequest();
//        changePwRequest.setUsername("username");
//        changePwRequest.setPassword("password");
//        changePwRequest.setNewPassword("password");
//        changePwRequest.setRenewPassword("password");
//        when(mockLoginRepository.findAccountsByUsernameAndPassword("username", "password"))
//                .thenReturn(Optional.empty());
//        when(mockLoginRepository.save(any(Account.class))).thenReturn(account());
//        contactServiceImplUnderTest.changePW(changePwRequest);
//        verify(mockLoginRepository).save(any(Account.class));
//    }
////
////    @Test
////    void testAddMedicalReportService() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        final Optional<MedicalService> medicalService = Optional.of(medicalService());
////        when(mockMedicalServiceRepository.findById(1)).thenReturn(medicalService);
////        final MedicalReportService medicalReportService = medicalReportService();
////        when(mockRServiceRepository.save(any(MedicalReportService.class))).thenReturn(medicalReportService);
////        contactServiceImplUnderTest.addMedicalReportService(medicalRecord, 1);
////    }
////
////    @Test
////    void testAddMedicalReportService_MedicalRecordRepositoryReturnsNull() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        assertThatThrownBy(() -> contactServiceImplUnderTest.addMedicalReportService(medicalRecord, 1))
////                .isInstanceOf(RuntimeException.class);
////    }
////
////    @Test
////    void testAddMedicalReportService_MedicalServiceRepositoryReturnsAbsent() {
////        final MedicalRecord medicalRecord = medicalRecord();
////        when(mockMedicalRecordRepository.findMedicalReportService(1)).thenReturn(null);
////        when(mockMedicalServiceRepository.findById(1)).thenReturn(Optional.empty());
////        assertThatThrownBy(() -> contactServiceImplUnderTest.addMedicalReportService(medicalRecord, 1))
////                .isInstanceOf(ClinicException.class);
////    }
//}
