package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.staff.StaffForChangePosition;
import fpt.edu.capstone.dto.staff.StaffName_ConsultingRoom;
import fpt.edu.capstone.dto.staff.Staff_ConsultingRoomRequestForEach;
import fpt.edu.capstone.dto.staff.Staff_ConsultingRoomRequestTo;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.repository.StaffRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class StaffServiceImplTest {

    @Mock
    private StaffRepository mockStaffRepository;

    private StaffServiceImpl staffServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        staffServiceImplUnderTest = new StaffServiceImpl(mockStaffRepository);
    }
    public Staff staff(){
        Staff staff = new Staff();
        staff.setStaffId(1);
        staff.setAvatar("avatar.com");
        staff.setCertificate("cert");
        staff.setEducation("FPT");
        staff.setBranchId(1);
        return staff;
    }
    @Test
    void testFindStaffByStaffId() {
                 
                 
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

                 
        final Staff result = staffServiceImplUnderTest.findStaffByStaffId(0);

                 
    }

    @Test
    void testFindStaffByStaffId_StaffRepositoryReturnsAbsent() {
                 
        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> staffServiceImplUnderTest.findStaffByStaffId(1))
                .isInstanceOf(NoSuchElementException.class);
    }

    @Test
    void testGetAllStaffName() {
                 
        when(mockStaffRepository.getListStaffName(1)).thenReturn(Arrays.asList());

                 
        final List<StaffName_ConsultingRoom> result = staffServiceImplUnderTest.getAllStaffName(1);

                 
    }

    @Test
    void testGetAllStaffName_StaffRepositoryReturnsNoItems() {
                 
        when(mockStaffRepository.getListStaffName(1)).thenReturn(Collections.emptyList());

                 
        final List<StaffName_ConsultingRoom> result = staffServiceImplUnderTest.getAllStaffName(1);

                 
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testUpdateStaffPosition() {
                 
        final Staff_ConsultingRoomRequestTo staff_consultingRoomRequestTo = new Staff_ConsultingRoomRequestTo();
        final Staff_ConsultingRoomRequestForEach staff_consultingRoomRequestForEach = new Staff_ConsultingRoomRequestForEach();
        staff_consultingRoomRequestForEach.setAccountId(1);
        staff_consultingRoomRequestForEach.setFullName("fullName");
        staff_consultingRoomRequestForEach.setPossition("possition");
        staff_consultingRoomRequestForEach.setRoomName("roomName");
        staff_consultingRoomRequestForEach.setConsultingRoomId(1);
        staff_consultingRoomRequestTo.setListStaffUpdate(Arrays.asList(staff_consultingRoomRequestForEach));

                 
        staffServiceImplUnderTest.updateStaffPosition(staff_consultingRoomRequestTo);

                 
        verify(mockStaffRepository).updatePositionStaff(1, "possition", 1);
    }

    @Test
    void testSave() {
                 
        final Staff staff = staff();

                 
        final Staff staff1 = staff();
        when(mockStaffRepository.save(any(Staff.class))).thenReturn(staff1);

                 
        staffServiceImplUnderTest.save(staff);

                 
        verify(mockStaffRepository).save(any(Staff.class));
    }

    @Test
    void testGetAllStaffForChange() {
                 
        when(mockStaffRepository.getAllStaffToChangePosition()).thenReturn(Arrays.asList());

                 
        final List<StaffForChangePosition> result = staffServiceImplUnderTest.getAllStaffForChange();

                 
    }

    @Test
    void testGetAllStaffForChange_StaffRepositoryReturnsNoItems() {
                 
        when(mockStaffRepository.getAllStaffToChangePosition()).thenReturn(Collections.emptyList());

                 
        final List<StaffForChangePosition> result = staffServiceImplUnderTest.getAllStaffForChange();

                 
        assertThat(result).isEqualTo(Collections.emptyList());
    }
}
