package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.unitgroup.UnitGroupRequest;
import fpt.edu.capstone.dto.unitgroup.UnitGroupsRequest;
import fpt.edu.capstone.entity.UnitGroup;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.UnitGroupRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UnitGroupServiceImplTest {

    @Mock
    private UnitGroupRepository mockUnitGroupRepository;

    private UnitGroupServiceImpl unitGroupServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        unitGroupServiceImplUnderTest = new UnitGroupServiceImpl(mockUnitGroupRepository);
    }
    public UnitGroup unitGroup(){
        unitGroup().setUnitGroupId(1);
        unitGroup().setName("name");
        unitGroup().setUse(Boolean.valueOf("false"));
        return unitGroup();
    }
    @Test
    void testGetListUnitGroup() {
        final List<UnitGroup> unitGroups = Arrays.asList(unitGroup());
        when(mockUnitGroupRepository.getAllUnitGroup()).thenReturn(unitGroups);

        final List<UnitGroup> result = unitGroupServiceImplUnderTest.getListUnitGroup();
        assertThat(result).isEqualTo(Collections.emptyList());

    }

    @Test
    void testGetListUnitGroup_UnitGroupRepositoryReturnsNoItems() {
        when(mockUnitGroupRepository.getAllUnitGroup()).thenReturn(Collections.emptyList());

        final List<UnitGroup> result = unitGroupServiceImplUnderTest.getListUnitGroup();

        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testCreateUnitGroup() {
        final UnitGroupRequest request = new UnitGroupRequest();
        request.setName("name");

        when(mockUnitGroupRepository.findByName("name")).thenReturn(unitGroup());
        when(mockUnitGroupRepository.save(any(UnitGroup.class))).thenReturn(unitGroup());

        unitGroupServiceImplUnderTest.createUnitGroup(request);

        verify(mockUnitGroupRepository).save(any(UnitGroup.class));
    }

    @Test
    void testCreateUnitGroup_UnitGroupRepositoryFindByNameReturnsNull() {

        final UnitGroupRequest request = new UnitGroupRequest();
        request.setName("name");

        when(mockUnitGroupRepository.findByName("name")).thenReturn(null);
        when(mockUnitGroupRepository.save(any(UnitGroup.class))).thenReturn(unitGroup());

        unitGroupServiceImplUnderTest.createUnitGroup(request);

        verify(mockUnitGroupRepository).save(any(UnitGroup.class));
    }

    @Test
    void testDeleteUnitGroup() {
        final UnitGroupsRequest unitGroupsRequest = new UnitGroupsRequest();
        unitGroupsRequest.setUnitGroupsId(Arrays.asList(1));

        final Optional<UnitGroup> unitGroup = Optional.of(unitGroup());
        when(mockUnitGroupRepository.findById(1)).thenReturn(unitGroup);

        unitGroupServiceImplUnderTest.deleteUnitGroup(unitGroupsRequest);

        verify(mockUnitGroupRepository).deleteUnitGroupById(1);
    }

    @Test
    void testDeleteUnitGroup_UnitGroupRepositoryFindByIdReturnsAbsent() {
        final UnitGroupsRequest unitGroupsRequest = new UnitGroupsRequest();
        unitGroupsRequest.setUnitGroupsId(Arrays.asList(1));

        when(mockUnitGroupRepository.findById(1)).thenReturn(Optional.empty());

        assertThatThrownBy(() -> unitGroupServiceImplUnderTest.deleteUnitGroup(unitGroupsRequest))
                .isInstanceOf(ClinicException.class);
    }
}
