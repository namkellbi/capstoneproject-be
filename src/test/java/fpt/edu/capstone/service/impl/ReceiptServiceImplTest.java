package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.medicine.ReProductRequest;
import fpt.edu.capstone.dto.product.ProductReRequest;
import fpt.edu.capstone.dto.receipt.*;
import fpt.edu.capstone.entity.*;
import fpt.edu.capstone.repository.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ReceiptServiceImplTest {

    @Mock
    private StoreHouseRepository mockStoreHouseRepository;
    @Mock
    private StaffRepository mockStaffRepository;
    @Mock
    private ModelMapper mockModelMapper;
    @Mock
    private ReceiptIssueRepository mockReceiptIssueRepository;
    @Mock
    private ProductRepository mockProductRepository;
    @Mock
    private CReceiptProductRepository mockCReceiptProductRepo;

    private ReceiptServiceImpl receiptServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        receiptServiceImplUnderTest = new ReceiptServiceImpl(mockStoreHouseRepository, mockStaffRepository,
                mockModelMapper, mockReceiptIssueRepository, mockProductRepository, mockCReceiptProductRepo);
    }
    public ReceiptAndIssue receiptAndIssue(){
        receiptAndIssue().setReceiptIssueId(1);
        receiptAndIssue().setWareHouseTransferId(1);
        receiptAndIssue().setReceivingWareHouseId(1);
        receiptAndIssue().setPersonInChargeId(1);
        receiptAndIssue().setPetitionerId(1);
        receiptAndIssue().setTotalCost(0.0);
        receiptAndIssue().setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptAndIssue().setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 2).getTime());
        receiptAndIssue().setStatusId(1);
        receiptAndIssue().setNox(Boolean.valueOf("True"));
        receiptAndIssue().setSupplierId(1);
        receiptAndIssue().setTransfer(Boolean.valueOf("true"));
        return receiptAndIssue();
    }
    public Staff staff(){
        Staff staff = new Staff();
        staff.setStaffId(1);
        staff.setAvatar("avatar.com");
        staff.setCertificate("cert");
        staff.setEducation("FPT");
        staff.setBranchId(1);
        return staff;
    }
    public StoreHouse storeHouse(){
        storeHouse().setStoreHouseId(1);
        storeHouse().setStoreHouseName("storeHouseName");
        storeHouse().setAbbreviationName("abbreviationName");
        storeHouse().setAddress("address");
        storeHouse().setAction(Boolean.valueOf("True"));
        storeHouse().setBranchId(1);
        storeHouse().setLocation("location");
        return storeHouse();
    }
    @Test
    void testCreateReceipt() {
                 
        final ReceiptRequest receiptRequest = new ReceiptRequest();
        receiptRequest.setReceivingWareHouseId(0);
        receiptRequest.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptRequest.setPetitionerId(0);
        receiptRequest.setSupplierId(0);
        final ProductReRequest productReRequest = new ProductReRequest();
        productReRequest.setProductId(0);
        productReRequest.setNumberOfRequest(0);
        productReRequest.setEntryPrice(0.0);
        receiptRequest.setProductReRequests(Arrays.asList(productReRequest));

                 
        final Optional<StoreHouse> storeHouse = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.findById(1)).thenReturn(storeHouse);

                 
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

                 
        final ReceiptAndIssue receiptAndIssue = receiptAndIssue();
        when(mockModelMapper.map(any(Object.class), eq(ReceiptAndIssue.class))).thenReturn(receiptAndIssue);

                 
        final ReceiptAndIssue receiptAndIssue1 = receiptAndIssue();
        when(mockReceiptIssueRepository.save(any(ReceiptAndIssue.class))).thenReturn(receiptAndIssue1);

                 
        receiptServiceImplUnderTest.createReceipt(receiptRequest);

                 
        verify(mockReceiptIssueRepository).insertReceipt_Product(1, 1, 1, 0.0);
    }

    @Test
    void testCreateReceipt_StoreHouseRepositoryReturnsAbsent() {
                 
        final ReceiptRequest receiptRequest = new ReceiptRequest();
        receiptRequest.setReceivingWareHouseId(1);
        receiptRequest.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptRequest.setPetitionerId(1);
        receiptRequest.setSupplierId(1);
        final ProductReRequest productReRequest = new ProductReRequest();
        productReRequest.setProductId(1);
        productReRequest.setNumberOfRequest(1);
        productReRequest.setEntryPrice(0.0);
        receiptRequest.setProductReRequests(Arrays.asList(productReRequest));

        when(mockStoreHouseRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> receiptServiceImplUnderTest.createReceipt(receiptRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testCreateReceipt_StaffRepositoryReturnsAbsent() {
                 
        final ReceiptRequest receiptRequest = new ReceiptRequest();
        receiptRequest.setReceivingWareHouseId(1);
        receiptRequest.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptRequest.setPetitionerId(1);
        receiptRequest.setSupplierId(1);
        final ProductReRequest productReRequest = new ProductReRequest();
        productReRequest.setProductId(1);
        productReRequest.setNumberOfRequest(1);
        productReRequest.setEntryPrice(0.0);
        receiptRequest.setProductReRequests(Arrays.asList(productReRequest));

                 
        final Optional<StoreHouse> storeHouse = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.findById(1)).thenReturn(storeHouse);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> receiptServiceImplUnderTest.createReceipt(receiptRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testCancelReceipt() {
                 
                 
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

                 
        final ReceiptAndIssue receiptAndIssue1 = receiptAndIssue();
        when(mockReceiptIssueRepository.save(any(ReceiptAndIssue.class))).thenReturn(receiptAndIssue1);

                 
        receiptServiceImplUnderTest.cancelReceipt(1);

                 
        verify(mockReceiptIssueRepository).save(any(ReceiptAndIssue.class));
    }

    @Test
    void testCancelReceipt_ReceiptIssueRepositoryFindByIdReturnsAbsent() {
                 
        when(mockReceiptIssueRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> receiptServiceImplUnderTest.cancelReceipt(1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testCompleteReceipt() {
                 
                 
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

                 
        final ReceiptAndIssue receiptAndIssue1 = receiptAndIssue();
        when(mockReceiptIssueRepository.save(any(ReceiptAndIssue.class))).thenReturn(receiptAndIssue1);

                 
        receiptServiceImplUnderTest.completeReceipt(1);

                 
        verify(mockReceiptIssueRepository).save(any(ReceiptAndIssue.class));
    }

    @Test
    void testCompleteReceipt_ReceiptIssueRepositoryFindByIdReturnsAbsent() {
                 
        when(mockReceiptIssueRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> receiptServiceImplUnderTest.completeReceipt(1)).isInstanceOf(RuntimeException.class);
    }

    @Test
    void testReceiptProduct() {
                 
        final ReceiptProductRequest receiptProductRequest = new ReceiptProductRequest();
        receiptProductRequest.setReceiptId(1);
        receiptProductRequest.setPersonInChargeId(1);
        receiptProductRequest.setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ReProductRequest reProductRequest = new ReProductRequest();
        reProductRequest.setProductId(1);
        reProductRequest.setAmount(1);
        reProductRequest.setNote("note");
        receiptProductRequest.setProductRequests(Arrays.asList(reProductRequest));

                 
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

                 
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

                 
        final CreateReceiptProduct createReceiptProduct = new CreateReceiptProduct(1, 1, 1,
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        when(mockCReceiptProductRepo.save(any(CreateReceiptProduct.class))).thenReturn(createReceiptProduct);

                 
        final Optional<Product> product = Optional.of(
                new Product(1, "productName", 0.0, 1, "note", "userObject", "using"));
        when(mockProductRepository.findById(1)).thenReturn(product);

        when(mockStoreHouseRepository.getProductInStore(1, 1)).thenReturn(null);

                 
        receiptServiceImplUnderTest.receiptProduct(receiptProductRequest);

                 
        verify(mockReceiptIssueRepository).insertReceiptProduct2(1, 1, 0, "note");
        verify(mockStoreHouseRepository).insertStore_Product(1, 1);
    }

    @Test
    void testReceiptProduct_ReceiptIssueRepositoryFindByIdReturnsAbsent() {
                 
        final ReceiptProductRequest receiptProductRequest = new ReceiptProductRequest();
        receiptProductRequest.setReceiptId(1);
        receiptProductRequest.setPersonInChargeId(1);
        receiptProductRequest.setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ReProductRequest reProductRequest = new ReProductRequest();
        reProductRequest.setProductId(1);
        reProductRequest.setAmount(1);
        reProductRequest.setNote("note");
        receiptProductRequest.setProductRequests(Arrays.asList(reProductRequest));

        when(mockReceiptIssueRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> receiptServiceImplUnderTest.receiptProduct(receiptProductRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testReceiptProduct_StaffRepositoryReturnsAbsent() {
                 
        final ReceiptProductRequest receiptProductRequest = new ReceiptProductRequest();
        receiptProductRequest.setReceiptId(1);
        receiptProductRequest.setPersonInChargeId(1);
        receiptProductRequest.setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ReProductRequest reProductRequest = new ReProductRequest();
        reProductRequest.setProductId(1);
        reProductRequest.setAmount(1);
        reProductRequest.setNote("note");
        receiptProductRequest.setProductRequests(Arrays.asList(reProductRequest));

                 
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> receiptServiceImplUnderTest.receiptProduct(receiptProductRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testReceiptProduct_ProductRepositoryReturnsAbsent() {
                 
        final ReceiptProductRequest receiptProductRequest = new ReceiptProductRequest();
        receiptProductRequest.setReceiptId(1);
        receiptProductRequest.setPersonInChargeId(1);
        receiptProductRequest.setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ReProductRequest reProductRequest = new ReProductRequest();
        reProductRequest.setProductId(1);
        reProductRequest.setAmount(1);
        reProductRequest.setNote("note");
        receiptProductRequest.setProductRequests(Arrays.asList(reProductRequest));

                 
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

                 
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

                 
        final CreateReceiptProduct createReceiptProduct = new CreateReceiptProduct(1, 1, 1,
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        when(mockCReceiptProductRepo.save(any(CreateReceiptProduct.class))).thenReturn(createReceiptProduct);

        when(mockProductRepository.findById(1)).thenReturn(Optional.empty());

                 
        assertThatThrownBy(() -> receiptServiceImplUnderTest.receiptProduct(receiptProductRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testReceiptProduct_StoreHouseRepositoryGetProductInStoreReturnsNull() {
                 
        final ReceiptProductRequest receiptProductRequest = new ReceiptProductRequest();
        receiptProductRequest.setReceiptId(1);
        receiptProductRequest.setPersonInChargeId(1);
        receiptProductRequest.setDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ReProductRequest reProductRequest = new ReProductRequest();
        reProductRequest.setProductId(1);
        reProductRequest.setAmount(1);
        reProductRequest.setNote("note");
        receiptProductRequest.setProductRequests(Arrays.asList(reProductRequest));

        
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

                 
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

                 
        final CreateReceiptProduct createReceiptProduct = new CreateReceiptProduct(1, 1, 1,
                new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        when(mockCReceiptProductRepo.save(any(CreateReceiptProduct.class))).thenReturn(createReceiptProduct);

                 
        final Optional<Product> product = Optional.of(
                new Product(1, "productName", 0.0, 1, "note", "userObject", "using"));
        when(mockProductRepository.findById(0)).thenReturn(product);

        when(mockStoreHouseRepository.getProductInStore(1, 1)).thenReturn(null);

                 
        receiptServiceImplUnderTest.receiptProduct(receiptProductRequest);

                 
        verify(mockReceiptIssueRepository).insertReceiptProduct2(1, 1, 1, "note");
        verify(mockStoreHouseRepository).insertStore_Product(1, 1);
    }

    @Test
    void testGetAllReceipt() {
                 
        when(mockReceiptIssueRepository.getReceiptsByIdBranch(1, true)).thenReturn(Arrays.asList());

                 
        final List<IShortReceipt> result = receiptServiceImplUnderTest.getAllReceipt(1);

                 
    }

    @Test
    void testGetAllReceipt_ReceiptIssueRepositoryReturnsNoItems() {
                 
        when(mockReceiptIssueRepository.getReceiptsByIdBranch(1, true)).thenReturn(Collections.emptyList());

                 
        final List<IShortReceipt> result = receiptServiceImplUnderTest.getAllReceipt(1);

                 
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetReceiptProductsById() {
                 
        when(mockReceiptIssueRepository.getReceiptsByIdReceipt(1)).thenReturn(null);

                 
        final ReceiptProductsResponse receiptProductsResponse = new ReceiptProductsResponse();
        receiptProductsResponse.setReceiptIssueId(1);
        receiptProductsResponse.setReceivingWarehouseID(1);
        receiptProductsResponse.setStoreHouseName("StoreHouseName");
        receiptProductsResponse.setSupplierName("SupplierName");
        receiptProductsResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptProductsResponse.setStatusID(1);
        receiptProductsResponse.setStatusName("StatusName");
        receiptProductsResponse.setProductsResponse(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ReceiptProductsResponse.class)))
                .thenReturn(receiptProductsResponse);

        when(mockReceiptIssueRepository.getProductsReceipt(1)).thenReturn(Arrays.asList());

                 
        final ReceiptProductsResponse result = receiptServiceImplUnderTest.getReceiptProductsById(1);

                 
    }

    @Test
    void testGetReceiptProductsById_ReceiptIssueRepositoryGetReceiptsByIdReceiptReturnsNull() {
                 
        when(mockReceiptIssueRepository.getReceiptsByIdReceipt(1)).thenReturn(null);

                 
        assertThatThrownBy(() -> receiptServiceImplUnderTest.getReceiptProductsById(0))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetReceiptProductsById_ReceiptIssueRepositoryGetProductsReceiptReturnsNoItems() {
                 
        when(mockReceiptIssueRepository.getReceiptsByIdReceipt(1)).thenReturn(null);

                 
        final ReceiptProductsResponse receiptProductsResponse = new ReceiptProductsResponse();
        receiptProductsResponse.setReceiptIssueId(1);
        receiptProductsResponse.setReceivingWarehouseID(1);
        receiptProductsResponse.setStoreHouseName("StoreHouseName");
        receiptProductsResponse.setSupplierName("SupplierName");
        receiptProductsResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptProductsResponse.setStatusID(1);
        receiptProductsResponse.setStatusName("StatusName");
        receiptProductsResponse.setProductsResponse(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(ReceiptProductsResponse.class)))
                .thenReturn(receiptProductsResponse);

        when(mockReceiptIssueRepository.getProductsReceipt(1)).thenReturn(Collections.emptyList());

                 
        final ReceiptProductsResponse result = receiptServiceImplUnderTest.getReceiptProductsById(1);

                 
    }

    @Test
    void testGetHistoryReceiptByIdReceipt() {
                 
        when(mockReceiptIssueRepository.getReceiptsHistory(1)).thenReturn(Arrays.asList());

                 
        final List<IReceiptHistoryResponse> result = receiptServiceImplUnderTest.getHistoryReceiptByIdReceipt(1);

                 
    }

    @Test
    void testGetHistoryReceiptByIdReceipt_ReceiptIssueRepositoryReturnsNoItems() {
                 
        when(mockReceiptIssueRepository.getReceiptsHistory(1)).thenReturn(Collections.emptyList());

                 
        final List<IReceiptHistoryResponse> result = receiptServiceImplUnderTest.getHistoryReceiptByIdReceipt(1);

                 
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetReceiptByStatus() {
                 
        when(mockReceiptIssueRepository.getReceiptsByIdBranchAndStatus(1, true, 1)).thenReturn(Arrays.asList());

                 
        final List<IShortReceipt> result = receiptServiceImplUnderTest.getReceiptByStatus(1, 1);

                 
    }

    @Test
    void testGetReceiptByStatus_ReceiptIssueRepositoryReturnsNoItems() {
                 
        when(mockReceiptIssueRepository.getReceiptsByIdBranchAndStatus(1, true, 1)).thenReturn(Collections.emptyList());

                 
        final List<IShortReceipt> result = receiptServiceImplUnderTest.getReceiptByStatus(1, 1);

                 
        assertThat(result).isEqualTo(Collections.emptyList());
    }
}
