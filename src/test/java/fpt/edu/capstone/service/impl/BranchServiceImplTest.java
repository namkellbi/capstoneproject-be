package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.branch.BranchRequest;
import fpt.edu.capstone.entity.Branch;
import fpt.edu.capstone.repository.BranchRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BranchServiceImplTest {

    @Mock
    private BranchRepository mockBranchRepository;

    private BranchServiceImpl branchServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        branchServiceImplUnderTest = new BranchServiceImpl(mockBranchRepository);
    }
    public Branch branch(){
        branch().setBranchId(1);
        branch().setBranchName("branchName");
        branch().setAction(Boolean.valueOf("false"));
        branch().setPhoneNumber("phoneNumber");
        branch().setAddress("address");
        return branch();
    }
    @Test
    void testGetById() {
        
        
        final Branch branch = branch();
        when(mockBranchRepository.findBranchById(1)).thenReturn(branch);

        
        final Branch result = branchServiceImplUnderTest.getById(1);

        
    }

    @Test
    void testGetAllBranch() {
        
        
        final List<Branch> branches = Arrays.asList(branch());
        when(mockBranchRepository.findByIsAction(true)).thenReturn(branches);

        
        final List<Branch> result = branchServiceImplUnderTest.getAllBranch();

        
    }

    @Test
    void testGetAllBranch_BranchRepositoryReturnsNoItems() {
        
        when(mockBranchRepository.findByIsAction(true)).thenReturn(Collections.emptyList());

        
        final List<Branch> result = branchServiceImplUnderTest.getAllBranch();

        
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetBranchByName() {
        
        
        final List<Branch> branches = Arrays.asList(branch());
        when(mockBranchRepository.findBranchByBranchName("branchName")).thenReturn(branches);

        
        final List<Branch> result = branchServiceImplUnderTest.getBranchByName("branchName");

        
    }

    @Test
    void testGetBranchByName_BranchRepositoryReturnsNoItems() {
        
        when(mockBranchRepository.findBranchByBranchName("branchName")).thenReturn(Collections.emptyList());

        
        final List<Branch> result = branchServiceImplUnderTest.getBranchByName("branchName");

        
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testDeleteBranchById() {
        
        
        final Branch branch = branch();
        when(mockBranchRepository.getById(1)).thenReturn(branch);

        
        final Branch branch1 = branch();
        when(mockBranchRepository.save(any(Branch.class))).thenReturn(branch1);

        
        final Branch result = branchServiceImplUnderTest.deleteBranchById(1);

        
    }

    @Test
    void testSaveBranch() {
        
        final BranchRequest branchRequest = new BranchRequest();
        branchRequest.setAddress("address");
        branchRequest.setPhoneNumber("phoneNumber");
        branchRequest.setBranchName("branchName");

        
        final Branch branch = branch();
        when(mockBranchRepository.save(any(Branch.class))).thenReturn(branch);

        
        final Branch result = branchServiceImplUnderTest.saveBranch(branchRequest);

        
    }

    @Test
    void testUpdateBranch() {
        
        final BranchRequest branchRequest = new BranchRequest();
        branchRequest.setAddress("address");
        branchRequest.setPhoneNumber("phoneNumber");
        branchRequest.setBranchName("branchName");

        
        final Branch branch = branch();
        when(mockBranchRepository.getById(0)).thenReturn(branch);

        
        final Branch branch1 = branch();
        when(mockBranchRepository.save(any(Branch.class))).thenReturn(branch1);

        
        final Branch result = branchServiceImplUnderTest.updateBranch(0, branchRequest);

        
    }
}
