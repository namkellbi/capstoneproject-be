package fpt.edu.capstone.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class EmailServiceImplTest {

    @Mock
    private JavaMailSender mockJavaMailSender;

    @InjectMocks
    private EmailServiceImpl emailServiceImplUnderTest;

    @Test
    void testSendForgotPasswordEmail() throws Exception {

        final MimeMessage mimeMessage = new MimeMessage(Session.getInstance(new Properties()));
        when(mockJavaMailSender.createMimeMessage()).thenReturn(mimeMessage);


        emailServiceImplUnderTest.sendForgotPasswordEmail("recipientEmail", "newPassword");


        verify(mockJavaMailSender).send(any(MimeMessage.class));
    }

    @Test
    void testSendForgotPasswordEmail_JavaMailSenderSendThrowsMailException() {

        final MimeMessage mimeMessage = new MimeMessage(Session.getInstance(new Properties()));
        when(mockJavaMailSender.createMimeMessage()).thenReturn(mimeMessage);

        doThrow(MailException.class).when(mockJavaMailSender).send(any(MimeMessage.class));


        assertThatThrownBy(
                () -> emailServiceImplUnderTest.sendForgotPasswordEmail("recipientEmail", "newPassword"))
                .isInstanceOf(Exception.class);
    }
}
