package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.export.ProductExRequest;
import fpt.edu.capstone.dto.export.ProductExportRequest;
import fpt.edu.capstone.dto.transfer.*;
import fpt.edu.capstone.entity.ReceiptAndIssue;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.entity.StoreHouse;
import fpt.edu.capstone.entity.Supplier;
import fpt.edu.capstone.repository.ReceiptIssueRepository;
import fpt.edu.capstone.repository.StaffRepository;
import fpt.edu.capstone.repository.StoreHouseRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransferServiceImplTest {

    @Mock
    private StoreHouseRepository mockStoreHouseRepository;
    @Mock
    private ReceiptIssueRepository mockReceiptIssueRepository;
    @Mock
    private StaffRepository mockStaffRepository;
    @Mock
    private ModelMapper mockModelMapper;

    private TransferServiceImpl transferServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        transferServiceImplUnderTest = new TransferServiceImpl(mockStoreHouseRepository, mockReceiptIssueRepository,
                mockStaffRepository, mockModelMapper);
    }
    public ReceiptAndIssue receiptAndIssue(){
        receiptAndIssue().setReceiptIssueId(1);
        receiptAndIssue().setWareHouseTransferId(1);
        receiptAndIssue().setReceivingWareHouseId(1);
        receiptAndIssue().setPersonInChargeId(1);
        receiptAndIssue().setPetitionerId(1);
        receiptAndIssue().setTotalCost(0.0);
        receiptAndIssue().setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        receiptAndIssue().setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 2).getTime());
        receiptAndIssue().setStatusId(1);
        receiptAndIssue().setNox(Boolean.valueOf("True"));
        receiptAndIssue().setSupplierId(1);
        receiptAndIssue().setTransfer(Boolean.valueOf("true"));
        return receiptAndIssue();
    }
    public Staff staff(){
        Staff staff = new Staff();
        staff.setStaffId(1);
        staff.setAvatar("avatar.com");
        staff.setCertificate("cert");
        staff.setEducation("FPT");
        staff.setBranchId(1);
        return staff;
    }
    public Supplier supplier(){
        supplier().setSupplierId(1);
        supplier().setSupplierName("supplierName");
        supplier().setAbbreviationName("abbreviationName");
        supplier().setAddress("address");
        supplier().setPhoneNumber("phoneNumber");
        supplier().setUse(Boolean.valueOf("true"));
        supplier().setMail("mail");
        return supplier();
    }
    public StoreHouse storeHouse(){
        storeHouse().setStoreHouseId(1);
        storeHouse().setStoreHouseName("storeHouseName");
        storeHouse().setBranchId(1);
        storeHouse().setAddress("address");
        storeHouse().setAction(Boolean.valueOf("false"));
        storeHouse().setLocation("location");
        storeHouse().setAbbreviationName("abbreviationName");
        return storeHouse();
    }

    @Test
    void testCreateTransfer() {
                    
        final TransferRequest transferRequest = new TransferRequest();
        transferRequest.setReceivingWareHouseId(1);
        transferRequest.setWareHouseTransferId(1);
        transferRequest.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferRequest.setPetitionerId(1);
        final ProductExRequest productExRequest = new ProductExRequest();
        productExRequest.setProductId(1);
        productExRequest.setNumberOfRequest(1);
        transferRequest.setProductExRequests(Arrays.asList(productExRequest));

                    
        final Optional<StoreHouse> storeHouse = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.findById(1)).thenReturn(storeHouse);

                    
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

                    
        final ReceiptAndIssue receiptAndIssue = receiptAndIssue();
        when(mockModelMapper.map(any(Object.class), eq(ReceiptAndIssue.class))).thenReturn(receiptAndIssue);

                    
        final ReceiptAndIssue receiptAndIssue1 = receiptAndIssue();
        when(mockReceiptIssueRepository.save(any(ReceiptAndIssue.class))).thenReturn(receiptAndIssue1);

                    
        transferServiceImplUnderTest.createTransfer(transferRequest);

                    
        verify(mockReceiptIssueRepository).insert_Transfer(1, 1);
        verify(mockReceiptIssueRepository).insertReceipt_Product(1, 1, 1, 0.0);
        verify(mockReceiptIssueRepository).insertIssue_Product(1, 1, 1);
    }

    @Test
    void testCreateTransfer_StoreHouseRepositoryReturnsAbsent() {
                    
        final TransferRequest transferRequest = new TransferRequest();
        transferRequest.setReceivingWareHouseId(1);
        transferRequest.setWareHouseTransferId(1);
        transferRequest.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferRequest.setPetitionerId(1);
        final ProductExRequest productExRequest = new ProductExRequest();
        productExRequest.setProductId(1);
        productExRequest.setNumberOfRequest(1);
        transferRequest.setProductExRequests(Arrays.asList(productExRequest));

        when(mockStoreHouseRepository.findById(1)).thenReturn(Optional.empty());

                    
        assertThatThrownBy(() -> transferServiceImplUnderTest.createTransfer(transferRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testCreateTransfer_StaffRepositoryReturnsAbsent() {
                    
        final TransferRequest transferRequest = new TransferRequest();
        transferRequest.setReceivingWareHouseId(1);
        transferRequest.setWareHouseTransferId(1);
        transferRequest.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferRequest.setPetitionerId(1);
        final ProductExRequest productExRequest = new ProductExRequest();
        productExRequest.setProductId(1);
        productExRequest.setNumberOfRequest(1);
        transferRequest.setProductExRequests(Arrays.asList(productExRequest));

                    
        final Optional<StoreHouse> storeHouse = Optional.of(
                storeHouse());
        when(mockStoreHouseRepository.findById(1)).thenReturn(storeHouse);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

                    
        assertThatThrownBy(() -> transferServiceImplUnderTest.createTransfer(transferRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testActionExportTransfer() {
                    
        final TransferExportProductRequest exportProductsRequest = new TransferExportProductRequest();
        exportProductsRequest.setIdExport(0);
        exportProductsRequest.setPersonInChargeId(0);
        exportProductsRequest.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductExportRequest productExportRequest = new ProductExportRequest();
        productExportRequest.setIdProduct(0);
        productExportRequest.setAmount(0);
        productExportRequest.setNote("note");
        exportProductsRequest.setProductExportRequests(Arrays.asList(productExportRequest));

                    
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

                    
        final Optional<Staff> staff = Optional.of(staff());
        when(mockStaffRepository.findById(1)).thenReturn(staff);

                    
        final ReceiptAndIssue receiptAndIssue1 = receiptAndIssue();
        when(mockReceiptIssueRepository.save(any(ReceiptAndIssue.class))).thenReturn(receiptAndIssue1);

                    
        transferServiceImplUnderTest.actionExportTransfer(exportProductsRequest);

                    
        verify(mockReceiptIssueRepository).save(any(ReceiptAndIssue.class));
        verify(mockReceiptIssueRepository).updateIssue_Product(1, 1, 1, "note");
    }

    @Test
    void testActionExportTransfer_ReceiptIssueRepositoryFindByIdReturnsAbsent() {
                    
        final TransferExportProductRequest exportProductsRequest = new TransferExportProductRequest();
        exportProductsRequest.setIdExport(1);
        exportProductsRequest.setPersonInChargeId(1);
        exportProductsRequest.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductExportRequest productExportRequest = new ProductExportRequest();
        productExportRequest.setIdProduct(1);
        productExportRequest.setAmount(1);
        productExportRequest.setNote("note");
        exportProductsRequest.setProductExportRequests(Arrays.asList(productExportRequest));

        when(mockReceiptIssueRepository.findById(1)).thenReturn(Optional.empty());

                    
        assertThatThrownBy(() -> transferServiceImplUnderTest.actionExportTransfer(exportProductsRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testActionExportTransfer_StaffRepositoryReturnsAbsent() {
                    
        final TransferExportProductRequest exportProductsRequest = new TransferExportProductRequest();
        exportProductsRequest.setIdExport(1);
        exportProductsRequest.setPersonInChargeId(1);
        exportProductsRequest.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        final ProductExportRequest productExportRequest = new ProductExportRequest();
        productExportRequest.setIdProduct(1);
        productExportRequest.setAmount(1);
        productExportRequest.setNote("note");
        exportProductsRequest.setProductExportRequests(Arrays.asList(productExportRequest));

                    
        final Optional<ReceiptAndIssue> receiptAndIssue = Optional.of(
                receiptAndIssue());
        when(mockReceiptIssueRepository.findById(1)).thenReturn(receiptAndIssue);

        when(mockStaffRepository.findById(1)).thenReturn(Optional.empty());

                    
        assertThatThrownBy(() -> transferServiceImplUnderTest.actionExportTransfer(exportProductsRequest))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetTransfersReceipt() {
                    
        when(mockReceiptIssueRepository.getTransfersReceipt()).thenReturn(Arrays.asList());

                    
        final List<ITransferReceiptResponse> result = transferServiceImplUnderTest.getTransfersReceipt();

                    
    }

    @Test
    void testGetTransfersReceipt_ReceiptIssueRepositoryReturnsNoItems() {
                    
        when(mockReceiptIssueRepository.getTransfersReceipt()).thenReturn(Collections.emptyList());

                    
        final List<ITransferReceiptResponse> result = transferServiceImplUnderTest.getTransfersReceipt();

                    
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetTransfersExport() {
                    
        when(mockReceiptIssueRepository.getTransfersExport()).thenReturn(Arrays.asList());

                    
        final List<ITransferExportResponse> result = transferServiceImplUnderTest.getTransfersExport();

                    
    }

    @Test
    void testGetTransfersExport_ReceiptIssueRepositoryReturnsNoItems() {
                    
        when(mockReceiptIssueRepository.getTransfersExport()).thenReturn(Collections.emptyList());

                    
        final List<ITransferExportResponse> result = transferServiceImplUnderTest.getTransfersExport();

                    
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetTransferShExportById() {
                    
        when(mockReceiptIssueRepository.getTransferExportById(1)).thenReturn(null);

                    
        final TransferShExportResponse transferShExportResponse = new TransferShExportResponse(1, "sourceStoreName",
                "destinationStoreName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, "statusName",
                Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(TransferShExportResponse.class)))
                .thenReturn(transferShExportResponse);

        when(mockReceiptIssueRepository.getProductsExportById(1)).thenReturn(Arrays.asList());

                    
        final TransferShExportResponse result = transferServiceImplUnderTest.getTransferShExportById(1);

                    
    }

    @Test
    void testGetTransferShExportById_ReceiptIssueRepositoryGetTransferExportByIdReturnsNull() {
                    
        when(mockReceiptIssueRepository.getTransferExportById(1)).thenReturn(null);

                    
        assertThatThrownBy(() -> transferServiceImplUnderTest.getTransferShExportById(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetTransferShExportById_ReceiptIssueRepositoryGetProductsExportByIdReturnsNoItems() {
                    
        when(mockReceiptIssueRepository.getTransferExportById(1)).thenReturn(null);

                    
        final TransferShExportResponse transferShExportResponse = new TransferShExportResponse(1, "sourceStoreName",
                "destinationStoreName", new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime(), 1, "statusName",
                Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(TransferShExportResponse.class)))
                .thenReturn(transferShExportResponse);

        when(mockReceiptIssueRepository.getProductsExportById(1)).thenReturn(Collections.emptyList());

                    
        final TransferShExportResponse result = transferServiceImplUnderTest.getTransferShExportById(1);

                    
    }

    @Test
    void testGetTransferShReceiptById() {
                    
        when(mockReceiptIssueRepository.getTransferReceiptById(1)).thenReturn(null);

                    
        final TransferShReceiptResponse transferShReceiptResponse = new TransferShReceiptResponse();
        transferShReceiptResponse.setReceiptId(1);
        transferShReceiptResponse.setSourceStoreName("sourceStoreName");
        transferShReceiptResponse.setDestinationStoreName("destinationStoreName");
        transferShReceiptResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferShReceiptResponse.setStatusID(1);
        transferShReceiptResponse.setStatusName("statusName");
        transferShReceiptResponse.setProductResponses(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(TransferShReceiptResponse.class)))
                .thenReturn(transferShReceiptResponse);

        when(mockReceiptIssueRepository.getProductsReceipt(1)).thenReturn(Arrays.asList());

                    
        final TransferShReceiptResponse result = transferServiceImplUnderTest.getTransferShReceiptById(1);

                    
    }

    @Test
    void testGetTransferShReceiptById_ReceiptIssueRepositoryGetTransferReceiptByIdReturnsNull() {
                    
        when(mockReceiptIssueRepository.getTransferReceiptById(1)).thenReturn(null);

                    
        assertThatThrownBy(() -> transferServiceImplUnderTest.getTransferShReceiptById(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetTransferShReceiptById_ReceiptIssueRepositoryGetProductsReceiptReturnsNoItems() {
                    
        when(mockReceiptIssueRepository.getTransferReceiptById(1)).thenReturn(null);

                    
        final TransferShReceiptResponse transferShReceiptResponse = new TransferShReceiptResponse();
        transferShReceiptResponse.setReceiptId(1);
        transferShReceiptResponse.setSourceStoreName("sourceStoreName");
        transferShReceiptResponse.setDestinationStoreName("destinationStoreName");
        transferShReceiptResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferShReceiptResponse.setStatusID(1);
        transferShReceiptResponse.setStatusName("statusName");
        transferShReceiptResponse.setProductResponses(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(TransferShReceiptResponse.class)))
                .thenReturn(transferShReceiptResponse);

        when(mockReceiptIssueRepository.getProductsReceipt(1)).thenReturn(Collections.emptyList());

                    
        final TransferShReceiptResponse result = transferServiceImplUnderTest.getTransferShReceiptById(1);

                    
    }

    @Test
    void testGetTransferFullExportById() {
                    
        when(mockReceiptIssueRepository.getTransferFullExportById(1)).thenReturn(null);

                    
        final TransferFullExportResponse transferFullExportResponse = new TransferFullExportResponse();
        transferFullExportResponse.setExportId(1);
        transferFullExportResponse.setSourceStoreName("sourceStoreName");
        transferFullExportResponse.setDestinationStoreName("destinationStoreName");
        transferFullExportResponse.setPetitionerName("petitionerName");
        transferFullExportResponse.setPersonInChargeName("personInChargeName");
        transferFullExportResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferFullExportResponse.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferFullExportResponse.setProductResponses(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(TransferFullExportResponse.class)))
                .thenReturn(transferFullExportResponse);

        when(mockReceiptIssueRepository.getProductsExportById(1)).thenReturn(Arrays.asList());

                    
        final TransferFullExportResponse result = transferServiceImplUnderTest.getTransferFullExportById(1);

                    
    }

    @Test
    void testGetTransferFullExportById_ReceiptIssueRepositoryGetTransferFullExportByIdReturnsNull() {
                    
        when(mockReceiptIssueRepository.getTransferFullExportById(1)).thenReturn(null);

                    
        assertThatThrownBy(() -> transferServiceImplUnderTest.getTransferFullExportById(1))
                .isInstanceOf(RuntimeException.class);
    }

    @Test
    void testGetTransferFullExportById_ReceiptIssueRepositoryGetProductsExportByIdReturnsNoItems() {
                    
        when(mockReceiptIssueRepository.getTransferFullExportById(1)).thenReturn(null);

                    
        final TransferFullExportResponse transferFullExportResponse = new TransferFullExportResponse();
        transferFullExportResponse.setExportId(1);
        transferFullExportResponse.setSourceStoreName("sourceStoreName");
        transferFullExportResponse.setDestinationStoreName("destinationStoreName");
        transferFullExportResponse.setPetitionerName("petitionerName");
        transferFullExportResponse.setPersonInChargeName("personInChargeName");
        transferFullExportResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferFullExportResponse.setExportDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferFullExportResponse.setProductResponses(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(TransferFullExportResponse.class)))
                .thenReturn(transferFullExportResponse);

        when(mockReceiptIssueRepository.getProductsExportById(1)).thenReturn(Collections.emptyList());

                    
        final TransferFullExportResponse result = transferServiceImplUnderTest.getTransferFullExportById(1);

                    
    }

    @Test
    void testGetTransferFullReceiptById() {
                    
        when(mockReceiptIssueRepository.getTransferFullReceiptById(1)).thenReturn(null);

                    
        final TransferFullReceiptResponse transferFullReceiptResponse = new TransferFullReceiptResponse();
        transferFullReceiptResponse.setReceiptId(1);
        transferFullReceiptResponse.setSourceStoreName("sourceStoreName");
        transferFullReceiptResponse.setDestinationStoreName("destinationStoreName");
        transferFullReceiptResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferFullReceiptResponse.setPetitionerName("petitionerName");
        transferFullReceiptResponse.setProductsResponse(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(TransferFullReceiptResponse.class)))
                .thenReturn(transferFullReceiptResponse);

        when(mockReceiptIssueRepository.getProductsReceipt(1)).thenReturn(Arrays.asList());

                    
        final TransferFullReceiptResponse result = transferServiceImplUnderTest.getTransferFullReceiptById(1);

                    
    }

    @Test
    void testGetTransferFullReceiptById_ReceiptIssueRepositoryGetProductsReceiptReturnsNoItems() {
                    
        when(mockReceiptIssueRepository.getTransferFullReceiptById(1)).thenReturn(null);

                    
        final TransferFullReceiptResponse transferFullReceiptResponse = new TransferFullReceiptResponse();
        transferFullReceiptResponse.setReceiptId(1);
        transferFullReceiptResponse.setSourceStoreName("sourceStoreName");
        transferFullReceiptResponse.setDestinationStoreName("destinationStoreName");
        transferFullReceiptResponse.setExpectedDate(new GregorianCalendar(2020, Calendar.JANUARY, 1).getTime());
        transferFullReceiptResponse.setPetitionerName("petitionerName");
        transferFullReceiptResponse.setProductsResponse(Arrays.asList());
        when(mockModelMapper.map(any(Object.class), eq(TransferFullReceiptResponse.class)))
                .thenReturn(transferFullReceiptResponse);

        when(mockReceiptIssueRepository.getProductsReceipt(1)).thenReturn(Collections.emptyList());

                    
        final TransferFullReceiptResponse result = transferServiceImplUnderTest.getTransferFullReceiptById(1);

                    
    }
}
