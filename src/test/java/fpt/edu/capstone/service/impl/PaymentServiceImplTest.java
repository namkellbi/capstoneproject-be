package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.vnpay.PaymentDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class PaymentServiceImplTest {

    @Mock
    private ModelMapper mockModelMapper;

    private PaymentServiceImpl paymentServiceImplUnderTest;

    @BeforeEach
    void setUp() throws Exception {
        paymentServiceImplUnderTest = new PaymentServiceImpl(mockModelMapper);
    }

    @Test
    void testGetPaymentVNPay() throws Exception {
        assertThat(paymentServiceImplUnderTest.getPaymentVNPay(
                new PaymentDTO(1, 60000, "orderType", "description", "bankCode"))).isNull();
    }

    @Test
    void testSavePayment() {
        paymentServiceImplUnderTest.savePayment("vnpResponseCode", "vnpOrderInfo");
    }
}
