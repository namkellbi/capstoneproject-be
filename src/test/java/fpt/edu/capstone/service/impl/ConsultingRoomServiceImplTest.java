package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.consulting.*;
import fpt.edu.capstone.entity.Branch;
import fpt.edu.capstone.entity.ConsultingRoom;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.ConsultingRoomRepository;
import fpt.edu.capstone.repository.MedicalServiceRepository;
import fpt.edu.capstone.service.BranchService;
import fpt.edu.capstone.service.WorkScheduleService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConsultingRoomServiceImplTest {

    @Mock
    private ConsultingRoomRepository mockConsultingRoomRepository;
    @Mock
    private WorkScheduleService mockWorkScheduleService;
    @Mock
    private BranchService mockBranchService;
    @Mock
    private MedicalServiceRepository mockMedicalServiceRepository;
    @Mock
    private ModelMapper mockModelMapper;

    private ConsultingRoomServiceImpl consultingRoomServiceImplUnderTest;

    @BeforeEach
    void setUp() {
        consultingRoomServiceImplUnderTest = new ConsultingRoomServiceImpl(mockConsultingRoomRepository,
                mockWorkScheduleService, mockBranchService, mockMedicalServiceRepository, mockModelMapper);
    }

    @Test
    void testGetConsultingRoomByConsultingRoomId() {
        assertThat(consultingRoomServiceImplUnderTest.getConsultingRoomByConsultingRoomId(0)).isNull();
    }

    public ConsultingRoom consultingRoom() {
        consultingRoom().setConsultingRoomId(1);
        consultingRoom().setRoomName("roomName");
        consultingRoom().setAbbreviationName("abbreviationName");
        consultingRoom().setBranchId(1);
        consultingRoom().setAction(Boolean.valueOf("false"));
        return consultingRoom();
    }
    public Branch branch(){
        branch().setBranchId(1);
        branch().setBranchName("branchName");
        branch().setAction(Boolean.valueOf("false"));
        branch().setPhoneNumber("phoneNumber");
        branch().setAddress("address");
        return branch();
    }

    @Test
    void testGetListConsultingRoom() {
               
               
        final List<ConsultingRoom> consultingRooms = Arrays.asList(
                consultingRoom());
        when(mockConsultingRoomRepository.findAll()).thenReturn(consultingRooms);

               
        final List<ConsultingRoom> result = consultingRoomServiceImplUnderTest.getListConsultingRoom();

               
    }

    @Test
    void testGetListConsultingRoom_ConsultingRoomRepositoryReturnsNoItems() {
               
        when(mockConsultingRoomRepository.findAll()).thenReturn(Collections.emptyList());

               
        final List<ConsultingRoom> result = consultingRoomServiceImplUnderTest.getListConsultingRoom();

               
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetConsultingRoomById() {
               
               
        final Optional<ConsultingRoom> consultingRoom = Optional.of(
                consultingRoom());
        when(mockConsultingRoomRepository.getByConsultingRoomIdAndAction(1, true)).thenReturn(consultingRoom);

               
        final ConsultingRoom result = consultingRoomServiceImplUnderTest.getConsultingRoomById(1);

               
    }

    @Test
    void testGetConsultingRoomById_ConsultingRoomRepositoryReturnsAbsent() {
               
        when(mockConsultingRoomRepository.getByConsultingRoomIdAndAction(1, true)).thenReturn(Optional.empty());

               
        assertThatThrownBy(() -> consultingRoomServiceImplUnderTest.getConsultingRoomById(1))
                .isInstanceOf(ClinicException.class);
    }

    @Test
    void testGetConsultingRoomByName() {
               
               
        final List<ConsultingRoom> consultingRooms = Arrays.asList(
                consultingRoom());
        when(mockConsultingRoomRepository.getConsultingRoomByName("name", true)).thenReturn(consultingRooms);

               
        final List<ConsultingRoom> result = consultingRoomServiceImplUnderTest.getConsultingRoomByName("name");

               
    }

    @Test
    void testGetConsultingRoomByName_ConsultingRoomRepositoryReturnsNoItems() {
               
        when(mockConsultingRoomRepository.getConsultingRoomByName("name", true)).thenReturn(Collections.emptyList());

               
        final List<ConsultingRoom> result = consultingRoomServiceImplUnderTest.getConsultingRoomByName("name");

               
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testCreateConsultingRoom() {
               
        final ConsultingRoomRequest request = new ConsultingRoomRequest();
        request.setRoomName("roomName");
        request.setAbbreviationName("abbreviationName");
        request.setBranchId(1);
        request.setAction(false);

               
        final Branch branch = branch();
        when(mockBranchService.getById(1)).thenReturn(branch);

               
        final ConsultingRoom consultingRoom = consultingRoom();
        when(mockModelMapper.map(any(Object.class), eq(ConsultingRoom.class))).thenReturn(consultingRoom);

               
        final ConsultingRoom consultingRoom1 = consultingRoom();
        when(mockConsultingRoomRepository.save(any(ConsultingRoom.class))).thenReturn(consultingRoom1);

               
        consultingRoomServiceImplUnderTest.createConsultingRoom(request);

               
        verify(mockConsultingRoomRepository).save(any(ConsultingRoom.class));
    }

    @Test
    void testCreateConsultingRoom_BranchServiceReturnsNull() {
               
        final ConsultingRoomRequest request = new ConsultingRoomRequest();
        request.setRoomName("roomName");
        request.setAbbreviationName("abbreviationName");
        request.setBranchId(1);
        request.setAction(false);

        when(mockBranchService.getById(0)).thenReturn(null);

               
        assertThatThrownBy(() -> consultingRoomServiceImplUnderTest.createConsultingRoom(request))
                .isInstanceOf(ClinicException.class);
    }

    @Test
    void testUpdateConsultingRoom() {
               
        final ConsultingRoomResponse response = new ConsultingRoomResponse();
        response.setConsultingRoomId(1);
        response.setRoomName("roomName");
        response.setAbbreviationName("abbreviationName");
        response.setBranchId(1);

               
        final ConsultingRoom consultingRoom = consultingRoom();
        when(mockConsultingRoomRepository.getByConsultingRoomId(1)).thenReturn(consultingRoom);

               
        final Branch branch = branch();
        when(mockBranchService.getById(1)).thenReturn(branch);

               
        final ConsultingRoom consultingRoom1 = consultingRoom();
        when(mockConsultingRoomRepository.saveAndFlush(any(ConsultingRoom.class))).thenReturn(consultingRoom1);

               
        consultingRoomServiceImplUnderTest.updateConsultingRoom(response);

               
        verify(mockConsultingRoomRepository).saveAndFlush(any(ConsultingRoom.class));
    }

    @Test
    void testUpdateConsultingRoom_ConsultingRoomRepositoryGetByConsultingRoomIdReturnsNull() {
               
        final ConsultingRoomResponse response = new ConsultingRoomResponse();
        response.setConsultingRoomId(1);
        response.setRoomName("roomName");
        response.setAbbreviationName("abbreviationName");
        response.setBranchId(1);

        when(mockConsultingRoomRepository.getByConsultingRoomId(0)).thenReturn(null);

               
        assertThatThrownBy(() -> consultingRoomServiceImplUnderTest.updateConsultingRoom(response))
                .isInstanceOf(ClinicException.class);
    }

    @Test
    void testUpdateConsultingRoom_BranchServiceReturnsNull() {
               
        final ConsultingRoomResponse response = new ConsultingRoomResponse();
        response.setConsultingRoomId(1);
        response.setRoomName("roomName");
        response.setAbbreviationName("abbreviationName");
        response.setBranchId(1);

               
        final ConsultingRoom consultingRoom = consultingRoom();
        when(mockConsultingRoomRepository.getByConsultingRoomId(1)).thenReturn(consultingRoom);

        when(mockBranchService.getById(1)).thenReturn(null);

               
        assertThatThrownBy(() -> consultingRoomServiceImplUnderTest.updateConsultingRoom(response))
                .isInstanceOf(ClinicException.class);
    }

    @Test
    void testGetConsultingRoomByIdService() {
               
               
        final List<ConsultingRoom> consultingRooms = Arrays.asList(
                consultingRoom());
        when(mockConsultingRoomRepository.getConsultingByIdService(1)).thenReturn(consultingRooms);

               
        final List<ConsultingRoom> result = consultingRoomServiceImplUnderTest.getConsultingRoomByIdService(0);

               
    }

    @Test
    void testGetConsultingRoomByIdService_ConsultingRoomRepositoryReturnsNoItems() {
               
        when(mockConsultingRoomRepository.getConsultingByIdService(1)).thenReturn(Collections.emptyList());

               
        final List<ConsultingRoom> result = consultingRoomServiceImplUnderTest.getConsultingRoomByIdService(1);

               
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetConsultingServices() {
               
               
        final List<ConsultingRoom> consultingRooms = Arrays.asList(
                consultingRoom());
        when(mockConsultingRoomRepository.getAllConsultingByIdBranch(1)).thenReturn(consultingRooms);

        when(mockMedicalServiceRepository.getServicesByIdConsulting(1, 1)).thenReturn(Arrays.asList());

               
        final List<ConsultingServicesResponse> result = consultingRoomServiceImplUnderTest.getConsultingServices(1, 1);

               
    }

    @Test
    void testGetConsultingServices_ConsultingRoomRepositoryReturnsNoItems() {
               
        when(mockConsultingRoomRepository.getAllConsultingByIdBranch(1)).thenReturn(Collections.emptyList());
        when(mockMedicalServiceRepository.getServicesByIdConsulting(1, 1)).thenReturn(Arrays.asList());

               
        final List<ConsultingServicesResponse> result = consultingRoomServiceImplUnderTest.getConsultingServices(1, 1);

               
    }

    @Test
    void testGetConsultingServices_MedicalServiceRepositoryReturnsNoItems() {
               
               
        final List<ConsultingRoom> consultingRooms = Arrays.asList(
                consultingRoom());
        when(mockConsultingRoomRepository.getAllConsultingByIdBranch(1)).thenReturn(consultingRooms);

        when(mockMedicalServiceRepository.getServicesByIdConsulting(1, 1)).thenReturn(Collections.emptyList());

               
        final List<ConsultingServicesResponse> result = consultingRoomServiceImplUnderTest.getConsultingServices(1, 1);

               
    }

    @Test
    void testGetAllService() {
               
        when(mockConsultingRoomRepository.getAllService()).thenReturn(Arrays.asList());

               
        final List<CheckUpResponse> result = consultingRoomServiceImplUnderTest.getAllService();

               
    }

    @Test
    void testGetAllService_ConsultingRoomRepositoryReturnsNoItems() {
               
        when(mockConsultingRoomRepository.getAllService()).thenReturn(Collections.emptyList());

               
        final List<CheckUpResponse> result = consultingRoomServiceImplUnderTest.getAllService();

               
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testGetAllConsultingFix() {
               
        when(mockConsultingRoomRepository.getAllConsultingRoomFix(1)).thenReturn(Arrays.asList());

               
        final List<AllConsultingResponse> result = consultingRoomServiceImplUnderTest.getAllConsultingFix(1);

               
    }

    @Test
    void testGetAllConsultingFix_ConsultingRoomRepositoryReturnsNoItems() {
               
        when(mockConsultingRoomRepository.getAllConsultingRoomFix(1)).thenReturn(Collections.emptyList());

               
        final List<AllConsultingResponse> result = consultingRoomServiceImplUnderTest.getAllConsultingFix(1);

               
        assertThat(result).isEqualTo(Collections.emptyList());
    }

    @Test
    void testDeleteConsultingRoom() {
               
               
        consultingRoomServiceImplUnderTest.deleteConsultingRoom(1);

               
        verify(mockConsultingRoomRepository).deleteConsultingById(1);
    }
}
