package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.report.*;

import java.util.List;

public interface ReportService {
    List<RevenueReport> getAllRevenueInBranch(Integer day, Integer month, int year, int branchId);

    int getPatientExamined(Integer day, Integer month, int year, int branchId);

    int getMedicalVisits(Integer day, Integer month, int year, int branchId);



    List<IPatientUsedService> getCostPatientUsed(Integer day, Integer month, int year, int branch);

    List<IHistoryUsedService> getHistoryPatientUsedService(Integer day, Integer month, int year, int branch, int patientId);
    List<ITotalCostByService> getCostByService(Integer day, Integer month, int year, int branch);
    List<IPatientUsedService2> getPatientUsedService(Integer day, Integer month, int year, int branch, int serviceId);
}
