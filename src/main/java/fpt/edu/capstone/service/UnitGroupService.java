package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.unitgroup.UnitGroupRequest;
import fpt.edu.capstone.dto.unitgroup.UnitGroupsRequest;
import fpt.edu.capstone.entity.UnitGroup;

import java.util.List;

public interface UnitGroupService {
    List<UnitGroup> getListUnitGroup();

    void createUnitGroup(UnitGroupRequest request);

    void deleteUnitGroup(UnitGroupsRequest unitGroupsRequest);
}
