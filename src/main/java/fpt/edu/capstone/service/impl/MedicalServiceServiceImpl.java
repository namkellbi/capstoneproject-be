package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.service.AllServiceRequest;
import fpt.edu.capstone.dto.service.ServiceRequestForCreate;
import fpt.edu.capstone.dto.service.ServiceResponse;
import fpt.edu.capstone.entity.MedicalService;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.ConsultingRoomRepository;
import fpt.edu.capstone.repository.MedicalServiceRepository;
import fpt.edu.capstone.service.MedicalServiceService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
@AllArgsConstructor
public class MedicalServiceServiceImpl implements MedicalServiceService {
    private final MedicalServiceRepository medicalServiceRepository;
    private final ConsultingRoomRepository consultingRoomRepository;
    @Override
    public List<MedicalService> getAllService(int idBranch) {
        return medicalServiceRepository.getAllServiceByIdBranch(idBranch);
    }
//    public List<ServiceResponse> getConsultingRoomByMedicalServiceId(int medicalServiceId){
//        //return serviceRepository.getConsultingRoomByMedicalServiceId(medicalServiceId);
//    }
    @Override
    public List<ServiceResponse> getDataService(int consultingRoomId){
    return medicalServiceRepository.getDataService(consultingRoomId);
    }

    @Override
    public List<ServiceResponse> getAllServiceName() {
        return medicalServiceRepository.getAllServiceName();
    }

    @Override
    public List<ServiceResponse> findServiceByServiceName(String serviceName) {
        return medicalServiceRepository.findServiceByServiceName(serviceName);
    }

    @Override
    public void createService(ServiceRequestForCreate serviceRequestForCreate) {
        MedicalService ms = new MedicalService();
        ms.setServiceName(serviceRequestForCreate.getServiceName());
        ms.setConsultingRoomId(serviceRequestForCreate.getConsultingRoomId());
        ms.setPrice(serviceRequestForCreate.getMoney());
        medicalServiceRepository.save(ms);
    }

    @Override
    public List<MedicalService> getAllMedicalServiceById(AllServiceRequest AllServiceRequest) {
        for (Integer medicalServiceId : AllServiceRequest.getIdServices()) {
            if (!medicalServiceRepository.findById(medicalServiceId).isPresent())
                throw new ClinicException(ResponseMessageConstants.SERVICE_DOES_NOT_EXIST);
        }
        List<MedicalService> medicalServiceList = new ArrayList<>();

        //get all by id
        for (Integer medicalServiceId : AllServiceRequest.getIdServices()) {
            MedicalService medicalService = medicalServiceRepository.getConsultingRoomByMedicalServiceId(medicalServiceId);
            medicalServiceList.add(medicalService);
        }
        return medicalServiceList;
    }


}
