package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.entity.Supplier;
import fpt.edu.capstone.repository.SupplierRepository;
import fpt.edu.capstone.service.SupplierService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class SupplierServiceImpl implements SupplierService {
    private final SupplierRepository supplierRepository;

    @Override
    public List<Supplier> getAllSupplier() {
        return supplierRepository.getAllSuppliers();
    }

}
