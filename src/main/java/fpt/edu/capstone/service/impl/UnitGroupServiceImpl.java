package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.unitgroup.UnitGroupRequest;
import fpt.edu.capstone.dto.unitgroup.UnitGroupsRequest;
import fpt.edu.capstone.entity.UnitGroup;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.UnitGroupRepository;
import fpt.edu.capstone.service.UnitGroupService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class UnitGroupServiceImpl implements UnitGroupService {
    private final UnitGroupRepository unitGroupRepository;

    @Override
    public List<UnitGroup> getListUnitGroup() {
        return unitGroupRepository.getAllUnitGroup();
    }

    @Override
    public void createUnitGroup(UnitGroupRequest request) {
        UnitGroup unitGroup = unitGroupRepository.findByName(request.getName());
        if (unitGroup != null) {
            throw new ClinicException(ResponseMessageConstants.UNIT_GROUP_NAME_EXIST);
        }
        UnitGroup unitGroupp = new UnitGroup();
        unitGroupp.setName(request.getName());
        unitGroupp.setUse(true);
        unitGroupRepository.save(unitGroupp);
    }

    @Override
    public void deleteUnitGroup(UnitGroupsRequest unitGroupsRequest) {
        //check exist
        for (Integer unitGroupId : unitGroupsRequest.getUnitGroupsId()) {
            if (!unitGroupRepository.findById(unitGroupId).isPresent())
                throw new ClinicException(ResponseMessageConstants.UNIT_GROUP_DOES_NOT_EXIST);
        }
        //delete
        for (Integer unitGroupId : unitGroupsRequest.getUnitGroupsId()) {
            unitGroupRepository.deleteUnitGroupById(unitGroupId);
        }

    }
}
