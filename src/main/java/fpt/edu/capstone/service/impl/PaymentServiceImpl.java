package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.PaymentConfig;
import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.vnpay.PaymentDTO;
import fpt.edu.capstone.dto.vnpay.PaymentResponseDTO;

import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.service.PaymentService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

@Service
@AllArgsConstructor
public class PaymentServiceImpl implements PaymentService {
    private final ModelMapper modelMapper;


    @Override
    public PaymentResponseDTO getPaymentVNPay(PaymentDTO paymentDTO) throws UnsupportedEncodingException {
//        Payment payment = modelMapper.map(paymentDTO, Payment.class);
//        String randomTransactionCode = PaymentConfig.getRandomNumber(8);
//        payment.setTransactionCode(randomTransactionCode);
//        int amount = paymentDTO.getAmount() * 100;
//
//        Date date = new Date();
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
//        String dateString = formatter.format(date);
//        String vnpCreateDate = dateString;
//
//        int billId = payment.getBillId();
//        String description = paymentDTO.getDescription();
//        String orderType = paymentDTO.getOrderType();
//        String p = "billId " + billId  + " amount " + String.valueOf(amount) + " bankCode " + paymentDTO.getBankCode() +
//                " description " + description + " orderType " + orderType;
//
//        Map<String, String> vnpParams = new HashMap<>();
//        vnpParams.put("vnp_Version", PaymentConfig.VERSION_VNPAY);
//        vnpParams.put("vnp_Command", PaymentConfig.COMMAND);
//        vnpParams.put("vnp_TmnCode", PaymentConfig.TMN_CODE);
//        vnpParams.put("vnp_Amount", String.valueOf(amount));
//        vnpParams.put("vnp_CreateDate", vnpCreateDate);
//        vnpParams.put("vnp_CurrCode", PaymentConfig.CURR_CODE);
//        vnpParams.put("vnp_IpAddr", PaymentConfig.IP_DEFAULT);
//
//        vnpParams.put("vnp_Locale", PaymentConfig.LOCATE_DEFAULT);
//        vnpParams.put("vnp_OrderInfo", p);
//        vnpParams.put("vnp_OrderType", PaymentConfig.ORDER_TYPE);
//        vnpParams.put("vnp_ReturnUrl", PaymentConfig.RETURN_URL);
//        vnpParams.put("vnp_TxnRef", payment.getTransactionCode());
//        vnpParams.put("vnp_BankCode", paymentDTO.getBankCode());
//
//
//        List fieldNames = new ArrayList(vnpParams.keySet());
//        Collections.sort(fieldNames);
//
//        StringBuilder hashData = new StringBuilder();
//        StringBuilder query = new StringBuilder();
//
//        Iterator itr = fieldNames.iterator();
//        while (itr.hasNext()) {
//            String fieldName = (String) itr.next();
//            String fieldValue = (String) vnpParams.get(fieldName);
//            if ((fieldValue != null) && (fieldValue.length() > 0)) {
//                //Build hash data
//                hashData.append(fieldName);
//                hashData.append('=');
//                hashData.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
//                //Build query
//                query.append(URLEncoder.encode(fieldName, StandardCharsets.US_ASCII.toString()));
//                query.append('=');
//                query.append(URLEncoder.encode(fieldValue, StandardCharsets.US_ASCII.toString()));
//                if (itr.hasNext()) {
//                    query.append('&');
//                    hashData.append('&');
//                }
//            }
//        }
//        String queryUrl = query.toString();
//        String vnp_SecureHash = PaymentConfig.hmacSHA512(PaymentConfig.vnp_HashSecret, hashData.toString());
//        queryUrl += "&vnp_SecureHash=" + vnp_SecureHash;
//        String paymentUrl = PaymentConfig.vnp_PayUrl + "?" + queryUrl;
//
//        PaymentResponseDTO result = new PaymentResponseDTO();
//        result.setPaymentUrl(paymentUrl);
//        return result;
        return null;
    }

    @Override
    public void savePayment(String vnpResponseCode, String vnpOrderInfo) {
        PaymentDTO paymentDTO = new PaymentDTO();

        String[] ss = vnpOrderInfo.split("\\+");
        Map<String, String> map = new HashMap<>();
        for (int i = 0; i < ss.length; i = i + 2) {
            map.put(ss[i], ss[i + 1]);
        }
        map.forEach((key, value) -> {
            if (key.equals("billId")) paymentDTO.setBillId(Integer.parseInt(value));
            if (key.equals("amount")) paymentDTO.setAmount(Integer.parseInt(value) / 100);
            if (key.equals("description")) paymentDTO.setDescription(value);
            if (key.equals("orderType")) paymentDTO.setOrderType(value);
            if (key.equals("bankCode")) paymentDTO.setBankCode(value);
        });
        if(paymentDTO.getBillId() != 0){
            //TODO: Cần kiểm tra là thanh toán dịch vụ gì, lưu tên dịch vụ vào db
//            DetailPackage getName = detailPackageService.findById(paymentDTO.getDetailPackageId());
//            String name  = getName.getDetailName();
//            paymentDTO.setOrderType("MUA_" +name);
        }

        LocalDateTime now = LocalDateTime.now();

//        Payment payment = modelMapper.map(paymentDTO, Payment.class);
//        //TODO: Need to check patient id already exist
//
//        payment.setCommand(PaymentConfig.COMMAND);
//        payment.setCurrCode(PaymentConfig.CURR_CODE);
//        payment.setLocal(PaymentConfig.LOCATE_DEFAULT);
//        String randomTransactionCode = PaymentConfig.getRandomNumber(8);
//        payment.setTransactionCode(randomTransactionCode);
//
//        if (vnpResponseCode.equals("00")) {
//            System.out.println("Thanh toán thành công");
//            paymentRepository.save(payment);
//        }
        if (vnpResponseCode.equals("07")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_07);
        }
        if (vnpResponseCode.equals("09")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_09);
        }
        if (vnpResponseCode.equals("10")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_10);
        }
        if (vnpResponseCode.equals("11")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_11);
        }
        if (vnpResponseCode.equals("12")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_12);
        }
        if (vnpResponseCode.equals("13")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_13);
        }
        if (vnpResponseCode.equals("24")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_24);
        }
        if (vnpResponseCode.equals("51")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_51);
        }
        if (vnpResponseCode.equals("65")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_65);
        }
        if (vnpResponseCode.equals("79")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_79);
        }
        if (vnpResponseCode.equals("99")) {
            throw new ClinicException(ResponseMessageConstants.VNP_RESPONSE_CODE_99);
        }
    }
}
