package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.common.StatusApp;
import fpt.edu.capstone.dto.medicalrecord.IMedicalReport;
import fpt.edu.capstone.dto.medicalreportservice.IReportServiceResponse;
import fpt.edu.capstone.dto.medicalreportservice.ServicesRequest;
import fpt.edu.capstone.dto.service.ReportServiceRequest;
import fpt.edu.capstone.entity.*;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.*;
import fpt.edu.capstone.service.ExaminationService;
import fpt.edu.capstone.service.MedicalRecordService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class MedicalRecordServiceImpl implements MedicalRecordService {
    private final MedicalRecordRepository medicalRecordRepository;
    private final MedicalReportServiceRepository mrServiceRepository;
    private final InvoiceRepository invoiceRepository;
    private final MedicalReportServiceRepository mRServiceRepository;
    private final ContactRepository contactRepository;
    private final MedicalReportRepository medicalReportRepository;
    private final ModelMapper modelMapper;
    private final BranchRepository branchRepository;
    private final ExaminationService examinationService;


    @Override
    public void updateNotePatient(int idPatient, String note) {
        MedicalRecord updateMedicalRecord = medicalRecordRepository.findMedicalRecordNewestByAccountId(idPatient);
        if (updateMedicalRecord == null)
            throw new ClinicException(ResponseMessageConstants.MEDICAL_RECORD_DOES_NOT_EXIST);
        updateMedicalRecord.setNote(note);
        medicalRecordRepository.save(updateMedicalRecord);
    }

    @Override
    public List<IReportServiceResponse> getReportServiceByIdPatient(int idPatient) {
        MedicalReport medicalReport = medicalReportRepository.getMedicalReportNewest(idPatient);
        List<IReportServiceResponse> rServiceResponse = mrServiceRepository.getReportServicePatient(medicalReport.getMedicalReportId());
        if (rServiceResponse == null) {
            throw new RuntimeException(ResponseMessageConstants.PATIENT_DOSE_NOT_MEDICAL_SERVICE);
        }
        return rServiceResponse;
    }

    @Override
    public List<IReportServiceResponse> getServiceByIdPatientInConsulting(int idPatient, int idConsulting) {
        MedicalReport medicalReport = medicalReportRepository.getMedicalReportNewest(idPatient);
        List<IReportServiceResponse> rServiceResponse = mrServiceRepository.getReportServicePatientAndRoom(medicalReport.getMedicalReportId(), idConsulting);
        if (rServiceResponse == null) {
            throw new RuntimeException(ResponseMessageConstants.PATIENT_DOSE_NOT_MEDICAL_SERVICE);
        }
        return rServiceResponse;
    }

    //tạo bill (return ???)
//    @Override
//    public void createBillServices(ServicesRequest servicesRequest) {
//        //tạo bill
//        BillService bill = modelMapper.map(servicesRequest, BillService.class);
//
//        bill.setPay(false);
//        bill.setPaymentType(StatusApp.CASH_PAYMENT);
//        if (!contactRepository.findById(servicesRequest.getStaffId()).isPresent() ||
//                !contactRepository.findById(servicesRequest.getPatientId()).isPresent())
//            throw new RuntimeException(ResponseMessageConstants.CONTACT_DOES_NOT_EXIST);
//        if (branchRepository.findBranchById(servicesRequest.getBranchId()) == null)
//            throw new RuntimeException(ResponseMessageConstants.BRANCH_DOES_NOT_EXIST);
//
//        BillService afBill = billRepository.save(bill);
//        List<Integer> idServicesPaid = servicesRequest.getIdReportServices();
//        for (Integer idServicePaid : idServicesPaid) {
//            billRepository.insertBill_Service(afBill.getBillId(), idServicePaid);
//        }

//        if(afBill.isPaymentType()){
//            //thanh toán bằng vn pay
//        }
//    }

    //thanh toán bill
    @Override
    public void paymentService(ServicesRequest servicesRequest) {
//        Optional<BillService> billService = billRepository.findById(idBill);
//        if (!billService.isPresent())
//            throw new RuntimeException("Hóa đơn thanh toán không tồn tại");
//        if(billService.get().isPay())
//            throw new RuntimeException("Hóa đơn đã được thanh toán rồi");
//        List<Integer> reportServiceIds = billRepository.getReportServices(idBill);
        for (Integer reportServiceId : servicesRequest.getIdServices()) {
            //find MedicalReport_Service
            Optional<MedicalReportService> medicalReportService = mrServiceRepository.findById(reportServiceId);
            if (!medicalReportService.isPresent())
                throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_SERVICE_DOES_NOT_EXIST);
            medicalReportService.get().setPay(true);
            mrServiceRepository.save(medicalReportService.get());
            //set stt
            examinationService.addSTT(medicalReportService.get().getMedicalReportId(), medicalReportService.get().getServiceId());
        }

//        billService.get().setPay(true);
//        billRepository.save(billService.get());
    }

    @Override
    public void finishPayment(int idPatient) {
        MedicalReport reportNewest = medicalReportRepository.getMedicalReportNewest(idPatient);
        List<IReportServiceResponse> iReportServiceResponses = mRServiceRepository.getReportServicePatientIsPaid(reportNewest.getMedicalReportId(), false);
        if (iReportServiceResponses.size() == 0) {
            IMedicalReport medicalReport = invoiceRepository.getMedicalReportNewest(idPatient);
            if (medicalReport == null)
                throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_DOES_NOT_EXIST);
            if (!invoiceRepository.getByMedicalReportId(medicalReport.getMedicalReportId()).isPresent())
                throw new RuntimeException(ResponseMessageConstants.INVOICE_DOST_NOT_EXIST);
            if (medicalReport.getIsFinishedExamination()) {
                Optional<Contact> contact = contactRepository.findById(idPatient);
                if (!contact.isPresent()) throw new RuntimeException(ResponseMessageConstants.CONTACT_DOES_NOT_EXIST);
                invoiceRepository.updateStatusInvoice(true, medicalReport.getMedicalReportId());
                contact.get().setStatus(false);
                contactRepository.save(contact.get());
            } else throw new RuntimeException("Bệnh nhân vẫn đang khám bệnh");
        } else throw new RuntimeException(ResponseMessageConstants.PAYMENT_HAS_NOT_BEEN_COMPLETED);
    }

    @Override
    public List<IReportServiceResponse> getServicePaying(ReportServiceRequest reportServiceRequest) {
        List<IReportServiceResponse> iReportServiceResponses = new ArrayList<>();
        for (Integer reportService : reportServiceRequest.getReportServices()) {
            IReportServiceResponse iReportServiceResponse = mrServiceRepository.getReportServiceById(reportService);
            if (iReportServiceResponse == null)
                throw new RuntimeException(ResponseMessageConstants.MEDICAL_REPORT_SERVICE_DOES_NOT_EXIST);
            iReportServiceResponses.add(iReportServiceResponse);
        }
        return iReportServiceResponses;
    }


}
