package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.common.RoleApp;
import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.account.LoginResponse;
import fpt.edu.capstone.dto.account.RegisterRequest;
import fpt.edu.capstone.dto.staff.Staff_ConsultingRoom;
import fpt.edu.capstone.entity.Account;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.entity.Staff;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.AuthenticationRepository;
import fpt.edu.capstone.repository.StaffRepository;
import fpt.edu.capstone.service.*;
import lombok.AllArgsConstructor;
import net.bytebuddy.utility.RandomString;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;

@Service
@AllArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {
    private final AuthenticationRepository loginRepository;
    private final ContactService contactService;
    private final StaffService staffService;
    private final StaffRepository staffRepository;
    private final EmailService emailService;
    private final RoleService roleService;


    @Override
    public LoginResponse login(LoginRequest request) throws Exception {
        Optional<Account> acc = loginRepository.findAccountsByUsername(request.getUsername());
        if (!acc.isPresent()) {
            throw new ClinicException(ResponseMessageConstants.ACCOUNT_DOES_NOT_EXIST);
        }
        Optional<Account> accounts = loginRepository.findAccountsByUsernameAndPassword(request.getUsername(), request.getPassword());
        if (!accounts.isPresent()) {
            throw new ClinicException(ResponseMessageConstants.USERNAME_OR_PASSWORD_INCORRECT);
        }
        if (StringUtils.containsWhitespace(request.getUsername()) || StringUtils.containsWhitespace(request.getPassword())) {
            throw new ClinicException(ResponseMessageConstants.USERNAME_OR_PASSWORD_MUST_NOT_CONTAIN_ANY_SPACE_CHARACTERS);
        }
        LoginResponse response = new LoginResponse();
        response.setUsername(request.getUsername());
        response.setPassword(request.getPassword());
        response.setAccountId(accounts.get().getAccountId());
        Contact contact = contactService.findContactByAccountId(accounts.get().getAccountId());
        Optional<Staff> staff = staffRepository.findById(accounts.get().getAccountId());

        response.setRoleId(contact.getRoleId());
        response.setFullName(contact.getFullName());

        if (contact.getRoleId() == RoleApp.CEO) return response;

        if (!staff.isPresent()) throw new RuntimeException(ResponseMessageConstants.STAFF_DOSE_NOT_EXIST);
        response.setBranchId(staff.get().getBranchId());
        response.setAvatar(staff.get().getAvatar());
        if ((contact.getRoleId() == RoleApp.DOCTOR )) {
            Optional<Staff_ConsultingRoom> staff_consultingRoom = staffRepository.findStaffInConsultingById(staff.get().getStaffId());
            if (!staff_consultingRoom.isPresent())
                throw new RuntimeException(ResponseMessageConstants.STAFF_CONSULTING_ROOM_DOSE_NOT_EXIST);
            response.setConsultingId(staff_consultingRoom.get().getConsultingRoomId());
        }
        return response;
    }

    @Override
    @Transactional(rollbackOn = ClinicException.class)
    public void register(RegisterRequest request) {

        Optional<Account> account = loginRepository.findAccountsByUsername(request.getUsername());
        if (account.isPresent()) {
            throw new ClinicException(ResponseMessageConstants.ACCOUNT_EXIST);
        }
        if (!StringUtils.equals(request.getPassword(), request.getConfirmPassword())) {
            throw new ClinicException(ResponseMessageConstants.CONFIRM_PASSWORD_WRONG);
        }
        Account acc = new Account();
        acc.setUsername(request.getUsername().trim());
        acc.setPassword(request.getPassword().trim());
        loginRepository.save(acc);
    }

    @Override
    public void saveAccount(Account account) {
        loginRepository.save(account);
    }

    @Override
    public void forgotPassword(String name) throws Exception {
        try {
            Account account = contactService.getContactByExactName(name);
            if(account == null){
                throw new ClinicException((ResponseMessageConstants.ACCOUNT_DOES_NOT_EXIST));
            }else{
                int id = account.getAccountId();
                Contact contact = contactService.findContactByAccountId(id);
                String email = contact.getEmail();
                //verify user by email
                Optional<Contact> contact1 = contactService.findByEmail(email);
                if(!contact1.isPresent() || contact1.get().getEmail() == null || contact1.get().getEmail() == "") {
                    throw new ClinicException(ResponseMessageConstants.EMAIL_DOES_NOT_EXIST);
                }else{
                    //generate reset token
                    String token = RandomString.make(8);
                    Account account1 = loginRepository.findAccountByAccountId(contact1.get().getAccountId());
                    account.setPassword(token);
                    loginRepository.save(account);
                    emailService.sendForgotPasswordEmail(contact1.get().getEmail(), token);
                }
            }

        } catch (Exception e) {
            throw e;
        }
    }


}
