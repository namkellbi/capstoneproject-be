package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.consulting.*;
import fpt.edu.capstone.dto.service.ServiceResponse;
import fpt.edu.capstone.entity.Branch;
import fpt.edu.capstone.entity.ConsultingRoom;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.ConsultingRoomRepository;
import fpt.edu.capstone.repository.MedicalServiceRepository;
import fpt.edu.capstone.service.BranchService;
import fpt.edu.capstone.service.ConsultingRoomService;
import fpt.edu.capstone.service.WorkScheduleService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ConsultingRoomServiceImpl implements ConsultingRoomService {
    private final ConsultingRoomRepository consultingRoomRepository;

    private final WorkScheduleService workScheduleService;
    private final BranchService branchService;
    private final MedicalServiceRepository medicalServiceRepository;
    private final ModelMapper modelMapper;


    @Override
    public List<ConsultingRoom> getConsultingRoomByConsultingRoomId(int consultingRoomId) {
        return null;
    }

    @Override
    public List<ConsultingRoom> getListConsultingRoom() {
        return consultingRoomRepository.findAll();
    }

    @Override
    public ConsultingRoom getConsultingRoomById(int roomId) {
        Optional<ConsultingRoom> cr = consultingRoomRepository.getByConsultingRoomIdAndAction(roomId, true);
        if (!cr.isPresent()) {
            throw new ClinicException(ResponseMessageConstants.CONSULTING_ROOM_DOES_NOT_EXIST);
        }
        return cr.get();
    }

    @Override
    public List<ConsultingRoom> getConsultingRoomByName(String name) {
        return consultingRoomRepository.getConsultingRoomByName(name, true);
    }

    @Override
    public void createConsultingRoom(ConsultingRoomRequest request) {
        Branch branch = branchService.getById(request.getBranchId());
        if (branch == null) {
            throw new ClinicException(ResponseMessageConstants.BRANCH_DOES_NOT_EXIST);
        }
        ConsultingRoom cr = modelMapper.map(request, ConsultingRoom.class);
        consultingRoomRepository.save(cr);
    }

    @Override
    public void updateConsultingRoom(ConsultingRoomResponse response) {
        ConsultingRoom consultingRoom = consultingRoomRepository.getByConsultingRoomId(response.getConsultingRoomId());
        if (consultingRoom == null) {
            throw new ClinicException(ResponseMessageConstants.CONSULTING_ROOM_DOES_NOT_EXIST);
        }
        Branch branch = branchService.getById(response.getBranchId());
        if (branch == null) {
            throw new ClinicException(ResponseMessageConstants.BRANCH_DOES_NOT_EXIST);
        }
        consultingRoom.setRoomName(response.getRoomName().trim());
        consultingRoom.setAbbreviationName(response.getAbbreviationName().trim());
        consultingRoom.setBranchId(response.getBranchId());
        consultingRoomRepository.saveAndFlush(consultingRoom);


    }

    @Override
    public List<ConsultingRoom> getConsultingRoomByIdService(int idService) {
        return consultingRoomRepository.getConsultingByIdService(idService);


    }

    @Override
    public List<ConsultingServicesResponse> getConsultingServices(int idPatient, int idBranch) {

        List<ConsultingRoom> consultingRooms = consultingRoomRepository.getAllConsultingByIdBranch(idBranch);
        List<ConsultingServicesResponse> consultingServicesResponses = new ArrayList<>();
        for (ConsultingRoom cr : consultingRooms) {
            List<ServiceResponse> medicalServices = medicalServiceRepository.getServicesByIdConsulting(cr.getConsultingRoomId(), idPatient);
            consultingServicesResponses.add(
                    new ConsultingServicesResponse(cr, medicalServices));
        }
        return consultingServicesResponses;

    }

    @Override
    public List<CheckUpResponse> getAllService() {
        List<CheckUpResponse> checkUpResponse = consultingRoomRepository.getAllService();
        return checkUpResponse;
    }

    @Override
    public List<AllConsultingResponse> getAllConsultingFix(int idBranch) {
        return consultingRoomRepository.getAllConsultingRoomFix(idBranch);
    }

    @Override
    public void deleteConsultingRoom(int idConsulting) {
        consultingRoomRepository.deleteConsultingById(idConsulting);
    }


}
