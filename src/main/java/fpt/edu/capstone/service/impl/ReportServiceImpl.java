package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.report.*;
import fpt.edu.capstone.repository.MedicalReportRepository;
import fpt.edu.capstone.service.ReportService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class ReportServiceImpl implements ReportService {

    private final MedicalReportRepository medicalReportRepository;

    @Override
    public List<RevenueReport> getAllRevenueInBranch(Integer day, Integer month, int year, int branchId) {
        return medicalReportRepository.getRevenueByBranchId(day, month, year, branchId);
    }

    @Override
    public int getPatientExamined(Integer day, Integer month, int year, int branchId) {
        return medicalReportRepository.getPatientExaminedInBranch(day, month, year, branchId);
    }

    @Override
    public int getMedicalVisits(Integer day, Integer month, int year, int branchId) {
        return medicalReportRepository.getMedicalVisitsInBranch(day, month, year,branchId);
    }

    @Override
    public List<IPatientUsedService> getCostPatientUsed(Integer day, Integer month, int year, int branch) {
        return medicalReportRepository.getTotalCostPatientUsed(day, month, year, branch);
    }

    @Override
    public List<IHistoryUsedService> getHistoryPatientUsedService(Integer day, Integer month, int year, int branch, int patientId) {
        return medicalReportRepository.getHistoryPatientUsedService(day, month, year, branch, patientId);
    }

    @Override
    public List<ITotalCostByService> getCostByService(Integer day, Integer month, int year, int branch) {
        return medicalReportRepository.getRevenueByService(day, month, year, branch);
    }

    @Override
    public List<IPatientUsedService2> getPatientUsedService(Integer day, Integer month, int year, int branch, int serviceId) {
        return medicalReportRepository.getPatientUsedService2(day, month, year, branch, serviceId);
    }
}
