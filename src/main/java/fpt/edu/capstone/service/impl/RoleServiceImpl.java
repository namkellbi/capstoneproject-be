package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.entity.Role;
import fpt.edu.capstone.repository.RoleRepository;
import fpt.edu.capstone.service.RoleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;
    @Override
    public List<Role> getAllRole() {
        return roleRepository.findAll() ;
    }
}
