package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.entity.WorkSchedule;
import fpt.edu.capstone.repository.WorkScheduleRepository;
import fpt.edu.capstone.service.WorkScheduleService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class WorkScheduleServiceImpl implements WorkScheduleService {
    private final WorkScheduleRepository workScheduleRepository;
    @Override
    public WorkSchedule getById(int id) {
        return workScheduleRepository.getById(id);
    }
}
