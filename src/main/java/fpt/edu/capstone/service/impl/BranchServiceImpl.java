package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.dto.branch.BranchRequest;
import fpt.edu.capstone.entity.Branch;
import fpt.edu.capstone.repository.BranchRepository;
import fpt.edu.capstone.service.BranchService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class BranchServiceImpl implements BranchService {
    private final BranchRepository branchRepository;
    @Override
    public Branch getById(int id) {
        return branchRepository.findBranchById(id);
    }

    @Override
    public List<Branch> getAllBranch() {
        return branchRepository.findByIsAction(true);
    }

    @Override
    public List<Branch> getBranchByName(String branchName) {
        return branchRepository.findBranchByBranchName(branchName);
    }
    @Override
    public Branch deleteBranchById(int id){
        Branch branch = branchRepository.getById(id);
    //    BranchResponse branchResponse = new BranchResponse();
        branch.setAction(Boolean.valueOf("False"));
        return branchRepository.save(branch);
    }
    @Override
    public Branch saveBranch(BranchRequest branchRequest){
        Branch branch = new Branch();
        branch.setBranchName(branchRequest.getBranchName().trim());
        branch.setAddress(branchRequest.getAddress().trim());
        branch.setPhoneNumber(branchRequest.getPhoneNumber().trim());
        branch.setAction(Boolean.valueOf("True"));
        return branchRepository.save(branch);
    }
    @Override
    public Branch updateBranch(int id,BranchRequest branchRequest){
        Branch branch = branchRepository.getById(id);
        branch.setBranchId(id);
        branch.setBranchName(branchRequest.getBranchName().trim());
        branch.setPhoneNumber(branchRequest.getPhoneNumber().trim());
        branch.setAddress(branchRequest.getAddress().trim());
        branch.setAction(Boolean.valueOf("True"));
        return branchRepository.save(branch);
    }
}
