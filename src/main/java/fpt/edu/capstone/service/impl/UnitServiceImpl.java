package fpt.edu.capstone.service.impl;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.unit.UnitsRequest;
import fpt.edu.capstone.dto.unitgroup.UnitRequest;
import fpt.edu.capstone.dto.unitgroup.UnitResponse;
import fpt.edu.capstone.entity.Unit;
import fpt.edu.capstone.entity.UnitGroup;
import fpt.edu.capstone.exception.ClinicException;
import fpt.edu.capstone.repository.UnitGroupRepository;
import fpt.edu.capstone.repository.UnitRepository;
import fpt.edu.capstone.service.UnitService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class UnitServiceImpl implements UnitService {
    private final UnitRepository unitRepository;

    private final UnitGroupRepository unitGroupRepository;

    private final ModelMapper modelMapper;
    @Override
    public List<UnitResponse> getListUnit() {
        List<UnitResponse> unitResponses = new ArrayList<>();
        List<Unit> units =unitRepository.getAllUnit();
        for(Unit unit: units){
            UnitResponse unitResponse = modelMapper.map(unit, UnitResponse.class);
            UnitGroup unitGroup = unitGroupRepository.getById(unit.getUnitGroupId());
            unitResponse.setUnitGroupName(unitGroup.getName());
            unitResponses.add(unitResponse);
        }

        return unitResponses;
    }

    @Override
    public void deleteUnit(UnitsRequest unitsRequest) {
        for (Integer unitId : unitsRequest.getIdUnits()) {
            if (!unitRepository.findById(unitId).isPresent())
                throw new ClinicException(ResponseMessageConstants.UNIT_GROUP_DOES_NOT_EXIST);
        }
        for(Integer idUnit: unitsRequest.getIdUnits()){
            unitRepository.deleteUnitById(idUnit);
        }
    }

    @Override
    public void createUnit(UnitRequest request) {
        Optional<Unit> unit = unitRepository.findByUnitNameAndUse(request.getUnitName(), true);
        if(unit.isPresent()){
            throw new ClinicException(ResponseMessageConstants.UNIT_NAME_EXIST);
        }
        Optional<UnitGroup> unitGroup = unitGroupRepository.findById(request.getUnitGroupId());
        if(!unitGroup.isPresent()){
            throw new ClinicException(ResponseMessageConstants.UNIT_GROUP_DOES_NOT_EXIST);
        }
        Unit unitRequest = modelMapper.map(request,Unit.class);
        unitRequest.setUse(true);
        unitRepository.save(unitRequest);
    }

    @Override
    public List<Unit> getUnitByUnitGroupId(int idUnitGroup) {
        return unitRepository.getUnitByUnitGroupId(idUnitGroup);
    }
}
