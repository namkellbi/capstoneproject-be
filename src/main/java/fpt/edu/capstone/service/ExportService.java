package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.export.*;
import fpt.edu.capstone.dto.product.IProduct;

import java.util.List;

public interface ExportService {
    void createExport(ExportRequest exportRequest);
    List<IShortExport> getAllExport(int idBranch);
    List<IShortExport> getExportByStatus(int idBranch, int idStatus);
    List<IProductQuantity> getProductsQuantityById(int storeHouseId);
    IProductQuantity getProductQuantityVyId(int storeHouseId, int productId);
    List<IProduct> getProductsByIdStore(int idStore);
    void exportProducts(ExportProductsRequest exportProductsRequest);
    ExportShortResponse getExportShortById(int idExport);
    ExportDetailResponse getExportDetailById(int idExport);

}
