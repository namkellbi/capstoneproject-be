package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.consulting.*;
import fpt.edu.capstone.entity.ConsultingRoom;

import java.util.List;

public interface ConsultingRoomService {
    List<ConsultingRoom> getConsultingRoomByConsultingRoomId(int consultingRoomId);

    List<ConsultingRoom> getListConsultingRoom();

    ConsultingRoom getConsultingRoomById(int roomId);

    List<ConsultingRoom> getConsultingRoomByName(String name);

    void createConsultingRoom(ConsultingRoomRequest request);

    void updateConsultingRoom(ConsultingRoomResponse response);

    List<ConsultingRoom> getConsultingRoomByIdService(int idService);

    List<ConsultingServicesResponse> getConsultingServices(int idPatient, int idBranch);
    List<CheckUpResponse> getAllService();
    List<AllConsultingResponse> getAllConsultingFix(int idBranch);

    void deleteConsultingRoom(int idConsulting);
}
