package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.product.PTypeRespone;
import fpt.edu.capstone.dto.product.ProductTypeResponse;
import fpt.edu.capstone.dto.product.ProductTypeResquest;
import fpt.edu.capstone.entity.ProductType;

import java.util.List;

public interface ProductTypeService {
    ProductType saveProductType(ProductType productType);
    void deleteProductType(ProductTypeResquest productTypeResquest);
    List<ProductType> getListProductType();
    List<ProductTypeResponse> getListProduct();

    List<PTypeRespone> getProductType();
}
