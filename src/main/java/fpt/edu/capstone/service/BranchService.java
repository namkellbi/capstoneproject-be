package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.branch.BranchRequest;
import fpt.edu.capstone.entity.Branch;

import java.util.List;

public interface BranchService {
    Branch getById(int id);
    List<Branch> getAllBranch();
    List<Branch> getBranchByName(String branchName);
    Branch deleteBranchById(int id);
    Branch saveBranch(BranchRequest branchRequest);
    Branch updateBranch(int id,BranchRequest branchRequest);
}
