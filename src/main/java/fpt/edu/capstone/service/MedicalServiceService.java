package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.service.AllServiceRequest;
import fpt.edu.capstone.dto.service.ServiceRequestForCreate;
import fpt.edu.capstone.dto.service.ServiceRequestForGetAll;
import fpt.edu.capstone.dto.service.ServiceResponse;
import fpt.edu.capstone.entity.MedicalService;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MedicalServiceService {
    List<MedicalService> getAllService(int idBranch);
    List<ServiceResponse> getDataService(int consultingRoomId);
    List<ServiceResponse> getAllServiceName();
    List<ServiceResponse> findServiceByServiceName(String serviceName);
    void createService(ServiceRequestForCreate serviceRequestForCreate);
    List<MedicalService> getAllMedicalServiceById(AllServiceRequest AllServiceRequest);
}
