package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.transfer.*;

import java.util.List;

public interface TransferService {
    //create
    void createTransfer(TransferRequest transferRequest);
    void actionExportTransfer(TransferExportProductRequest exportProductRequest);
    List<ITransferReceiptResponse> getTransfersReceipt();
    List<ITransferReceiptResponse> getTransfersReceipt(Integer idStatus);
    List<ITransferExportResponse> getTransfersExport();
    List<ITransferExportResponse> getTransfersExport(Integer idStatus);
    TransferShExportResponse getTransferShExportById(int idTransfer);
    TransferShReceiptResponse getTransferShReceiptById(int idTransfer);
    TransferFullExportResponse getTransferFullExportById(int idTransfer);
    TransferFullReceiptResponse getTransferFullReceiptById(int idTransfer);
}
