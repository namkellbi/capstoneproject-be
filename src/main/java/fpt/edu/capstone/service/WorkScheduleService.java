package fpt.edu.capstone.service;

import fpt.edu.capstone.entity.WorkSchedule;

public interface WorkScheduleService {
    WorkSchedule getById(int id);
}
