package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.staff.*;
import fpt.edu.capstone.entity.Staff;

import java.util.List;

public interface StaffService {
    Staff findStaffByStaffId(int id);
    List<StaffName_ConsultingRoom> getAllStaffName(int consultingRoomId);
    void updateStaffPosition(Staff_ConsultingRoomRequestTo staff_consultingRoomRequestTo);
    void save(Staff staff);
    List<StaffForChangePosition> getAllStaffForChange();
    List<IStaffResponse> getAllStaff(Integer role,  Integer branchId);
}
