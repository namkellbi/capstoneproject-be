package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.account.LoginResponse;
import fpt.edu.capstone.dto.account.RegisterRequest;
import fpt.edu.capstone.entity.Account;

public interface AuthenticationService {
    LoginResponse login(LoginRequest request) throws Exception;
    void register(RegisterRequest request);
    void saveAccount(Account account);
    void forgotPassword(String email) throws Exception;
}
