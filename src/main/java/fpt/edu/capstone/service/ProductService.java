package fpt.edu.capstone.service;

import fpt.edu.capstone.dto.product.IProductResponse;
import fpt.edu.capstone.dto.product.ProductRequest;
import fpt.edu.capstone.dto.product.ProductResponse;
import fpt.edu.capstone.dto.productGroup.IProductGroupResponse;
import fpt.edu.capstone.dto.productGroup.PrGroupUpRequest;
import fpt.edu.capstone.dto.productGroup.ProductGroupRequest;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface ProductService {
    void createProductGroup(ProductGroupRequest productGroupRequest);
    List<IProductGroupResponse> getAllProductGroup();
    void updateProductGroup(PrGroupUpRequest prGroupUpRequest);
    void deleteProduct(int idProductGroup);
    void createProduct(ProductRequest productRequest);
    List<ProductResponse> getAllProduct(Integer storeId, Integer branchId);
    List<ProductResponse> getAllProduct1();

}
