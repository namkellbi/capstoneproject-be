package fpt.edu.capstone.dto.productGroup;

public interface IProductGroup {
    int getProductGroupId();
    String getProductGroupName();
}
