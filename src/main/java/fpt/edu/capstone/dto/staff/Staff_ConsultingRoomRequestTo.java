package fpt.edu.capstone.dto.staff;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class Staff_ConsultingRoomRequestTo {
    List<Staff_ConsultingRoomRequestForEach> listStaffUpdate;
}
