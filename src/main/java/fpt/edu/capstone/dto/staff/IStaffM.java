package fpt.edu.capstone.dto.staff;

public interface IStaffM {
    String getAvatar();
    String getEducation();
    String getCertificate();
    int getBranchId();
    int getConsultingRoomId();
    String getPossition();
}
