package fpt.edu.capstone.dto.staff;

public interface StaffForChangePosition {
    int getAccountId();
    String getFullName();
    String getPossition();
    String getRoomName();
    int getConsultingRoomId();
}
