package fpt.edu.capstone.dto.staff;

import javax.persistence.Column;
import javax.persistence.Id;

public interface Staff_ConsultingRoom {
     int getStaffId();
     int getConsultingRoomId();
     String getPossition();
}
