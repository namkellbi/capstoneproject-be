package fpt.edu.capstone.dto.staff;

import java.util.Date;

public interface IStaffResponse {
    int getAccountId();
    String getFullName();
    Date getDob();
    String getAddress();
    String getVillage();
    String getDistrict();
    String getProvince();
    String getSex();
    String getIdentityCard();
    String getPhoneNumber();
    int getRoleId();
    String getRoleName();
    String getEducation();
    String getCertificate();
    int getBranchId();
    String getBranchName();



}
