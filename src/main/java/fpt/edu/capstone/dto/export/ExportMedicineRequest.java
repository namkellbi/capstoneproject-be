package fpt.edu.capstone.dto.export;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExportMedicineRequest {
    private int idReceipt;
    private int idMedicine;
    private int amount;
//    private double exportPrice;
}
