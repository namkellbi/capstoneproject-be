package fpt.edu.capstone.dto.export;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ExportDetailResponse {
    private int receiptIssueId;
    private int storeHouseId;
    private String storeHouseName;
    private String petitionerName;
    private String personInCharge;
    private Date exportDate;
    private List<IProductExportResponse> iProductExportResponses;
}
