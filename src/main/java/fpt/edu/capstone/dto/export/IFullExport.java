package fpt.edu.capstone.dto.export;

import java.util.Date;

public interface IFullExport {
    int getReceiptIssueId();
    int getStoreHouseId();
    String getStoreHouseName();
    String getPetitionerName();
    String getPersonInCharge();
    Date getExportDate();
}
