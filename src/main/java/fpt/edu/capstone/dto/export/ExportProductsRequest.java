package fpt.edu.capstone.dto.export;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ExportProductsRequest {
    private int idExport;
    private int storeHouseId;
    private int personInChargeId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date exportDate;
    private List<ProductExportRequest> productExportRequests;
}
