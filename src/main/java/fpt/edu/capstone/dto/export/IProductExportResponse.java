package fpt.edu.capstone.dto.export;

import java.util.Date;


public interface IProductExportResponse {
     int getProductId();
     String getProductName();
     int getNumberOfRequests();
     Integer getAmount();
     String getUnitName();
     String getNote();

}
