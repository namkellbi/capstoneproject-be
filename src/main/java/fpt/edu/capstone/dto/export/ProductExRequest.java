package fpt.edu.capstone.dto.export;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductExRequest {
    private int productId;
    private int numberOfRequest;
}
