package fpt.edu.capstone.dto.export;

public interface IProductQuantity {
    int getProductId();
    String getProductName();
    int getQuantity();
    String getUnitName();
}
