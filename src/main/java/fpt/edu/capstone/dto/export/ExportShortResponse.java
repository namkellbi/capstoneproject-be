package fpt.edu.capstone.dto.export;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@Getter
@Setter
public class ExportShortResponse {
    private int receiptIssueId;
    private String petitionerName;
    private Date expectedDate;
    private int statusID;
    private String statusName;
    private List<IProductExportResponse> iProductExportResponses;
}
