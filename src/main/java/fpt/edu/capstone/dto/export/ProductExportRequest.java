package fpt.edu.capstone.dto.export;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductExportRequest {
    private int idProduct;
    private int amount;
    private String note;
}
