package fpt.edu.capstone.dto.contact;

import java.util.Date;

public interface IContact {
    int getAccountId();
    String getFullname();
    Date getDob();
    String getPastMedicalHistory();
}
