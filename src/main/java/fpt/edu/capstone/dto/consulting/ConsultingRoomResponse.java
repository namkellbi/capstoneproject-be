package fpt.edu.capstone.dto.consulting;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConsultingRoomResponse {
    private int consultingRoomId;
    private String roomName;
    private String abbreviationName;
    private int branchId;
}
