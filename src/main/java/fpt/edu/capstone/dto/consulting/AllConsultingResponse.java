package fpt.edu.capstone.dto.consulting;

public interface AllConsultingResponse {
    int getConsultingRoomId();
    String getRoomName();
    String getAbbreviationName();
    String getBranchId();
    boolean getIsAction();
    String getBranchName();
}
