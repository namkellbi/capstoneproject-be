package fpt.edu.capstone.dto.consulting;

public interface IConsultingSTT {
    int getMedicalReportId();
    int getSTT();
    int getConsultingRoomId();
    String getRoomName();
    int getStatusId();
    String getStatusName();
    int getAccountId();
}
