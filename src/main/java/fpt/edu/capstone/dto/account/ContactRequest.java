package fpt.edu.capstone.dto.account;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Date;
@Getter
@Setter
public class ContactRequest {
  //  private int accountId;
    private String fullName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dob;
    private String address;
    private String village;
    private String district;
    private String province;
    private String sex;
    private String identityCard;
    private String phoneNumber;
    private String ethnicity;
    private String job;
    private String email;
}
