package fpt.edu.capstone.dto.account;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginResponse {
    private int accountId;
    private String username;
    private String password;
    private String fullName;
    private int roleId;
    private int branchId;
    private int consultingId;
    private String avatar;
}
