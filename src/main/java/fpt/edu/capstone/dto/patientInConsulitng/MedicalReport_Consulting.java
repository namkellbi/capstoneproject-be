package fpt.edu.capstone.dto.patientInConsulitng;

public interface MedicalReport_Consulting {
    int getMedicalReportId();
    int getConsultingRoomId();
    int getMedicalServiceId();
    int getStatusId();
    int getSTT();
}
