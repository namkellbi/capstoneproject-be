package fpt.edu.capstone.dto.transfer;

import java.util.Date;

public interface IFullTransferReceipt {
    int getReceiptId();

    String getSourceStoreName();

    String getDestinationStoreName();

    Date getExpectedDate();

    String getPetitionerName();
}
