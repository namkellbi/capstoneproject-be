package fpt.edu.capstone.dto.transfer;

import com.fasterxml.jackson.annotation.JsonFormat;
import fpt.edu.capstone.dto.export.ProductExportRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@Getter
@Setter
public class TransferExportProductRequest {
    private int idExport;
    private int personInChargeId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date exportDate;
    private List<ProductExportRequest> productExportRequests;
}
