package fpt.edu.capstone.dto.transfer;

import java.util.Date;

public interface ITransferReceiptResponse {
    int getReceiptId();

    Integer getSourceStoreId();
    Integer getDestinationStoreId();
    String getSourceStoreName();

    String getDestinationStoreName();

    Date getExpectedDate();

    int getStatusID();

    String getStatusName();
}
