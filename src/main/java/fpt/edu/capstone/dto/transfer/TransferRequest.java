package fpt.edu.capstone.dto.transfer;

import com.fasterxml.jackson.annotation.JsonFormat;
import fpt.edu.capstone.dto.export.ProductExRequest;
import fpt.edu.capstone.dto.product.ProductReRequest;
import lombok.Getter;
import lombok.Setter;


import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TransferRequest {
    private int receivingWareHouseId;
    private int wareHouseTransferId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date expectedDate;
    private int petitionerId;

    List<ProductExRequest> productExRequests;
}
