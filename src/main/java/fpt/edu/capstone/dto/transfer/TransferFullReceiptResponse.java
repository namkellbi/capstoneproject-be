package fpt.edu.capstone.dto.transfer;

import fpt.edu.capstone.dto.medicine.IProductReResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TransferFullReceiptResponse {
    private int receiptId;
    private String sourceStoreName;
    private String destinationStoreName;
    private Date expectedDate;
    private String petitionerName;
   private List<IProductReResponse> productsResponse;


}
