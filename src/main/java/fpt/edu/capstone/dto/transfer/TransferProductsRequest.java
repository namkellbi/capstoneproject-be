package fpt.edu.capstone.dto.transfer;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class TransferProductsRequest {
    private int idTransfer;
    private int sourceWarehouseId;
    private int destinationWarehouseId;
    private int importerId;
//    private int amountRequest;
//    private Date dateRequest;
//    private int exporterId;
//    private int importerId;
//    private String note;
//    List<MedicineT2Request> medicineRequests;
//    List<EquipmentT2Request> equipmentRequests;
}
