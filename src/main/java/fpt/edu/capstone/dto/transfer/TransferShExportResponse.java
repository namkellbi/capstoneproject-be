package fpt.edu.capstone.dto.transfer;

import fpt.edu.capstone.dto.export.IProductExportResponse;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class TransferShExportResponse {
    private int exportId;
    private String sourceStoreName;
    private String destinationStoreName;
    private Date expectedDate;
    private int statusID;
    private String statusName;
    private List<IProductExportResponse> productResponses;

}
