package fpt.edu.capstone.dto.transfer;

import java.util.Date;

public interface ITransferFull {
    int getExportId();

    String getSourceStoreName();

    String getDestinationStoreName();

    String getPetitionerName();

    String getPersonInChargeName();

    Date getExpectedDate();

    Date getExportDate();
}
