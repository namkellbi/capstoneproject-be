package fpt.edu.capstone.dto.medicine;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class MedicineRequest {
    private String medicineName;
    private int unitId;
    private String note;
    private List<Integer> medicineTypeIds;
}
