package fpt.edu.capstone.dto.medicine;

public interface MedicineTypeResponse {
    int getMedicineTypeId();
    String getMedicineTypeName();
    int getProductTypeId();
    String getProductTypeName();
}
