package fpt.edu.capstone.dto.medicine;

public interface IMedicine_Type {
    public int getMedicineTypeId();

    public String getMedicineTypeName();

    public int getProductTypeId();
}
