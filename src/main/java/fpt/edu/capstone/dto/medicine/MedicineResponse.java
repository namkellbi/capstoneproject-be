package fpt.edu.capstone.dto.medicine;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class MedicineResponse {
    private int medicineId;
    private String medicineName;
    private double price;
    private int unitId;
    private String unitName;
    private String note;
    private List<IMedicine_Type> medicineTypes;
}
