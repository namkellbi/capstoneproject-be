package fpt.edu.capstone.dto.medicine;

public interface IProductReResponse {
    int getReceiptId();
    int getProductId();
    String getProductName();
    int getNumberOfRequests();
    String  getUnitName();
    Integer getNumberReceipted();
    Integer getAmount();
}
