package fpt.edu.capstone.dto.medicine;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class MedicineTypeRequest {
    private List<Integer> medicineTypeId;
}
