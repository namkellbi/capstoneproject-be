package fpt.edu.capstone.dto.storeHouse;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class StoreHouseResponse {
    int storeHouseId;
    String storeHouseName;
    int requestInternalTransfer;
    int requestReceipt;
    int requestInventory;
}
