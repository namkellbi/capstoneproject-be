package fpt.edu.capstone.dto.storeHouse;

public interface IEquipmentStoreResponse {
    String getEquipmentName();
    int getAmount();
    int getEquipmentId();
    int getUnitId();
    String getUnitName();
}
