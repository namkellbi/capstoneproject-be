package fpt.edu.capstone.dto.storeHouse;

public interface IEquipmentStore {
    int getEquipmentId();
    int getStoreHouseId();
    int getAmount();
    int getReceiptId();
}
