package fpt.edu.capstone.dto.storeHouse;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
@Getter
@Setter
public class MedicineStoreRequest {
    @NotNull
    private int idReceipt;
    @NotNull
    private int idMedicine;
    @NotNull
    private int quantityStock;
    @NotNull
    private int actualQuantity;
    @NotNull
    private int deviant;
    @NotBlank
    private String note;
    @NotBlank
    private int unitId;
}
