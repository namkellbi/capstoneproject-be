package fpt.edu.capstone.dto.storeHouse;

public interface IMedicineStoreResponse {
    int getMedicineId();
    String getMedicineName();
    int getAmount();
    int getUnitId();
    String getUnitName();
}
