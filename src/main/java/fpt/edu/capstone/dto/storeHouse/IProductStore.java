package fpt.edu.capstone.dto.storeHouse;

public interface IProductStore {
    int getProductID();
    int getStoreHouseId();

}
