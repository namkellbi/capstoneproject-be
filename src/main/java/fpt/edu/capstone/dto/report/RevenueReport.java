package fpt.edu.capstone.dto.report;

public interface RevenueReport {
    int getBranchId();
    String getBranchName();
    double getTotalCost();
    int getMonth();
    int getYear();
}
