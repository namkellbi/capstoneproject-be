package fpt.edu.capstone.dto.report;

import java.util.Date;

public interface IPatientUsedService2 {
    int getAccountId();
    String getPatientName();
    String getAddress();
    String getVillage();
    String getDistrict();
    String getProvince();
    int getMedicalServiceId();
    String getServiceName();
    Date getDate();
    double getPriceService();

}
