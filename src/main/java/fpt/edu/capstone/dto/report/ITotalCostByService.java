package fpt.edu.capstone.dto.report;

public interface ITotalCostByService {
    int getMedicalServiceId();
    String getServiceName();
    int getBranchId();
    String getBranchName();
    double getTotalCost();
    int getMedicalVisits();
}
