package fpt.edu.capstone.dto.report;

public interface IPatientUsedService {
    int getAccountId();
    String getPatientName();
    String getAddress();
    String getVillage();
    String getDistrict();
    String getProvince();
    int getBranchId();
    String getBranchName();
    double getTotalCost();
}
