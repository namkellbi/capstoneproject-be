package fpt.edu.capstone.dto.report;

import java.util.Date;

public interface IHistoryUsedService {
    int getAccountId();
    String getPatientName();
    int getBranchId();
    String getBranchName();
    double getPriceService();
    String getServiceName();
    Date getDate();
}
