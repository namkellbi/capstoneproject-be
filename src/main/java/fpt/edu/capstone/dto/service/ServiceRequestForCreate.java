package fpt.edu.capstone.dto.service;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ServiceRequestForCreate {
    private String serviceName;
    private int consultingRoomId;
    private double money;
}
