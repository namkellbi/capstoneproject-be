package fpt.edu.capstone.dto.receipt;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface IShortReceipt {
    int getReceiptIssueId();
    int getReceivingWarehouseID();
    String getStoreHouseName();
    String getSupplierName();
    @JsonFormat(pattern = "yyyy-MM-dd")
    Date getExpectedDate();
    int getStatusID();
    String getStatusName();
}
