package fpt.edu.capstone.dto.receipt;

import java.util.Date;

public interface IReceiptHistoryResponse {
    int getReceiptedId();
    Date getDateReceipt();
    String getAvatar();
    String getFullName();
}
