package fpt.edu.capstone.dto.receipt;

import com.fasterxml.jackson.annotation.JsonFormat;
import fpt.edu.capstone.dto.medicine.IProductReResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ReceiptProductsResponse {
    private int ReceiptIssueId;
    private int ReceivingWarehouseID;
    private String StoreHouseName;
    private String SupplierName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date ExpectedDate;
    private int StatusID;
    private String StatusName;
    //do nham sql
    List<IProductReResponse> productsResponse;

}
