package fpt.edu.capstone.dto.receipt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@Getter
@Setter
public class UpReceiptRequest {
    private int idReceipt;
    private int storeHouseId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
    List<MedicineRRequest> medicineRequests;
    List<EquipmentRRequest>equipmentRequests;

}
