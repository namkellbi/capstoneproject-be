package fpt.edu.capstone.dto.receipt;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EquipmentRRequest {
    private int idEquipment;
    private int amount;
    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass())
            return false;
        EquipmentRRequest mr = (EquipmentRRequest) obj;
        return mr.getIdEquipment() == this.getIdEquipment() && mr.getAmount()==this.getAmount();
    }
}
