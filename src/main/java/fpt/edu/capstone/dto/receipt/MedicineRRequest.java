package fpt.edu.capstone.dto.receipt;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicineRRequest {
    private int idMedicine;
    private int amount;

    @Override
    public boolean equals(Object obj) {
        if (obj.getClass() != this.getClass())
            return false;
        MedicineRRequest mr = (MedicineRRequest) obj;
        return mr.getIdMedicine() == this.getIdMedicine() && mr.getAmount()==this.getAmount();
    }
}
