package fpt.edu.capstone.dto.receipt;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@Getter
@Setter
public class ReceiptShortProduct {
    int idReceipt;
    int storeHouseId;
    String storeHouseName;
    Double totalCost;
    @JsonFormat(pattern = "yyy-MM-dd")
    Date date;
    int statusId;
    String statusName;
    String serialNumber;
    List<IMedicineShort> iMedicineShorts;
    List<IEquipmentShort> iEquipmentShorts;
}
