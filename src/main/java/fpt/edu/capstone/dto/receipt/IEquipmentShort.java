package fpt.edu.capstone.dto.receipt;

import java.util.Date;

public interface IEquipmentShort {
    int getReceiptId();
    int getEquipmentId();
    String getEquipmentName();
    int getAmount();
    String  getNote();
    int getUnitId();
    String getUnitName();
    Double getEntryPrice();
    Integer getSupplierId();


}
