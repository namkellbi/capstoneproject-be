package fpt.edu.capstone.dto.receipt;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;


public interface IReceiptResponse {
     int getIdReceipt();
     int getStoreHouseId();
     String getStoreHouseName();
     Integer getPersonInChargeId();
     String getPersonName();
     Double getTotalCost();
     @JsonFormat(pattern = "yyyy-MM-dd")
     Date getDate();
     int getStatusId();
     String getStatusName();
     String getSerialNumber();
}
