package fpt.edu.capstone.dto.receipt;

import com.fasterxml.jackson.annotation.JsonFormat;
import fpt.edu.capstone.dto.equipment.ReceiptEquipmentRequest;
import fpt.edu.capstone.dto.medicine.ReProductRequest;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
public class ReceiptProductRequest {
    private int receiptId;
    private int personInChargeId;
//    private int storeHouseId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
    private List<ReProductRequest> productRequests;
}
