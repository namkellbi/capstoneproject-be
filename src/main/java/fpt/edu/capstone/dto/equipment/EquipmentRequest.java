package fpt.edu.capstone.dto.equipment;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class EquipmentRequest {
    private String name;
    private String note;
    private int unitId;
    private List<Integer> equipmentTypeIds;
}
