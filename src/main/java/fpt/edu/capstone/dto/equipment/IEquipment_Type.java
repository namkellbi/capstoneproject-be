package fpt.edu.capstone.dto.equipment;

public interface IEquipment_Type {
    public int getEquipmentTypeId();

    public String getEquipmentTypeName();

    public int getProductTypeId();
}
