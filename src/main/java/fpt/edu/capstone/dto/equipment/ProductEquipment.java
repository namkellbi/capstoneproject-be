package fpt.edu.capstone.dto.equipment;

public interface ProductEquipment {
    int getProductTypeId();
    String getName();
    int getEquipmentTypeId();
    String getEquipmentTypeName();
}
