package fpt.edu.capstone.dto.equipment;

import java.util.Date;

public interface IEquipmentRResponse {
    int getReceiptId();
    int getMedicineId();
    String getMedicineName();
    int getAmount();
    Double getEntryPrice();
    String getNote();
    Date getDateOfManufacture();
    Date getExpirationDate();
    Integer getSupplierId();
    String getSupplierName();
}
