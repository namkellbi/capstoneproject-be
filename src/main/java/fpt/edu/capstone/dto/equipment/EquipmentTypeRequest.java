package fpt.edu.capstone.dto.equipment;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class EquipmentTypeRequest {
    private List<Integer> equipmentTypeId;
}
