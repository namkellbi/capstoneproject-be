package fpt.edu.capstone.dto.equipment;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReceiptEquipmentRequest {
    private int idEquipment;
    private int quantity;
    private String note;
    private int entryPrice;
    private int idSupplier;
}
