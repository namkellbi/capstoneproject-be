package fpt.edu.capstone.dto.equipment;

import lombok.Getter;
import lombok.Setter;


public interface IEquipmentResponse {
    int getEquipmentId();
    String getEquipmentName();
    int getUnitId();

    String getUnitName();

}
