package fpt.edu.capstone.dto.unitgroup;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class UnitGroupRequest {
    @NotBlank
    private String name;
}
