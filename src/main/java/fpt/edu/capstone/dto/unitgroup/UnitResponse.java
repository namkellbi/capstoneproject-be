package fpt.edu.capstone.dto.unitgroup;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class UnitResponse {
    private int unitId;
    private String unitName;
    private int unitGroupId;
    private String unitGroupName;
}
