package fpt.edu.capstone.dto.unitgroup;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Getter
@Setter
public class UnitGroupsRequest {
    List<Integer> unitGroupsId;
}
