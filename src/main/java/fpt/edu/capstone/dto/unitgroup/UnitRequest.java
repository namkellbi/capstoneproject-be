package fpt.edu.capstone.dto.unitgroup;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class UnitRequest {
    @NotBlank
    private String unitName;
    @NotNull
    private int unitGroupId;
}
