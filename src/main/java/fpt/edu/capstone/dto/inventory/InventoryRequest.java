package fpt.edu.capstone.dto.inventory;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class InventoryRequest {
    private int storeHouseId;
    private int petitionerId;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;

}
