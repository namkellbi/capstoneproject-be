package fpt.edu.capstone.dto.inventory;

public interface IInventoryProductResponse {
    int getInventoryId();
    int getProductID();
    String getProductName();
    int getQuantityStock();
    int getActualQuantity();
    int getDeviant();
    String getUnitName();
    String getNote();
}
