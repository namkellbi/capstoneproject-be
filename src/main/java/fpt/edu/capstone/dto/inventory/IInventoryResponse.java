package fpt.edu.capstone.dto.inventory;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

public interface IInventoryResponse {
    int getInventoryId();
    Date getDate();
    int getStoreHouseId();
    String getStoreHouseName();
    String getBranchName();
    String getStatusID();
    String getStatusName();
}
