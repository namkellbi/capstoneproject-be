package fpt.edu.capstone.dto.medicalrecord;

import java.util.Date;

public interface IMedicalReport {
     int getMedicalReportId();
     Date getDate();
     int getAccountId();
     boolean getIsFinishedExamination();

}
