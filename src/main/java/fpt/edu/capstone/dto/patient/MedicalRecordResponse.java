package fpt.edu.capstone.dto.patient;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import java.util.Date;

@Getter
@Setter
public class MedicalRecordResponse {
    private int accountId;
    private String fullName;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dob;
    private String address;
    private String village;
    private String district;
    private String province;
    private String sex;
    private String identityCard;
    private String phoneNumber;
    private String ethnicity;
    private String job;
//    private boolean status;
//    private int roleId;
    private String email;

    private int medicalRecordId;
    private Integer circuit;
    private Integer bloodPressure;
    private Float BMI;
    private Float Height;
    private Float Temperature;
    private Float Weight;
    private String pastMedicalHistory;
    private String allergies;
    private String note;
    private String icd10;
    private String icd;
    private String advice;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date date;

}
