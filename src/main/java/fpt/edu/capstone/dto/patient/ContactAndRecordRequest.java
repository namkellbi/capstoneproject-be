package fpt.edu.capstone.dto.patient;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Getter
@Setter
public class ContactAndRecordRequest {

    private int accountId;
    private String fullName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date dob;

    private String address;
    private String village;
    private String district;
    private String province;
    private String sex;
    private String identityCard;
    private String phoneNumber;
    private String ethnicity;
    private String job;
    private String email;


    private int medicalRecordId;
    private Integer circuit;
    private Integer bloodPressure;
    private Float BMI;
    private Float Height;
    private Float Temperature;
    private Float Weight;
    private String pastMedicalHistory;
    private String allergies;
    private String note;
    private String icd10;
    private String icd;
    private String advice;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date date;
}
