package fpt.edu.capstone.dto.patient;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Required;

import java.util.Date;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PatientResponse {
    private int accountId;
    private String fullName;
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date dob;
    private int idConsultingRoom;
    private String consultingRoomName;
    private String sex;
    private String address;
    private String allergies;
    private String pastMedicalHistory;
    private String phoneNumber;
    private String identityCard;
    private String village;
    private String district;
    private String province;
    private String email;
    private int idMedicalReport;
    private int stt;
    private int idStatus;
    private String statusName;

}
