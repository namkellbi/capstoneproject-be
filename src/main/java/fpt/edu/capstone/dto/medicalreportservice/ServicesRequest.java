package fpt.edu.capstone.dto.medicalreportservice;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;
@Getter
@Setter
public class ServicesRequest {
//    private boolean paymentType;
//    private int staffId;
//    private int patientId;
//    private int branchId;
//    private Date date;
//    private double totalCost;
//    private Integer idPatient;
    private List<Integer> idServices;
}
