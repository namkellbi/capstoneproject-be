package fpt.edu.capstone.dto.medicalreportservice;

public interface IReportServiceResponse {
    int getReportServiceId();
    String getServiceName();
    double getPrice();
    double getTaxPercent();
    double getTotalCost();
    boolean getIsPay();
    String getRoomName();
    Integer getReportId();
    Integer getServiceId();
}
