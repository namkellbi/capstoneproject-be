package fpt.edu.capstone.dto.vnpay;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PaymentDTO {
    private int billId;
    private int amount;
    private String orderType;
    private String description;
    private String bankCode;
}
