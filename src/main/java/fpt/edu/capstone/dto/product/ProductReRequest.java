package fpt.edu.capstone.dto.product;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductReRequest {
    private int productId;
    private int numberOfRequest;
    private double entryPrice;
}
