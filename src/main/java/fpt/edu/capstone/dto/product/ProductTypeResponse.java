package fpt.edu.capstone.dto.product;

public interface ProductTypeResponse {
    int getQuantity();
    int getProductTypeId();
    String getProductTypeName();
}
