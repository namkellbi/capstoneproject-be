package fpt.edu.capstone.dto.product;

public interface IProduct {
    int getProductID();
    String getProductName();
}
