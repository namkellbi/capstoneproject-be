package fpt.edu.capstone.repository;

import fpt.edu.capstone.entity.Account;
import fpt.edu.capstone.entity.Contact;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Integer> {

    Optional<Contact> findByAccountIdAndStatus(int accountId, boolean status);

    @Query(value = "Select * from Contact where roleId = ?1", nativeQuery = true)
    List<Contact> findContactByRoleId(int roleId);

    @Query(value = "select distinct mr.AccountId from MedicalReport mr " +
            "join MedicalReport_Consulting mc on mr.MedicalReportId = mc.MedicalReportId " +
            "join ConsultingRoom cr on cr.ConsultingRoomId = mc.ConsultingRoomId " +
            "where mr.IsFinishedExamination = 0 and cr.BranchId = ?1", nativeQuery = true)
    List<Integer> getIdPatientExaminationByIdBranch(int idBranch);
    @Query(value = "select c from Contact c where c.roleId = ?1 and c.status = true and c.accountId = ?2")
    Contact findContactByRoleIdAndStatusT(int roleId, int idAccount);

    @Query(value = "Select * from Contact where roleId = ?1 and status = '0'", nativeQuery = true)
    List<Contact> findContactByRoleIdAndStatusF(int roleId);

    @Query(value = "Select * from Contact where roleId = 3 or roleId = 4 or roleId = 5 or roleId = 6", nativeQuery = true)
    List<Contact> findStaffByRoleId();

    @Query(value = "Select * from Contact where roleId != '2' and accountId=?1", nativeQuery = true)
    Contact findStaffByRoleIdAndAccountId(int accountId);

    @Query(value = "select * from Contact where roleId != '2' and (lower(fullName) like lower(concat('%', :fullName ,'%')) or :fullName is null or :fullName like '')", nativeQuery = true)
    List<Contact> findStaffByName(@Param("fullName") String fullName);

    @Query(value = "select * from Contact where status =:status and roleId = 1 " +
            "and  (lower(fullName) like lower(concat('%', :fullName ,'%')) or :fullName is null or :fullName like '' )", nativeQuery = true)
    List<Contact> findContactByNameAndStatus(@Param("fullName") String fullName, @Param("status") boolean status);
    @Query(value = "select * from Contact where roleId = 1 " +
            "and  (lower(fullName) like lower(concat('%', :fullName ,'%')) or :fullName is null or :fullName like '' )", nativeQuery = true)
    List<Contact> findContactByName(@Param("fullName") String fullName);
    @Query(value = "select a from Account a where lower(a.username) like lower(concat('%', :userName ,'%'))")
    Account findContactByExactName(@Param("userName") String userName);

    @Query(value = "select distinct c from Invoice i " +
            "join MedicalReport mr on i.medicalReportId = mr.medicalReportId " +
            "join Contact c on c.accountId = mr.accountId " +
            "where i.status = false and i.branchId = ?1")
    List<Contact> findContactPaymentByIdBranch(int idBranch);

    @Query(value = "select distinct c from Invoice i " +
            "join MedicalReport mr on i.medicalReportId = mr.medicalReportId " +
            "join Contact c on c.accountId = mr.accountId " +
            "where i.status = false and i.branchId =:idBranch " +
            "and (lower(c.fullName) like lower(concat('%', :name , '%')) or :name is null or :name like '')")
    List<Contact> findContactPaymentByIdBranchAndName(@Param("idBranch") int idBranch, @Param("name") String name);

    @Query(value = "select c from Contact c")
    Page<Contact> getAllContact(Pageable pageable);

    Optional<Contact> findByEmail(String email);
}
