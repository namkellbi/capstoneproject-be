package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.medicalreportservice.IReportServiceResponse;
import fpt.edu.capstone.entity.MedicalReportService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface MedicalReportServiceRepository extends JpaRepository<MedicalReportService, Integer> {

    @Query(value = "select SUM(TotalCost) from MedicalReport_Service where  InvoiceDetailId = ?1", nativeQuery = true)
    double getSumCostService(int invoiceDetailId);

    @Query(value = "select mrs.reportServiceId as reportServiceId,  ms.serviceName as serviceName, ms.price as price " +
            ",it.taxCode as taxPercent, mrs.totalCost as totalCost, mrs.isPay as isPay, cr.roomName as roomName, mrs.medicalReportId as reportId, ms.medicalServiceId as serviceId " +
            "from MedicalReport mr " +
            "join MedicalReportService mrs on mr.medicalReportId = mrs.medicalReportId " +
            "join MedicalService ms on mrs.serviceId = ms.medicalServiceId " +
            "join InvoiceDetail id on id.invoiceDetailId = mrs.InvoiceDetailId " +
            "join InvoiceType it on id.invoiceTypeId = it.invoiceTypeId " +
            "join ConsultingRoom cr on cr.consultingRoomId = ms.consultingRoomId " +
            "where  mr.medicalReportId = ?1 ")
    List<IReportServiceResponse> getReportServicePatient(int idMedicalReport);
    @Query(value = "select mrs.reportServiceId as reportServiceId,  ms.serviceName as serviceName, ms.price as price " +
            ",it.taxCode as taxPercent, mrs.totalCost as totalCost, mrs.isPay as isPay, cr.roomName as roomName, mrs.medicalReportId as reportId, ms.medicalServiceId as serviceId " +
            "from MedicalReport mr " +
            "join MedicalReportService mrs on mr.medicalReportId = mrs.medicalReportId " +
            "join MedicalService ms on mrs.serviceId = ms.medicalServiceId " +
            "join InvoiceDetail id on id.invoiceDetailId = mrs.InvoiceDetailId " +
            "join InvoiceType it on id.invoiceTypeId = it.invoiceTypeId " +
            "join ConsultingRoom cr on cr.consultingRoomId = ms.consultingRoomId " +
            "where  mr.medicalReportId = ?1 and cr.consultingRoomId =?2")
    List<IReportServiceResponse> getReportServicePatientAndRoom(int idMedicalReport, int idConsulting);

    @Query(value = "select mrs.reportServiceId as reportServiceId,  ms.serviceName as serviceName, ms.price as price " +
            ",it.taxCode as taxPercent, mrs.totalCost as totalCost, mrs.isPay as isPay, cr.roomName as roomName " +
            "from MedicalReport mr " +
            "join MedicalReportService mrs on mr.medicalReportId = mrs.medicalReportId " +
            "join MedicalService ms on mrs.serviceId = ms.medicalServiceId " +
            "join InvoiceDetail id on id.invoiceDetailId = mrs.InvoiceDetailId " +
            "join InvoiceType it on id.invoiceTypeId = it.invoiceTypeId " +
            "join ConsultingRoom cr on cr.consultingRoomId = ms.consultingRoomId " +
            "where  mrs.reportServiceId = ?1 ")
    IReportServiceResponse getReportServiceById(int idReportService);

    @Query(value = "select mrs.reportServiceId as reportServiceId,  ms.serviceName as serviceName, ms.price as price " +
            ",it.taxCode as taxPercent, mrs.totalCost as totalCost, mrs.isPay as isPay, cr.roomName as roomName " +
            "from MedicalReport mr " +
            "join MedicalReportService mrs on mr.medicalReportId = mrs.medicalReportId " +
            "join MedicalService ms on mrs.serviceId = ms.medicalServiceId " +
            "join InvoiceDetail id on id.invoiceDetailId = mrs.InvoiceDetailId " +
            "join InvoiceType it on id.invoiceTypeId = it.invoiceTypeId " +
            "join ConsultingRoom cr on cr.consultingRoomId = ms.consultingRoomId " +
            "where  mr.medicalReportId =?1 and mrs.isPay = ?2")
    List<IReportServiceResponse> getReportServicePatientIsPaid(int idMedicalReport,boolean isPaid);
}
