package fpt.edu.capstone.repository;

import fpt.edu.capstone.entity.CreateReceiptProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CReceiptProductRepository extends JpaRepository<CreateReceiptProduct,Integer> {
}
