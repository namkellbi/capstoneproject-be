package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.export.*;
import fpt.edu.capstone.dto.storeHouse.IEquipmentStore;
import fpt.edu.capstone.dto.storeHouse.IEquipmentStoreResponse;
import fpt.edu.capstone.dto.storeHouse.IProductStore;
import fpt.edu.capstone.dto.storeHouse.IMedicineStoreResponse;
import fpt.edu.capstone.entity.StoreHouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Repository
public interface StoreHouseRepository extends JpaRepository<StoreHouse, Integer> {
    @Query(value = "select sh from StoreHouse sh where sh.isAction = true")
    List<StoreHouse> getAllStoreHouse();

    @Query(value = "select s from StoreHouse s " +
            "where (s.storeHouseName like ?1 or s.abbreviationName like ?2) " +
            "and s.isAction = true and s.storeHouseId not in (?3)")
    List<StoreHouse> getByStoreNameN(String nameStore, String abbName,
                                     int idStoreHouse);

    @Query(value = "select s from StoreHouse s " +
            "where s.storeHouseName like :nameStore or s.abbreviationName like :abbName and s.isAction = true")
    List<StoreHouse> getByStoreName(@Param("nameStore") String nameStore, @Param("abbName") String abbName);

    @Query(value = "select  sh from StoreHouse sh where sh.storeHouseId  = ?1 and sh.isAction = true ")
    Optional<StoreHouse> findByStoreHouseId(int idStorehouse);

    @Query(value = "select sh from StoreHouse sh where sh.isAction = true and sh.storeHouseId = ?1")
    Optional<StoreHouse> getByIdStoreHouse(int idStoreHouse);

    @Modifying
    @Transactional
    @Query(value = "update StoreHouse sh set sh.isAction = false where sh.storeHouseId = ?1")
    void deleteByIdStoreHouse(int idStoreHouse);

    @Query(value = "select s from StoreHouse s where s.branchId = ?1 ")
    List<StoreHouse> getStoreHouseByIdBranch(int idBranch);

    @Query(value = "select count(*) from StoreHouse sh " +
            "join Inventory i on sh.StoreHouseId = i.StoreHouseId " +
            "where i.Status = 1 ", nativeQuery = true)
    int getRequestInventory();

    //medicine in store
    @Query(value = "select sp.ProductID, sp.StoreHouseId from StoreHouse_Product sp " +
            "where sp.StoreHouseId = ?2 and sp.ProductID = ?1", nativeQuery = true)
    IProductStore getProductInStore(int productId, int idStoreHouse);

    @Transactional
    @Modifying
    @Query(value = "insert StoreHouse_Product (ProductID, StoreHouseId) " +
            "values (?1,?2)", nativeQuery = true)
    void insertStore_Product(int productId, int idStoreHouse);

//    @Transactional
//    @Modifying
//    @Query(value = "update StoreHouse_Medicine set Amount = ?3 " +
//            "where MedicineId = ?1 and StoreHouseId = ?2", nativeQuery = true)
//    void updateMedicineInStore(int idMedicine, int idStoreHouse, int amount);

    @Query(value = "select m.MedicineName, shm.Amount, m.MedicineId, m.UnitId, u.UnitName " +
            "from StoreHouse sh " +
            "join StoreHouse_Medicine shm on sh.StoreHouseId = shm.StoreHouseId " +
            "join Medicine m on shm.MedicineId = m.MedicineId " +
            "join Unit u on u.UnitId = m.UnitId " +
            "where sh.StoreHouseId = ?1 and sh.Branchid = ?2", nativeQuery = true)
    List<IMedicineStoreResponse> getMedicineByIdStoreHouse(int idStoreHouse, int idBranch);


    //equipment in store
    @Query(value = "select se.StoreHouseId, se.EquipmentId, se.Amount, se.ReceiptId " +
            "from StoreHouse_Equipment se " +
            "where se.EquipmentId = ?1 and se.StoreHouseId = ?2 and se.ReceiptId = ?3", nativeQuery = true)
    IEquipmentStore getEquipmentInStore(int idEquipment, int idStoreHouse, int idReceipt);

    @Transactional
    @Modifying
    @Query(value = "insert StoreHouse_Equipment (EquipmentId,StoreHouseId,Amount,ReceiptId) " +
            "values (?1,?2,?3,?4)", nativeQuery = true)
    void insertEquipmentInStore(int idEquipment, int idStoreHouse, int amount, int idReceipt);

//    @Transactional
//    @Modifying
//    @Query(value = "update StoreHouse_Equipment set Amount = ?3 " +
//            "where StoreHouseId = ?2 and EquipmentId = ?1 ", nativeQuery = true)
//    void updateEquipmentInStore(int idEquipment, int idStoreHouse, int amount);

    @Query(value = "select e.EquipmentId, e.Name as EquipmentName, se.Amount, e.UnitId, u.UnitName " +
            "from Equipment e " +
            "join StoreHouse_Equipment se on e.EquipmentId = se.EquipmentId " +
            "join StoreHouse sh on sh.StoreHouseId = se.StoreHouseId " +
            "join Unit u on u.UnitId = e.UnitId " +
            "where sh.StoreHouseId = ?1 and sh.Branchid = ?2", nativeQuery = true)
    List<IEquipmentStoreResponse> getEquipmentByIdStoreHouse(int idStoreHouse, int idBranch);


    //Export

    //delete, update amount medicine
    @Transactional
    @Modifying
    @Query(value = "delete StoreHouse_Medicine " +
            "where MedicineId = ?1 and StoreHouseId =  ?2 and ReceiptId = ?3 ", nativeQuery = true)
    void deleteMedicineInStore(int idMedicine, int idStore, int idReceipt);

    @Transactional
    @Modifying
    @Query(value = "update StoreHouse_Medicine set Amount = ?1 " +
            "where MedicineId = ?2 and StoreHouseId =  ?3 and ReceiptId = ?4 ", nativeQuery = true)
    void updateMedicineInStore(int amount, int idMedicine, int idStore, int idReceipt);

    //delete . update amount equipment
    @Transactional
    @Modifying
    @Query(value = "delete StoreHouse_Equipment " +
            "where StoreHouseId = ?1 and ReceiptId = ?2 and EquipmentId = ?3", nativeQuery = true)
    void deleteEquipmentInStore(int idStore, int idReceipt, int idEquipment);

    @Transactional
    @Modifying
    @Query(value = "update StoreHouse_Equipment set Amount = ?1 " +
            "where StoreHouseId = ?2 and ReceiptId = ?3 and EquipmentId = ?4", nativeQuery = true)
    void updateEquipmentInStore(int amount, int idStore, int idReceipt, int idEquipment);


}
