package fpt.edu.capstone.repository;

import fpt.edu.capstone.entity.UnitGroup;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

public interface UnitGroupRepository extends JpaRepository<UnitGroup, Integer> {
    @Query(value = "select u from UnitGroup u where u.isUse = true and u.name like :name")
    UnitGroup findByName(@Param("name") String name);
    @Query(value = "select ug from UnitGroup ug where ug.isUse = true ")
    List<UnitGroup> getAllUnitGroup();

    @Transactional
    @Modifying
    @Query(value = "update UnitGroup set isUse = false where unitGroupId = ?1")
    void deleteUnitGroupById(int idUnitGroup);
}
