package fpt.edu.capstone.repository;

import fpt.edu.capstone.entity.Unit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

public interface UnitRepository extends JpaRepository<Unit, Integer> {
    @Query("select u from Unit u where u.unitName like ?1 and u.isUse = true ")
    Optional<Unit> findByUnitNameAndUse(String name, boolean isUse);

    @Query(value = "select u from Unit u where  u.isUse = true ")
    List<Unit> getAllUnit();

    @Transactional
    @Modifying
    @Query(value = "update Unit set isUse = false where unitId = ?1")
    void deleteUnitById(int idUnit);

    @Query(value = "select u from Unit u where u.isUse = true and u.unitId = ?1")
    Optional<Unit> findByUnitId(int idUnit);

    @Query(value = "select u from Unit u where u.unitGroupId = ?1 and u.isUse = true")
    List<Unit> getUnitByUnitGroupId(int idUnitGroup);
}
