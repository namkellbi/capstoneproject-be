package fpt.edu.capstone.repository;

import com.amazonaws.services.dynamodbv2.xspec.B;
import fpt.edu.capstone.entity.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer> {
    @Query(value = "select * from Branch where isAction = ?1",nativeQuery = true)
    List<Branch> findByIsAction(boolean isAction);

    @Query(value = "select b from Branch b where b.isAction = true and b.branchId =?1")
    Branch findBranchById(int idBranch);

    @Query(value = "select b from Branch b " +
            " where b.isAction = true and (lower(b.branchName) like lower(concat('%' , :branchName, '%')) or :branchName is null or :branchName like '' )" )
    List<Branch> findBranchByBranchName(@Param("branchName") String branchName);
}
