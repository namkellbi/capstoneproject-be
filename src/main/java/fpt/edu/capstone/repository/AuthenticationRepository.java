package fpt.edu.capstone.repository;

import fpt.edu.capstone.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AuthenticationRepository extends JpaRepository<Account,Integer> {
    Optional<Account> findAccountsByUsernameAndPassword(String username, String password);

    Optional<Account> findAccountsByUsername(String username);
    Account findAccountByUsername(String username);
    Account findAccountByAccountId(int accountId);
}
