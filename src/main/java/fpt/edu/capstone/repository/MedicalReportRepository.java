package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.consulting.IConsultingSTT;
import fpt.edu.capstone.dto.patientInConsulitng.MedicalReport_Consulting;
import fpt.edu.capstone.dto.report.*;
import fpt.edu.capstone.entity.MedicalReport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface MedicalReportRepository extends JpaRepository<MedicalReport, Integer> {
    //    List<MedicalReport> findMedicalReportsByFinishedExamination(boolean isFinished);
    @Query(value = "select top 1 * from MedicalReport where AccountId = ?1 " +
            "and IsFinishedExamination = 0 " +
            "order by MedicalReportId desc ", nativeQuery = true)
    MedicalReport findMedicalReportNewest(int idAccount);

    @Query(value = "select top 1 * from MedicalReport where AccountId = ?1 " +
            "order by MedicalReportId desc ", nativeQuery = true)
    MedicalReport getMedicalReportNewest(int idAccount);

    @Query(value = "select * from MedicalReport m where m.IsFinishedExamination =:isFinish", nativeQuery = true)
    List<MedicalReport> findReportByFinishedExamination(@Param("isFinish") boolean isFinish);

    @Query(value = "select m.MedicalReportId, mc.STT, mc.ConsultingRoomId, c.RoomName, mc.StatusId, " +
            "s.name as StatusName, m.AccountId from MedicalReport m " +
            "join MedicalReport_Consulting mc on m.MedicalReportId = mc.MedicalReportId " +
            "join ConsultingRoom c on c.ConsultingRoomId = mc.ConsultingRoomId " +
            "join Status s on mc.StatusId = s.StatusId " +
            "where m.isFinishedExamination = 0 and mc.ConsultingRoomId = ?1 " +
            "and mc.StatusId ! = ?2 and c.BranchId = ?3 order by mc.STT asc", nativeQuery = true)
    List<IConsultingSTT> findByFinishedExaminationAndConsultingRoomId(int consultingId, int status, int idBranch);
    @Query(value = "select m.MedicalReportId, mc.STT, mc.ConsultingRoomId, c.RoomName, mc.StatusId, " +
            "s.name as StatusName, m.AccountId from MedicalReport m " +
            "join MedicalReport_Consulting mc on m.MedicalReportId = mc.MedicalReportId " +
            "join ConsultingRoom c on c.ConsultingRoomId = mc.ConsultingRoomId " +
            "join Status s on mc.StatusId = s.StatusId " +
            "where m.isFinishedExamination = 0 and mc.ConsultingRoomId = ?1 " +
            "and mc.StatusId = ?2 and c.BranchId = ?3 order by mc.STT asc", nativeQuery = true)
    List<IConsultingSTT> findByFinishedExaminationAndConsultingRoomIdAndStatus(int consultingId, int status, int idBranch);

    @Query(value = "select * from MedicalReport_Consulting where MedicalReportId = ?1", nativeQuery = true)
    MedicalReport_Consulting findReport_Consulting(int idMedicalReport);

    @Modifying
    @Transactional
    @Query(value = "Update MedicalReport set ConsultingRoomId = ?1" +
            " where MedicalRecordId = ?2 ", nativeQuery = true)
    void updateConsultingId(int consultingRoomId, int medicalRecordId);

    @Modifying
    @Transactional
    @Query(value = "Update MedicalReport set IsFinishedExamination = 'true'" +
            "where MedicalRecordId = ?1 and AccountId = ?2", nativeQuery = true)
    void updateFinishExamination(int medicalRecordId, int accountId);

    @Modifying
    @Transactional
    @Query(value = "Update MedicalReport set IsFinishedExamination = 'true'" +
            "where MedicalReportId = ?1 and AccountId = ?2", nativeQuery = true)
    void updateFinishExamination1(int medicalReport, int accountId);

    @Query(value = "select *from MedicalReport_Consulting mc " +
            "where mc.ConsultingRoomId = ?1 and mc.MedicalReportId = ?2 " , nativeQuery = true)
    MedicalReport_Consulting existPatientInConsulting(int idConsulting, int idMedicalReport);

    @Query(value = "select *from MedicalReport_Consulting mc " +
            "where  mc.MedicalReportId = ?1 " , nativeQuery = true)
    List<MedicalReport_Consulting> existPatientInConsulting1(int idMedicalReport);

    @Transactional
    @Modifying
    @Query(value = "insert MedicalReport_Consulting (MedicalReportId,ConsultingRoomId,MedicalServiceId,StatusId,STT) " +
            "values (?1,?2,?3,?4,?5)",nativeQuery = true)
    void insertPatientInConsulting(int idMedicalReport, int idConsulting, int idService, int idStatus, int sTT);

    @Transactional
    @Modifying
    @Query(value = "update MedicalReport_Consulting " +
            "set MedicalServiceId = ?3, StatusId = ?4 ,STT = ?5 " +
            "where MedicalReportId = ?1 and ConsultingRoomId = ?2", nativeQuery = true)
    void updatePatientInConsulting(int idMedicalReport, int idConsulting, int idService, int idStatus, int sTT );
    @Transactional
    @Modifying
    @Query(value = "update MedicalReport_Consulting " +
            "set STT = ?3 " +
            "where MedicalReportId = ?1 and ConsultingRoomId = ?2", nativeQuery = true)
    void updatePatientInConsulting2(int idMedicalReport, int idConsulting, int sTT );
    @Transactional
    @Modifying
    @Query(value = "update MedicalReport_Consulting " +
            "set StatusId = ?3 " +
            "where MedicalReportId = ?1 and ConsultingRoomId = ?2", nativeQuery = true)
    void updatePatientInConsulting3(int idMedicalReport, int idConsulting, int idStatus );

    @Query(value = "select * from MedicalReport_Consulting " +
            "where ConsultingRoomId = ?1 and StatusId != ?2 " +
            "order by STT asc", nativeQuery = true)
    List<MedicalReport_Consulting> getByConsultingAndWait(int idConsulting, int idStatus);

    @Query(value = "select * from MedicalReport_Consulting " +
            "where ConsultingRoomId = ?1 and  MedicalReportId = ?2 " +
            "order by STT asc ", nativeQuery = true)
    List<MedicalReport_Consulting> getByConsultingAndIdConsulting(int idConsulting, int idReportService);
    @Query(value = "select * from MedicalReport_Consulting " +
            "where ConsultingRoomId = ?1 and StatusId = ?2 " +
            "order by STT asc", nativeQuery = true)
    List<MedicalReport_Consulting> getByConsultingAndInWait(int idConsulting, int idStatus);

    @Transactional
    @Modifying
    @Query(value = "update MedicalReport_Consulting set " +
            "StatusId = ?3 " +
            "where ConsultingRoomId = ?1 and MedicalReportId = ?2", nativeQuery = true)
    void changeStatusExamination(int idConsulting, int idMedicalReport, int idStatus);


    @Transactional
    @Modifying
    @Query(value = "delete MedicalReport_Consulting " +
            "where MedicalReportId = ?1 and ConsultingRoomId = ?2", nativeQuery = true)
    void deletePatientIndex(int idMedicalReport, int idConsulting);

    //report
    @Query(value = "select b.BranchId, b.BranchName, sum(mrs.TotalCost) as TotalCost,  MONTH(mr.Date) as Month, " +
            "YEAR(mr.Date) as Year from Branch b " +
            "join ConsultingRoom cr on b.BranchId = cr.BranchId " +
            "join MedicalService ms on ms.ConsultingRoomId = cr.ConsultingRoomId " +
            "join MedicalReport_Service mrs on mrs.MedicalServiceId = ms.MedicalServiceId " +
            "join MedicalReport mr on mr.MedicalReportId = mrs.MedicalReportId " +
            "where mrs.Ispay = 1 and Day(mr.Date) = IIF(?1 is null, Day(mr.Date), ?1) and  " +
            "Month(mr.Date) = IIF(?2 is null, Month(mr.Date), ?2) and Year(mr.Date) = ?3 and b.BranchId= ?4 " +
            "Group By b.BranchId, b.BranchName, MONTH(mr.Date), YEAR(mr.Date)", nativeQuery = true)
    List<RevenueReport> getRevenueByBranchId(Integer  day, Integer month, int year, int branchId);

//    @Query(value = "select b.BranchId, b.BranchName, sum(mrs.TotalCost) as TotalCost,  MONTH(mr.Date) as Month, " +
//            "YEAR(mr.Date) as Year from Branch b " +
//            "join ConsultingRoom cr on b.BranchId = cr.BranchId " +
//            "join MedicalService ms on ms.ConsultingRoomId = cr.ConsultingRoomId " +
//            "join MedicalReport_Service mrs on mrs.MedicalServiceId = ms.MedicalServiceId " +
//            "join MedicalReport mr on mr.MedicalReportId = mrs.MedicalReportId " +
//            "where mrs.Ispay = 1 and Month(mr.Date) = ?2 and year(mr.Date) = ?3 and b.BranchId = ?1 and day(mr.Date) = ?4 " +
//            "Group By b.BranchId, b.BranchName, MONTH(mr.Date), YEAR(mr.Date)", nativeQuery = true)
//    List<RevenueReport> getRevenueByBranchId1(int  branchId, int month, int year, int day);

    @Query(value = "select count(*) as medicalVisits from MedicalReport mr " +
            "join Invoice i on i.MedicalReportId = mr.MedicalReportId " +
            "join Branch b on i.BranchId = b.BranchId " +
            "where Day(mr.Date) = IIF(?1 is null, Day(mr.Date), ?1) and  " +
            "Month(mr.Date) = IIF(?2 is null, Month(mr.Date), ?2) and Year(mr.Date) = ?3 and b.BranchId= ?4", nativeQuery = true)
    int getMedicalVisitsInBranch(Integer  day, Integer month, int year, int branchId);
//    @Query(value = "select count(*) as medicalVisits from MedicalReport mr " +
//            "join Invoice i on i.MedicalReportId = mr.MedicalReportId " +
//            "join Branch b on i.BranchId = b.BranchId " +
//            "where Month( mr.Date) = ?2 and year(mr.Date) = ?3 and b.BranchId = ?1 and day(mr.Date) = ?4 ", nativeQuery = true)
//    int getMedicalVisitsInBranch1(int branchId, int month, int year, int day);

    @Query(value = "select count(*) as patient " +
            "from (select count(*) as quantity from MedicalReport mr " +
            "join Invoice i on i.MedicalReportId = mr.MedicalReportId " +
            "join Branch b on i.BranchId = b.BranchId " +
            "where Day(mr.Date) = IIF(?1 is null, Day(mr.Date), ?1) and " +
            "Month(mr.Date) = IIF(?2 is null, Month(mr.Date), ?2) and Year(mr.Date) = ?3 and b.BranchId= ?4 " +
            "group by mr.AccountId) as aa", nativeQuery = true)
    int getPatientExaminedInBranch(Integer  day, Integer month, int year, int branchId);
//    @Query(value = "select count(*) as patient " +
//            "from (select count(*) as quantity from MedicalReport mr " +
//            "join Invoice i on i.MedicalReportId = mr.MedicalReportId " +
//            "join Branch b on i.BranchId = b.BranchId " +
//            "where Month( mr.Date) = ?2 and year(mr.Date) = ?3 and b.BranchId = ?1 and day(mr.Date) = ?4 " +
//            "group by mr.AccountId) as aa", nativeQuery = true)
//    int getPatientExaminedInBranch1(int branchId, int month, int year, int day);

    @Query(value = "select mr.AccountId, c.FullName as PatientName,c.Address, c.Village,c.District, c.Province, " +
            "b.BranchId, b.BranchName, sum(mrs.TotalCost) as TotalCost from MedicalReport mr " +
            "join MedicalReport_Service mrs on mr.MedicalReportId = mrs.MedicalReportId " +
            "join Contact c on c.AccountId = mr.AccountId " +
            "join Invoice i on i.MedicalReportId = mr.MedicalReportId " +
            "join Branch b on i.BranchId = b.BranchId " +
            "where Day(mr.Date) = IIF(?1 is null, Day(mr.Date), ?1) and  " +
            "Month(mr.Date) = IIF(?2 is null, Month(mr.Date), ?2) and Year(mr.Date) = ?3 and b.BranchId= ?4 " +
            "group by mr.AccountId, c.FullName, b.BranchId, b.BranchName, c.Address, c.Village,c.District, c.Province", nativeQuery = true)
    List<IPatientUsedService> getTotalCostPatientUsed(Integer day, Integer month, int year, int branch);

    @Query(value = "select mr.AccountId, c.FullName as PatientName, b.BranchId, b.BranchName, mrs.TotalCost as PriceService, " +
            "ms.ServiceName, mr.Date from MedicalReport mr " +
            "join MedicalReport_Service mrs on mr.MedicalReportId = mrs.MedicalReportId " +
            "join Contact c on c.AccountId = mr.AccountId " +
            "join Invoice i on i.MedicalReportId = mr.MedicalReportId " +
            "join Branch b on i.BranchId = b.BranchId " +
            "join MedicalService ms on ms.MedicalServiceId = mrs.MedicalServiceId " +
            "where Day(mr.Date) = IIF(?1 is null, Day(mr.Date), ?1) and " +
            "Month(mr.Date) = IIF(?2 is null, Month(mr.Date), ?2) and Year(mr.Date) = ?3 " +
            "and b.BranchId= ?4 and c.AccountId = ?5",nativeQuery = true)
    List<IHistoryUsedService> getHistoryPatientUsedService(Integer day, Integer month, int year, int branch, int patientId);

    @Query(value = "select ms.MedicalServiceId, ms.ServiceName , b.BranchId, b.BranchName, " +
            "sum(mrs.TotalCost) as TotalCost, count(mrs.TotalCost) as MedicalVisits from MedicalReport mr " +
            "join MedicalReport_Service mrs on mr.MedicalReportId = mrs.MedicalReportId " +
            "join Contact c on c.AccountId = mr.AccountId " +
            "join Invoice i on i.MedicalReportId = mr.MedicalReportId " +
            "join Branch b on i.BranchId = b.BranchId " +
            "join MedicalService ms on ms.MedicalServiceId = mrs.MedicalServiceId " +
            "where Day(mr.Date) = IIF(?1 is null, Day(mr.Date), ?1) and  " +
            "Month(mr.Date) = IIF(?2 is null, Month(mr.Date), ?2) and Year(mr.Date) = ?3 and b.BranchId= ?4 " +
            "group by ms.MedicalServiceId, ms.ServiceName , b.BranchId, b.BranchName", nativeQuery = true)
    List<ITotalCostByService> getRevenueByService(Integer day, Integer month, int year, int branch);

    @Query(value = "select c.AccountId, c.FullName as PatientName,c.Address, c.Village,c.District, c.Province, " +
            " ms.MedicalServiceId, ms.ServiceName,mr.Date, mrs.TotalCost as PriceService from MedicalReport mr " +
            "join MedicalReport_Service mrs on mr.MedicalReportId = mrs.MedicalReportId " +
            "join Contact c on c.AccountId = mr.AccountId " +
            "join Invoice i on i.MedicalReportId = mr.MedicalReportId " +
            "join Branch b on i.BranchId = b.BranchId " +
            "join MedicalService ms on ms.MedicalServiceId = mrs.MedicalServiceId " +
            "where Day(mr.Date) = IIF(?1 is null, Day(mr.Date), ?1) and " +
            "Month(mr.Date) = IIF(?2 is null, Month(mr.Date), ?2) and Year(mr.Date) = ?3 " +
            "and b.BranchId= ?4 and ms.MedicalServiceId = ?5", nativeQuery = true)
    List<IPatientUsedService2> getPatientUsedService2(Integer day, Integer month, int year, int branch, int serviceId);
}
