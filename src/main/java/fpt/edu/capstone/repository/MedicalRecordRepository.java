package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.medicalrecord.IMedicalReportService;
import fpt.edu.capstone.entity.MedicalRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.Optional;
@Repository
public interface MedicalRecordRepository extends JpaRepository<MedicalRecord,Integer> {
    @Query(value = "select top 1 * from MedicalRecord mr where AccountId = ?1 order by MedicalRecordId desc", nativeQuery = true)
    MedicalRecord findMedicalRecordNewestByAccountId(int accountId);
    Optional<MedicalRecord>findTopByAccountIdOrderByMedicalRecordIdDesc(int accountId);
    @Modifying
    @Transactional
    @Query(value = "Update MedicalRecord  set note =?1 " +
            "where medicalRecordId = ?2")
    void updatePastMedicalHistory(String note, int idMedicalRecord);

    //lay nhung thong tin can thiet de add medicalReportService
    @Query(value = "select mrt.medicalReportId as medicalReportId, id.invoiceDetailId as invoiceDetailId, it.taxCode as taxCode " +
            "from MedicalRecord mrd " +
            "join MedicalReport mrt on mrd.medicalRecordId = mrt.medicalRecordId " +
            "join Invoice i on mrt.medicalReportId = i.medicalReportId " +
            "join InvoiceDetail id on i.invoiceId = id.invoiceId " +
            "join InvoiceType it on id.invoiceTypeId = it.invoiceTypeId " +
            "where mrd.medicalRecordId = :medicalRecordId ")
    IMedicalReportService findMedicalReportService(@Param("medicalRecordId") int medicalRecordId);


}
