package fpt.edu.capstone.repository;

import fpt.edu.capstone.dto.product.IProduct;
import fpt.edu.capstone.dto.product.IProduct1;
import fpt.edu.capstone.dto.product.IProductResponse;
import fpt.edu.capstone.dto.productGroup.IProductGroup;
import fpt.edu.capstone.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Transactional
    @Modifying
    @Query(value = "insert ProductGroup_Product (ProductId,ProductGroupId) " +
            "values (?1,?2)", nativeQuery = true)
    void insertProduct_ProductGroup(int idProduct, int idProductGroup);

    @Query(value = "with receipt as(select  rp2.ProductId, p.ProductName, sum(rp2.Amount) as Quantity, u.UnitName " +
            ", b.BranchName, sh.StoreHouseName from ReceiptAndIssue ri " +
            "join Create_Receipt_Product crp on ri.ReceiptIssueId = crp.ReceiptId " +
            "join Receipt_product_2 rp2 on rp2.id = crp.id " +
            "join Product p on p.ProductID = rp2.ProductId " +
            "join Unit u on u.UnitId = p.ProductID " +
            "join StoreHouse sh on sh.StoreHouseId = ri.ReceivingWarehouseID " +
            "join Branch b on b.BranchId = sh.Branchid " +
            "where ri.ReceivingWarehouseID = IIF(?1 is null,ri.ReceivingWarehouseID ,?1) and ri.NOX = 1 " +
            "and b.BranchId = IIF(?2 is null,b.BranchId ,?2) " +
            "Group By rp2.ProductId, ri.ReceivingWarehouseID, p.ProductName, u.UnitName, b.BranchName, sh.StoreHouseName), " +

            "export as(select isp.ProductId, IsNull(sum(isp.Amount),0) as Quantity " +
            "from ReceiptAndIssue ri " +
            "join Issue_Product isp on ri.ReceiptIssueId = isp.IssueId " +
            "join StoreHouse sh on sh.StoreHouseId = ri.ReceivingWarehouseID " +
            "join Branch b on b.BranchId = sh.Branchid " +
            "where ri.ReceivingWarehouseID = IIF(?1 is null,ri.ReceivingWarehouseID ,?1) and ri.NOX = 0 " +
            "and b.BranchId = IIF(?2 is null,b.BranchId ,?2) " +
            "Group By ri.ReceivingWarehouseID, isp.ProductId), " +

            "inven as (select inp.ProductId, sum(inp.AdjustmentAmount) as deviant " +
            "from Inventory i " +
            "join Inventory_Product inp on i.InventoryId = inp.InventoryId " +
            "join StoreHouse sh on sh.StoreHouseId = i.StoreHouseId " +
            "join Branch b on b.BranchId = sh.Branchid " +
            "where i.StoreHouseId = IIF(?1 is null,i.StoreHouseId ,?1) " +
            "and b.BranchId = IIF(?2 is null,b.BranchId ,?2) " +
            "Group By i.StoreHouseId, inp.ProductId), " +

            "prod as (select p.unitId as unitId, p.productId as productId, p.price as price, p.productName as productName, " +
            "p.userObject as userObject, p.using as using, u.unitName as unitName, p.note as note from Product p " +
            "join Unit u on p.unitId = u.unitId) " +
            "select  IsNull(r.Quantity-IsNull(e.Quantity,0)-IsNull(i.deviant,0),0) as Quantity, r.BranchName, r.StoreHouseName, " +
            "prod.unitId as unitId, prod.productId as productId, prod.price as price, prod.productName as productName, " +
            "prod.userObject as userObject, prod.using as using, prod.unitName as unitName, prod.note as note " +
            "from prod " +
            "left join receipt r on prod.productId = r.ProductId " +
            "left join export e on prod.productId = e.ProductId " +
            "left join inven i on i.ProductId = prod.productId", nativeQuery = true)
    List<IProductResponse> getAllProduct(Integer storeId, Integer branchId);

    @Query(value = "select p.unitId as unitId, p.productId as productId, p.price as price, p.productName as productName, " +
            "p.userObject as userObject, p.using as using, u.unitName as unitName, p.note as note from Product p " +
            "join Unit u on p.unitId = u.unitId", nativeQuery = true)
    List<IProduct1> getAllProduct1();

    @Query(value = "select pg.ProductGroupId, pg.ProductGroupName " +
            "from ProductGroup_Product pgp " +
            "join ProductGroup pg on pgp.ProductGroupId = pg.ProductGroupId " +
            "where pgp.ProductId = ?1", nativeQuery = true)
    List<IProductGroup> getProGroupByIdProduct(int idProduct);
}
