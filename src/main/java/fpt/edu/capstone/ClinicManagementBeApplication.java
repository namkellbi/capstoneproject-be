package fpt.edu.capstone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClinicManagementBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClinicManagementBeApplication.class, args);
    }

}
