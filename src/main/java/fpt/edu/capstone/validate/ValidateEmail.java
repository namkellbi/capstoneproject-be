package fpt.edu.capstone.validate;

import fpt.edu.capstone.common.ResponseMessageConstants;

import java.util.regex.Pattern;

public class ValidateEmail {
    private static final String EMAIL_PATTERN = "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,5}$";
    private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN);
    public static boolean validEmail(String email){
        if (email.isEmpty()) throw new RuntimeException(ResponseMessageConstants.EMAIL_FORMAT_INCORRECT);
        System.out.println(pattern.matcher(email).matches());
        return true;
    }
}
