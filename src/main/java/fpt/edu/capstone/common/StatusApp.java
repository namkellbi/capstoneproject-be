package fpt.edu.capstone.common;

public interface StatusApp {
    int READY = 1;
    int COMPLETE = 2;
    int CANCEL = 3;
    int WAITING = 4;
    int BEING_EXAMINATION = 5;
    int WAITING_FOR_RESULT = 6;
    boolean CASH_PAYMENT = true;
}
