package fpt.edu.capstone.common;

public interface ResponseMessageConstants {
    String SUCCESS = "MSG_SUCCESS";
    String ERROR = "MSG_ERROR";
    String DATA_NULL = "No Data";
    String PhoneNumber_NULL = "May dien so dien thoai vao";
    String NOT_EXIST_DATA = "DATA NOT FOUND";
    String DELETE_SUCCESS = "Xóa thành công";
    String DELETE_FAILED = "Xóa thất bại";
    String PRODUCT_HAS_BEEN_USE = "Nhóm sản phẩm dang được sử dụng.Không thể xóa";
    String PRODUCT_GROUP_DOES_NOT_EXIST = "Nhóm sản phẩm không tồn tại";
    String PRODUCT_DOES_NOT_EXIST = "Sản phẩm không tồn tại";

    //ACCOUNT
    String ACCOUNT_DOES_NOT_EXIST = "Tài khoản không tồn tại";
    String EMAIL_DOES_NOT_EXIST = "Account chưa tạo gmail";
    String ACCOUNT_EXIST = "Tài khoản đã tồn tại";
    String USERNAME_OR_PASSWORD_INCORRECT = "Tài khoản hoặc mật khẩu không chính xác";
    String CONFIRM_PASSWORD_WRONG = "Xác nhận mật khẩu không đúng.";

    String USERNAME_OR_PASSWORD_MUST_NOT_CONTAIN_ANY_SPACE_CHARACTERS = "Tài khoản hoặc mặt khẩu không được chứa khoảng trắng";

    //FILE
    String UPLOAD_FILE_SUCCESS = "Tải lên tệp thành công";
    String CHOOSE_UPLOAD_FILE = "Vui lòng chọn tệp tải lên";
    String UPLOAD_IMAGE_OVER_SIZE = "Hình ảnh tải lên bị quá dung lượng cho phép";
    String FILE_UPLOAD_INVALID = "Tệp bạn tải lên không đúng định dạng cho phép";
    String FILE_OVER_30MB = "Tệp bạn tải lên vượt quá 30 MB";

    //CONTACT
    String CONTACT_DOES_NOT_EXIST = "Liên hệ không tồn tại";
    String EMAIL_FORMAT_INCORRECT = "email nhập sai định dạng";

    //CONSULTING ROOM
    String CONSULTING_ROOM_DOES_NOT_EXIST = "Phòng khám không tồn tại";

    //WORK SCHEDULE
    String WORK_SCHEDULE_DOES_NOT_EXIST = "Lịch làm việc không tồn tại";

    //BRANCH
    String BRANCH_DOES_NOT_EXIST = "Cơ sở không tồn tại";

    //SERVICE
    String SERVICE_DOES_NOT_EXIST = "Dịch vụ khám không tồn tại";
    String INVOICE_AND_INVOICE_DETAIL_DOES_NOT_EXIST = "Không có invoce, invoice detail của bệnh nhân này";


    //MEDICAL RECORD
    String MEDICAL_RECORD_DOES_NOT_EXIST = "Không tìm thấy hồ sơ bệnh án";

    //PATIENT
    String PATIENT_DOSE_NOT_MEDICAL_SERVICE = "Bệnh nhân chưa có dịch vụ khám bệnh nào";

    //STORE HOUSE
    String STORE_NAME_EXIST = "Tên kho hoặc tên viết tắt kho đã tồn tại";
    String STORE_HOUSE_DOSE_NOT_EXIST = "Kho hàng không tồn tại";

    //STAFF
    String STAFF_DOSE_NOT_EXIST = "Nhân viên không tồn tại";
    String STAFF_DOSE_NOT_EXIST_IN_BRANCH = "Nhân viên không thuộc cơ sở này";

    //INVOICE_TAX_SERVICE
    int INVOICE_TAX_SERVICE = 1;
    int INVOICE_TAX_EQUIPMENT = 2;
    int INVOICE_TAX_MEDICINE = 3;
    //MEDICINE
    String PRODUCT_TYPE_NOT_EXIST = "Không có loại thuốc này";


    //UNIT GROUP
    String UNIT_GROUP_NAME_EXIST = "Tên nhóm đơn vị đã tồn tại";
    String UNIT_GROUP_DOES_NOT_EXIST = "Nhóm đơn vị không tồn tại";
    //PRODUCT TYPE
    String PRODUCT_TYPE_DOES_NOT_EXIST = "Nhóm sản phẩm không tồn tại";
    //EQUIPMENT
    String EQUIPMENT_TYPE_DOES_NOT_EXIST = "Nhóm dụng cụ không tồn tại";

    //UNIT
    String UNIT_NAME_EXIST = "Tên đơn vị đã tồn tại";
    String UNIT_DOES_NOT_EXIST = "Đơn vị không tồn tại";

    //MEDICAL REPORT SERVICE
    String MEDICAL_REPORT_SERVICE_DOES_NOT_EXIST = "Không tồn tại dịch vụ cần trả trong hóa đơn";
    //STAFF_CONSULTING_ROOM
    String STAFF_CONSULTING_ROOM_DOSE_NOT_EXIST = "Không tồn tại thông tin nhân viên ở phòng nào";

    //INVOICE
    String INVOICE_DOST_NOT_EXIST = "Hóa đơn không tồn tại";
    String PAYMENT_HAS_NOT_BEEN_COMPLETED = "Chưa hoàn thành hết hóa đơn thanh toán";

    //MEDICAL REPORT
    String MEDICAL_REPORT_DOES_NOT_EXIST = "Phiếu khám không tồn tại";

    //SUPPLIER
    String SUPPLIER_DOES_NOT_EXIST = "Nhà cung cấp không tồn tại";

    //MEDICINE
    String MEDICINE_DOES_NOT_EXIST = "Thuốc không tồn tại";
    String MEDICINE_TYPE_DOES_NOT_EXIST ="Loại thuốc không tồn tại";
    //Service


    //EQUIPMENT
    String EQUIPMENT_DOES_NOT_EXIST = "Vật tư này không tồn tại";

    //EQUIPMENT_TYPE


    //STATUS
    String STATUS_DOES_NOT_EXIST = "Không tồn taiij trạng thái này";

    //RECEIPT
    String RECEIPT_DOES_NOT_EXIST = "Phiếu nhập kho không tồn tại";
    String SERIAL_NUMBER_EXIST = "Mã lô đã tồn tại";

    //ROLE
    String NO_PERMISSTION = "Không có quyền";

    //INVENTORY
    String INVENTORY_DOES_NOT_EXIST = "Phiếu kiểm kê không tồn tại";

    //EXPORT
    String EXPORT_DOES_NOT_EXIST = "Phiếu xuất kho không tồn tại";


    //PAYMENT
    String PAYMENT_SUCCESS = "Thanh toán thành công";
    String PRICE_EQUAL_GREATER_THAN_ZERO = "Giá của gói lớn hơn hoặc bằng 0.";
    String BANNER_IMAGE_INVALID = "Ảnh banner không hợp lệ";
    String BANNER_IMAGE_DOES_NOT_EXIST = "Ảnh banner không tồn tại.";
    String BANNER_POSITION_INVALID = "Chọn một hoặc nhiều vị trí hiển thị banner.";
    String DISCOUNT_PRICE_INVALID = "Giá khuyến mãi không hợp lệ";
    String RENTAL_PACKAGE_DOES_NOT_EXIST = "Nhóm gói không tồn tại";
    String DETAIL_PACKAGE_DOES_NOT_EXIST = "Gói không tồn tại";
    String BANNER_TITLE_HAVE_ALREADY_EXISTED = "Tên gói đã tồn tại";
    String PAYMENT_DOES_NOT_EXIST = "Giao dịch không tồn tại";
    String SEND_RESET_PASSWORD_MAIL_SUCCESS = "Chúng tôi đã gửi mail làm mới mật khẩu tới email của bạn. Vui lòng kiểm tra email.";
    String SEND_EMAIL_FAIL = "Có lỗi xảy ra. Gửi email không thành công!";
    String DETAIL_PAYMENT_NOT_FOUND = "Không tìm thấy gói dịch vụ";
    String VNP_RESPONSE_CODE_07 = "Trừ tiền thành công. Giao dịch bị nghi ngờ";
    String VNP_RESPONSE_CODE_09 = "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.";
    String VNP_RESPONSE_CODE_10 = "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần";
    String VNP_RESPONSE_CODE_11 = "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.";
    String VNP_RESPONSE_CODE_12 = "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.";
    String VNP_RESPONSE_CODE_13 = "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.";
    String VNP_RESPONSE_CODE_24 = "Giao dịch không thành công do: Khách hàng hủy giao dịch";
    String VNP_RESPONSE_CODE_51 = "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch.";
    String VNP_RESPONSE_CODE_65 = "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày.";
    String VNP_RESPONSE_CODE_79 = "Giao dịch không thành công do: KH nhập sai mật khẩu thanh toán quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch";
    String VNP_RESPONSE_CODE_99 = "Các lỗi khác";
    String NO_PURCHASED_PACKAGE = "Nhà tuyển dụng chưa mua gói dịch vụ nào";


}
