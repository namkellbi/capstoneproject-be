package fpt.edu.capstone.common;

public interface RoleApp {
    int PATIENT = 1;
    int CEO = 2;
    int CASHIER = 4;
    int DOCTOR = 3;
    int WH_STAFF = 5;
    int MANAGER = 6;
}
