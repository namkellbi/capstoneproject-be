package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.report.RevenueReport;
import fpt.edu.capstone.service.ReportService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class ReportController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    private final ReportService reportService;

    @GetMapping("/revenue-report")
    public ResponseData getRevenueReportInBranch(@RequestParam(name = "branchId") int branchId,
                                                 @RequestParam(name = "month", required = false) Integer month,
                                                 @RequestParam(name = "year") int year,
                                                 @RequestParam(name = "day", required = false) Integer day) {
        try {
            List<RevenueReport> revenueReportList = reportService.getAllRevenueInBranch(day,month,year,branchId);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS, revenueReportList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/report-patient-examined")
    public ResponseData getReportPatientExaminedInBranch(@RequestParam(name = "branchId") int branchId,
                                                         @RequestParam(name = "month", required = false) Integer month,
                                                         @RequestParam(name = "year") int year,
                                                         @RequestParam(name = "day", required = false) Integer day) {
        try {

            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getPatientExamined(day,month, year, branchId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/report-medical-visits")
    public ResponseData getReportMedicalVisitsInBranch(@RequestParam(name = "branchId") int branchId,
                                                       @RequestParam(name = "month", required = false) Integer month,
                                                       @RequestParam(name = "year") int year,
                                                       @RequestParam(name = "day", required = false) Integer day) {
        try {

            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getMedicalVisits(day,month, year, branchId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "tinh chi phi thu duoc tung benh nhan da su dung dich vu trong tung (ngay, thang, nam)")
    @GetMapping("/cost-patient-used")
    public ResponseData getTotalCostPatientUsed(@RequestParam(name = "branchId") int branchId,
                                                @RequestParam(name = "month", required = false) Integer month,
                                                @RequestParam(name = "year") int year,
                                                @RequestParam(name = "day", required = false) Integer day) {
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getCostPatientUsed(day, month, year, branchId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "cac dich vu benh nhan da su dung trong tung (ngay, thang, nam) do")
    @GetMapping("/history-patient-used-service")
    public ResponseData getHistoryPatientUsedService(@RequestParam(name = "branchId") int branchId,
                                                     @RequestParam(name = "month", required = false) Integer month,
                                                     @RequestParam(name = "year") int year,
                                                     @RequestParam(name = "day", required = false) Integer day,
                                                     @RequestParam(name = "patientId") int patientId) {
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getHistoryPatientUsedService(day, month, year, branchId, patientId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "tinh doanh thu ma dich vu thu duoc trong tung (ngay, thang, nam) do")
    @GetMapping("/revenue-by-service")
    public ResponseData getHistoryPatientUsedService(@RequestParam(name = "branchId") int branchId,
                                                     @RequestParam(name = "month", required = false) Integer month,
                                                     @RequestParam(name = "year") int year,
                                                     @RequestParam(name = "day", required = false) Integer day) {
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getCostByService(day, month, year, branchId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "benh nhan nao da su dung dich vu do trong tung (ngay, thang, nam) do")
    @GetMapping("/report-patient-used-serviceId")
    public ResponseData getPatientUsedService(@RequestParam(name = "branchId") int branchId,
                                              @RequestParam(name = "month", required = false) Integer month,
                                              @RequestParam(name = "year") int year,
                                              @RequestParam(name = "day", required = false) Integer day,
                                              @RequestParam(name = "serviceId") int serviceId) {
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    reportService.getPatientUsedService(day, month, year, branchId, serviceId));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
