package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.consulting.*;
import fpt.edu.capstone.dto.service.AllServiceRequest;
import fpt.edu.capstone.entity.ConsultingRoom;
import fpt.edu.capstone.entity.MedicalService;
import fpt.edu.capstone.service.ConsultingRoomService;
import fpt.edu.capstone.service.MedicalServiceService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class ConsultingRoomController {
    private static final Logger logger = LoggerFactory.getLogger(ConsultingRoomController.class);

    private final ConsultingRoomService consultingRoomService;
    private final MedicalServiceService medicalServiceService;

    @GetMapping("/get-all-consulting")
    public ResponseData getAllConsultingRoom(@RequestParam(name = "idBranch") int idBranch) {
        try {
            List<AllConsultingResponse> list = consultingRoomService.getAllConsultingFix(idBranch);
            if (list.isEmpty() || list == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, list);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @GetMapping("/get-all-serviceByConsultingRoomId")
    public ResponseData getAllServiceByConsultingRoomId() {
        try {
            List<CheckUpResponse> listService = consultingRoomService.getAllService();
            if (listService.isEmpty() || listService == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, listService);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/get-consulting-by-id")
    public ResponseData getByRoomId(@RequestParam(name = "roomId") int roomId) {
        try {
            ConsultingRoom cr = consultingRoomService.getConsultingRoomById(roomId);
            if (cr == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, cr);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/get-consulting-by-name")
    public ResponseData getByName(@RequestParam(name = "roomName", defaultValue = StringUtils.EMPTY) String roomName) {
        try {
            List<ConsultingRoom> cr = consultingRoomService.getConsultingRoomByName(roomName);
            if (cr.isEmpty() || cr == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, cr);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PostMapping("/create-consulting")
    public ResponseData createConsulting(@RequestBody @Valid ConsultingRoomRequest consultingRoomRequest) {
        try {
            consultingRoomService.createConsultingRoom(consultingRoomRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PutMapping("/update-consulting")
    public ResponseData updateConsulting(@RequestBody ConsultingRoomResponse response) {
        try {
            consultingRoomService.updateConsultingRoom(response);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/get-consulting-by-service")
    public ResponseData getConsultingByService(@RequestParam(name = "idService") int idService) {
        try {
            List<ConsultingRoom> consultingRooms = consultingRoomService.getConsultingRoomByIdService(idService);
            if (consultingRooms.isEmpty() || consultingRooms == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, consultingRooms);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "lấy các dịch vụ bệnh nhân chưa khám theo từng phòng ở cơ sở nào")
    @GetMapping("/get-all-consulting-service")
    public ResponseData getAllConsultingAndService(@RequestParam(name = "idPatient") int idPatient,
                                                   @RequestParam(name = "idBranch") int idBranch) {
        try {
            List<ConsultingServicesResponse> consultingServicesResponses = consultingRoomService.getConsultingServices(idPatient, idBranch);
            if (consultingServicesResponses.isEmpty() || consultingServicesResponses == null) {
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, consultingServicesResponses);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "xoa phong lam  viec")
    @DeleteMapping("/delete-consulting")
    public ResponseData deleteConsultingRoom(@RequestParam(name = "idConsulting") int idConsulting) {
        try {
            consultingRoomService.deleteConsultingRoom(idConsulting);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
