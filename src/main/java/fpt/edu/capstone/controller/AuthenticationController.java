package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.account.LoginResponse;
import fpt.edu.capstone.dto.account.RegisterRequest;
import fpt.edu.capstone.entity.Account;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.service.AuthenticationService;
import fpt.edu.capstone.service.ContactService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class AuthenticationController {
    private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);
    private final ContactService contactService;
    private final AuthenticationService authenticationService;

    @PostMapping("/login")
    public ResponseData login(@RequestBody @Valid LoginRequest request){
        try {
            LoginResponse response = authenticationService.login(request);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, response);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PostMapping("/register")
    public ResponseData register(@RequestBody RegisterRequest request){
        try {

            authenticationService.register(request);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PostMapping("/forgot-password")
    @Operation(summary = "user forgot password, system will send a mail to user's email with reset password token link")
    public ResponseData processForgotPassword(@RequestParam String name) {
        try {

                authenticationService.forgotPassword(name);
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(),
                        "Chúng tôi đã gửi mail làm mới mật khẩu tới email vui lòng kiểm tra.");

        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
