package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.unit.UnitsRequest;
import fpt.edu.capstone.dto.unitgroup.UnitGroupRequest;
import fpt.edu.capstone.dto.unitgroup.UnitGroupsRequest;
import fpt.edu.capstone.dto.unitgroup.UnitRequest;
import fpt.edu.capstone.dto.unitgroup.UnitResponse;
import fpt.edu.capstone.entity.Unit;
import fpt.edu.capstone.entity.UnitGroup;
import fpt.edu.capstone.service.UnitGroupService;
import fpt.edu.capstone.service.UnitService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class UnitController {
    private static final Logger logger = LoggerFactory.getLogger(UnitController.class);

    private final UnitService unitService;

    private final UnitGroupService unitGroupService;

    /* REGION UNIT GROUP */
    @GetMapping("/get-unit-group")
    @Operation(summary = "Lấy danh sách tất cả Nhóm đơn vị")
    public ResponseData getListUnitGroup(){
        try {
            List<UnitGroup> groupList = unitGroupService.getListUnitGroup();
            if(groupList.isEmpty() || groupList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, groupList);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PostMapping("/create-unit-group")
    @Operation(summary = "Tạo nhóm đơn vị")
    public ResponseData createUnitGroup(@RequestBody @Valid UnitGroupRequest request){
        try {
            unitGroupService.createUnitGroup(request);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @DeleteMapping("/delete-unit-group")
    @Operation(summary = "xóa một nhóm đơn vị")
    public ResponseData deleteUnitGroup(@RequestBody UnitGroupsRequest unitGroupsRequest){
        try {
            unitGroupService.deleteUnitGroup(unitGroupsRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DELETE_SUCCESS);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    /* REGION UNIT */

    @GetMapping("/get-unit")
    @Operation(summary = "Lấy danh sách tất cả đơn vị")
    public ResponseData getListUnit(){
        try {
            List<UnitResponse> list = unitService.getListUnit();
            if(list.isEmpty() || list == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, list);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @DeleteMapping("/delete-unit")
    @Operation(summary = "xóa một đơn vị")
    public ResponseData deleteUnit(@RequestBody UnitsRequest unitsRequest) {
        try {
            unitService.deleteUnit(unitsRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DELETE_SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @PostMapping("/create-unit")
    @Operation(summary = "Tạo đơn vị")
    public ResponseData createUnit(@RequestBody @Valid UnitRequest request){
        try {
            unitService.createUnit(request);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/get-unit-by-id-group")
    @Operation(summary = "Lấy danh sách tất cả đơn vị")
    public ResponseData getUnitsByUnitGroupId(@RequestParam("idUnitGroup") int idUnitGroup){
        try {
            List<Unit> list = unitService.getUnitByUnitGroupId(idUnitGroup);
            if(list.isEmpty() || list == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, list);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
