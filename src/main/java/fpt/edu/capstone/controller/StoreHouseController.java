package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.entity.StoreHouse;
import fpt.edu.capstone.service.StoreHouseService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class StoreHouseController {
    public static final Logger logger = LoggerFactory.getLogger(StoreHouse.class);
    private final StoreHouseService storeHouseService;

    @PostMapping ("create-store-house")
    public ResponseData createStoreHouse(@RequestBody StoreHouse storeHouse){
        try{
            storeHouseService.createStoreHouse(storeHouse);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(),msg);
        }
    }

    @PutMapping("update-store-house")
    public ResponseData updateStoreHouse(@RequestBody StoreHouse storeHouse){
        try{
            storeHouseService.updateStoreHouse(storeHouse);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(),msg);
        }
    }

    @GetMapping ("all-store-house")
    public ResponseData getAllStoreHouse(){
        try{
            List<StoreHouse> storeHouses =  storeHouseService.getAllStoreHouse();
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS, storeHouses);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(),msg);
        }
    }

    @GetMapping ("store-house-by-id")
    public ResponseData getAllStoreHouse(@RequestParam(name = "idStoreHouse") int idStoreHouse){
        try{
            StoreHouse storeHouse =  storeHouseService.getStoreHouseById(idStoreHouse);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS, storeHouse);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(),msg);
        }
    }
    @GetMapping ("store-house-by-id-branch")
    public ResponseData getStoreHouseByIdBranch(@RequestParam(name = "idBranch") int idBranch){
        try{

            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    storeHouseService.getStoreHouseByBranchId(idBranch));
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(),msg);
        }
    }

    @DeleteMapping ("store-house-by-id")
    public ResponseData deleteStoreHouseById(@RequestParam(name = "idStoreHouse") int idStoreHouse){
        try{
            storeHouseService.deleteStore(idStoreHouse);
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(),msg);
        }
    }

}
