package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.entity.StoreHouse;
import fpt.edu.capstone.entity.Supplier;
import fpt.edu.capstone.entity.UnitGroup;
import fpt.edu.capstone.service.SupplierService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class SupplierController {
    public static final Logger logger = LoggerFactory.getLogger(StoreHouse.class);
    private final SupplierService supplierService;

    @GetMapping("/get-all-supplier")
    @Operation(summary = "lấy tất cả nhà cung cấp status true")
    public ResponseData getAllSupplier(){
        try {
            List<Supplier> suppliers = supplierService.getAllSupplier();
            if(suppliers.isEmpty() || suppliers == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, suppliers);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
}
