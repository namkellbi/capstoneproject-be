package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.inventory.IInventoryProductResponse;
import fpt.edu.capstone.dto.inventory.IInventoryResponse;
import fpt.edu.capstone.dto.inventory.InventoryProductRequest;
import fpt.edu.capstone.dto.inventory.InventoryRequest;
import fpt.edu.capstone.service.InventoryService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class InventoryController {
    private static final Logger logger = LoggerFactory.getLogger(FileController.class);
    private final InventoryService inventoryService;

    @Operation(summary = "1 tao phieu kiem ke kho")
    @PostMapping("create-inventory")
    public ResponseData createInventory(@RequestBody InventoryRequest inventoryRequest) {
        try {

            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    inventoryService.createInventory(inventoryRequest));
        } catch (Exception ex) {
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ex.getMessage());
        }
    }

    @Operation(summary = "get all inventory by id branch")
    @GetMapping("get-inventory-by-branch")
    public ResponseData getAllInventory(@RequestParam("idBranch") int idBranch) {
        List<IInventoryResponse> iInventoryResponseList = inventoryService.getAllInventory(idBranch);
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    iInventoryResponseList);
        } catch (Exception ex) {
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ex.getMessage());
        }
    }

    @Operation(summary = "get all inventory by id branch and status")
        @GetMapping("inventory-by-branch-status")
    public ResponseData getInventoriesByStatus(@RequestParam("idBranch") int idBranch,
                                               @RequestParam("idStatus") int idStatus) {
        List<IInventoryResponse> iInventoryResponseList = inventoryService.getInventoriesByStatus(idBranch, idStatus);
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    iInventoryResponseList);
        } catch (Exception ex) {
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ex.getMessage());
        }
    }

    @Operation(summary = "get inventory by idInventory status hoàn thành")
    @GetMapping("inventory-by-id")
    public ResponseData getInventoryById(@RequestParam("idInventory") int idInventory) {
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    inventoryService.getInventoryByIdInventory(idInventory));
        } catch (Exception ex) {
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ex.getMessage());
        }
    }

    @Operation(summary = "get product inventory by idInventory status hoàn thành")
    @GetMapping("inventory-product-by-id")
    public ResponseData getInventoryProduct(@RequestParam("idInventory") int idInventory) {
        try {
            List<IInventoryProductResponse> iInventoryProductResponses = inventoryService.getInventoryProduct(idInventory);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    iInventoryProductResponses);
        } catch (Exception ex) {
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ex.getMessage());
        }
    }


    @Operation(summary = "cancel inventory idInventory")
    @GetMapping("cancel-inventory")
    public ResponseData cancelInventoryById(@RequestParam("idInventory") int idInventory) {
        try {
            inventoryService.cancelInventory(idInventory);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception ex) {
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ex.getMessage());
        }
    }

    @Operation(summary = "tạo kiểm kê product và update actualQuantity product")
    @PostMapping("finish-inventory")
    public ResponseData getMedicineInStore(@RequestBody InventoryProductRequest inventoryProductRequest) {
        try {
            inventoryService.finishInventory(inventoryProductRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception ex) {
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), ex.getMessage());
        }
    }
}
