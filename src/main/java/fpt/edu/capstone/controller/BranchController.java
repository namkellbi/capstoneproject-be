package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.account.LoginRequest;
import fpt.edu.capstone.dto.account.LoginResponse;
import fpt.edu.capstone.dto.branch.BranchRequest;
import fpt.edu.capstone.entity.Branch;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.entity.Role;
import fpt.edu.capstone.service.BranchService;
import fpt.edu.capstone.service.RoleService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class BranchController {
    private static final Logger logger = LoggerFactory.getLogger(BranchController.class);
    private final BranchService branchService;
    private final RoleService roleService;
    @Operation(summary = "get all cơ sở")
    @GetMapping("/get-all-branch")
    public ResponseData getAllBranch() {
        try {
            List<Branch> branchList = branchService.getAllBranch();
            if(branchList.isEmpty() || branchList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, branchList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @Operation(summary = "get all role")
    @GetMapping("/get-all-role")
    public ResponseData getAllRole() {
        try {
            List<Role> roleList = roleService.getAllRole();
            if(roleList.isEmpty() || roleList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, roleList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }


    @Operation(summary = "get cơ sở by name")
    @GetMapping("get-branch-by-name")
    public ResponseData getBranchByName(@RequestParam(name = "branchName", defaultValue = "") String branchName) {
        try {
            List<Branch> branchList = branchService.getBranchByName(branchName);
            if(branchList.isEmpty() || branchList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS
                    , branchList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PutMapping("/deleteBranch/{id}")
    public ResponseData deleteBranch(@PathVariable(value = "id") int branchId) {
        try {
            Branch branch = branchService.deleteBranchById(branchId);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, branch);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PostMapping("/save-branch")
    public ResponseData saveBranch(@RequestBody @Valid BranchRequest branchRequest) {

        try {
            Branch branch = branchService.saveBranch(branchRequest);
            if(branch == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, branch);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PutMapping("/update-branch/{id}")
    public ResponseData updateBranch(@RequestBody @Valid BranchRequest branchRequest, @PathVariable(value = "id") int accountId) {
        try {
            Branch branch = branchService.updateBranch(accountId, branchRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, branch);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/branch/{id}")
    public ResponseData getBranchById( @PathVariable(value = "id") int idBranch) {
        try {
            Branch branch = branchService.getById(idBranch);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, branch);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }


}
