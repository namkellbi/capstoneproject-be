package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.account.ContactRequest;
import fpt.edu.capstone.dto.patient.ContactAndRecordRequest;
import fpt.edu.capstone.entity.Contact;
import fpt.edu.capstone.service.ContactService;
import fpt.edu.capstone.service.MedicalRecordService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class PatientController {
    private static final Logger logger = LoggerFactory.getLogger(PatientController.class);
    private final ContactService contactService;
    private final MedicalRecordService medicalRecordService;

    //lấy ra danh sách bệnh nhân
    @GetMapping("/patientT")
    public ResponseData getListPatientT(@RequestParam("idBranch")int idBranch) {
        try {
            List<Contact> contactList = contactService.findContactByRoleIdAndStatusT(1, idBranch);
            if(contactList.isEmpty() || contactList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contactList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/patient")
    public ResponseData getListAllPatient() {
        try {
            List<Contact> contactList = contactService.findContactByRoleId(1);
            if(contactList.isEmpty() || contactList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contactList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @GetMapping("/patientF")
    public ResponseData getListPatientF() {
        try {
            List<Contact> contactList = contactService.findContactByRoleIdAndStatusF(1);
            if(contactList.isEmpty() || contactList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contactList);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    //lấy thông tin một bệnh nhân
    @GetMapping("/patient/{id}")
    public ResponseData getPatient(@PathVariable(value = "id") int accId) {
        try {
            Contact contact = contactService.findContactByAccountId(accId);
            if( contact == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contact);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    //tạo hồ sơ cho 1 bệnh nhân
    @PostMapping("/patient")
    public ResponseData saveProfilePatient(@RequestBody @Valid ContactRequest contactRequest) {

        try {
            Contact contact = contactService.ContactRequestToContact(contactRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contact);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    //update thông tin cho một bênh nhân với id cho sẵn
    @PutMapping("/patient/{id}")
    public ResponseData updateProfilePatient(@RequestBody @Valid ContactRequest contactRequest, @PathVariable(value = "id") int accountId) {
        try {
            Contact contact = contactService.updateContact(accountId, contactRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contact);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "Update note patient")
    @PutMapping("patient-note")
    public ResponseData updateNotePatient(@RequestParam(name = "idPatient") int idPatient,
                                          @RequestParam(name = "note") String note) {
        try {
            medicalRecordService.updateNotePatient(idPatient, note);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, null);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR, e.getMessage());
        }
    }

    @Operation(summary = "lấy ra contact by name and status")
    @GetMapping("patient-by-name-and-status")
    public ResponseData getContactByNameAndStatus(@RequestParam(name = "name", defaultValue = StringUtils.EMPTY) String name
            , @RequestParam(name = "status", required = false, defaultValue = "true") boolean status){
        try{
            List<Contact> contacts = contactService.getContactByNameAndStatus(name,status);
            if(contacts.isEmpty() || contacts == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contacts);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

    @Operation(summary = "lấy ra contact by name ")
    @GetMapping("patient-by-name")
    public ResponseData getContactByName(@RequestParam(name = "name", defaultValue = StringUtils.EMPTY) String name){
        try{
            List<Contact> contacts = contactService.getContactByName(name);
            if(contacts.isEmpty() || contacts == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, contacts);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

    @Operation(summary = "update contact and medicalRecord info")
    @PutMapping("patient-examination-info")
    public ResponseData updatePatientExaminationInfo(@RequestBody ContactAndRecordRequest contactAndRecordRequest){
        try{
            contactService.updateExaminationInfo(contactAndRecordRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }

}
