package fpt.edu.capstone.controller;

import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.receipt.IReceiptHistoryResponse;
import fpt.edu.capstone.dto.receipt.ReceiptProductRequest;
import fpt.edu.capstone.dto.receipt.ReceiptRequest;
import fpt.edu.capstone.dto.receipt.UpReceiptRequest;
import fpt.edu.capstone.service.ReceiptService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class ReceiptController {
    private static final Logger logger = LoggerFactory.getLogger(ProductController.class);
    private final ReceiptService receiptService;

    @Operation(summary = "1 create receipt")
    @PostMapping("/create-receipt")
    public ResponseData createReceipt(@RequestBody ReceiptRequest receiptRequest) {
        try {
            receiptService.createReceipt(receiptRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
//    @Operation(summary = "1.2 update receipt by admin(admin update lại số luongj sản phẩm và đồng ý )")
//    @PutMapping("/receipt-by-id")
//    public ResponseData updateReceiptById(@RequestBody UpReceiptRequest receiptRequest, @RequestParam("idRole")int idRole) {
//        try {
//            receiptService.updateReceipt(receiptRequest,idRole);
//            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
//        } catch (Exception e) {
//            String msg = LogUtils.printLogStackTrace(e);
//            logger.error(msg);
//            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
//        }
//    }

    @Operation(summary = "1.3 update trạng thái hủy bỏ")
    @PutMapping("/cancel-receipt-by-id")
    public ResponseData cancelReceiptById(@RequestParam("idReceipt") int idReceipt) {
        try {
            receiptService.cancelReceipt(idReceipt);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "1.3 update trạng thái hoàn thành ")
    @PutMapping("/complete-receipt-by-id")
    public ResponseData completeReceiptById(@RequestParam("idReceipt") int idReceipt) {
        try {
            receiptService.completeReceipt(idReceipt);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "2 receipt Product (khi nhập sản phẩm)")
    @PostMapping("/receipt-products")
    public ResponseData receiptProducts(@RequestBody ReceiptProductRequest productRequest) {
        try {
            receiptService.receiptProduct(productRequest);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "3 getAll receipt by id branch")
    @GetMapping("/receipt-by-branch")
    public ResponseData receiptProducts(@RequestParam("idBranch")int idBranch) {
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    receiptService.getAllReceipt(idBranch));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
//    @Operation(summary = "3.9 get detail receipt by id receipt(khi chua)")
//    @GetMapping("/receipt-short-by-id")
//    public ResponseData getShortReceipt(@RequestParam("idReceipt")int idReceipt) {
//        try {
//            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
//                    receiptService.getShortReceiptById(idReceipt));
//        } catch (Exception e) {
//            String msg = LogUtils.printLogStackTrace(e);
//            logger.error(msg);
//            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
//        }
//    }

    @Operation(summary = "4 get detail receipt by id receipt(khi da ket thuc)")
    @GetMapping("/receipt-detail-by-id")
    public ResponseData getDetailReceipt(@RequestParam("idReceipt")int idReceipt) {
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    receiptService.getReceiptProductsById(idReceipt));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }

    @Operation(summary = "4.1 get receipt in branch by status id")
    @GetMapping("/receipt-short-by-status")
    public ResponseData getReceiptByStatus(@RequestParam("idBranch")int idBranch,
                                        @RequestParam("idStatus")int idStatus) {
        try {
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    receiptService.getReceiptByStatus(idBranch,idStatus));
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @Operation(summary = "Lịch sử các lần nhập kho trước đó")
    @GetMapping("/receipt-history-by-id")
    public ResponseData getReceiptHistory(@RequestParam("idReceipt")int idReceipt) {
        try {
            List<IReceiptHistoryResponse> iReceiptHistoryResponses = receiptService.getHistoryReceiptByIdReceipt(idReceipt);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
                    iReceiptHistoryResponses);
        } catch (Exception e) {
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }


//    @Operation(summary = "3.5 get receipt by id receipt")
//    @GetMapping("/receipt-by-id")
//    public ResponseData getReceiptByIdReceipt(@RequestParam("idReceipt")int idReceipt) {
//        try {
//            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS,
//                    receiptService.getReceiptFullDetailById(idReceipt));
//        } catch (Exception e) {
//            String msg = LogUtils.printLogStackTrace(e);
//            logger.error(msg);
//            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
//        }
//    }
}
