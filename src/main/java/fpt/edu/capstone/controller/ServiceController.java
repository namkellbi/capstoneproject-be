package fpt.edu.capstone.controller;



import com.cloudinary.utils.StringUtils;
import fpt.edu.capstone.common.ResponseMessageConstants;
import fpt.edu.capstone.dto.service.AllServiceRequest;
import fpt.edu.capstone.dto.service.ServiceRequestForCreate;
import fpt.edu.capstone.dto.service.ServiceRequestForGetAll;
import fpt.edu.capstone.dto.service.ServiceResponse;
import fpt.edu.capstone.entity.MedicalService;
import fpt.edu.capstone.service.MedicalServiceService;
import fpt.edu.capstone.utils.Enums;
import fpt.edu.capstone.utils.LogUtils;
import fpt.edu.capstone.utils.ResponseData;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@AllArgsConstructor
public class ServiceController {
    private static final Logger logger = LoggerFactory.getLogger(ServiceController.class);
    private final MedicalServiceService medicalServiceService;
    @GetMapping("service")
    public ResponseData getAllService(@RequestParam(name = "idBranch")int idBranch){
        try{
            List<MedicalService> medicalServiceList = medicalServiceService.getAllService(idBranch);
            if(medicalServiceList.isEmpty() || medicalServiceList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    medicalServiceList);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
    @GetMapping("get-all-service")
    public ResponseData getAllServiceName(){
        try{
            List<ServiceResponse> medicalServiceList = medicalServiceService.getAllServiceName();
            if(medicalServiceList.isEmpty() || medicalServiceList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    medicalServiceList);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }
    @PostMapping("get-all-service-by-listID")
    public ResponseData getAllServiceByListId(@RequestBody AllServiceRequest allServiceRequest){
        try{
            List<MedicalService> medicalServiceList = medicalServiceService.getAllMedicalServiceById(allServiceRequest);
            if(medicalServiceList.isEmpty() || medicalServiceList == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS, ResponseMessageConstants.SUCCESS,
                    medicalServiceList);
        }catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR,e.getMessage());
        }
    }



    @GetMapping("/service/{id}")
    public ResponseData getConsultingRoomByMedicalServiceId(@PathVariable(value = "id") int consultingRoomId){
        try {
            List<ServiceResponse> serviceResponses = medicalServiceService.getDataService(consultingRoomId);
            if(serviceResponses.isEmpty() || serviceResponses == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, serviceResponses);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @GetMapping("/find-service-by-Name")
    public ResponseData findServiceByName(@RequestParam(name = "name", defaultValue = StringUtils.EMPTY) String serviceName){
        try {
            List<ServiceResponse> serviceResponses = medicalServiceService.findServiceByServiceName(serviceName);
            if(serviceResponses.isEmpty() || serviceResponses == null){
                return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.DATA_NULL);
            }
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS, serviceResponses);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }
    @PostMapping("/create-service")
    public ResponseData findServiceByName(@RequestBody ServiceRequestForCreate serviceRequestForCreate){
        try {
            medicalServiceService.createService(serviceRequestForCreate);
            return new ResponseData(Enums.ResponseStatus.SUCCESS.getStatus(), ResponseMessageConstants.SUCCESS);
        } catch (Exception e){
            String msg = LogUtils.printLogStackTrace(e);
            logger.error(msg);
            return new ResponseData(Enums.ResponseStatus.ERROR.getStatus(), e.getMessage());
        }
    }


}
