package fpt.edu.capstone.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "MedicalReport")
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
public class MedicalReport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "MedicalReportId")
    private int medicalReportId;
    @Column(name = "Date")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date date;
    @Column(name = "AccountId")
    private int accountId;
    @Column(name = "IsFinishedExamination")
    private boolean isFinishedExamination;
    @Column(name = "MedicalRecordId")
    private int medicalRecordId;


}
