package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "Inventory")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Inventory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int inventoryId;
    @Column(name = "PersonInCharge")
    private Integer personInCharge;
    @Column(name = "petitioner")
    private int petitionerId;
    @Column(name = "StoreHouseId")
    private int storeHouseId;
    @Column(name = "InventoryDate")
    private Date inventoryDate;
    @Column(name = "Date")
    private Date date;
    @Column(name = "StatusId")
    private int statusId;

}
