package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "Unit")
@AllArgsConstructor
@NoArgsConstructor
public class Unit {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UnitId")
    private int unitId;
    @Column(name = "UnitName")
    private String unitName;
    @Column(name = "UnitGroupId")
    private int unitGroupId;
    @Column(name = "IsUse")
    private boolean isUse;
}
