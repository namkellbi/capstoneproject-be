package fpt.edu.capstone.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "Invoice")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Invoice {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int invoiceId;
    @Column(name = "StaffId")
    private int staffId;
    @Column(name = "MedicalReportId")
    private int medicalReportId;
    @Column(name = "TotalCost")
    private double totalCost;
    @Column(name = "Date")
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date date;
    @Column(name = "BranchId")
    private int branchId;
    @Column(name = "Status")
    private boolean status;
}
