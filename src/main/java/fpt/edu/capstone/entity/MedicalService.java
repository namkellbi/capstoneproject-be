package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "MedicalService")
@AllArgsConstructor
@NoArgsConstructor
public class MedicalService {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int medicalServiceId;
    @Column(name = "ConsultingRoomId")
    private int consultingRoomId;
    @Column(name = "ServiceName")
    private String serviceName;
    @Column(name = "Price")
    private double price;
}
