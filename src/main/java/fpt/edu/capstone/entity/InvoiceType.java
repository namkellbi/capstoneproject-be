package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "InvoiceType")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceType {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int invoiceTypeId;
    @Column(name = "TaxCode")
    private float taxCode;
    @Column(name = "ProductTypeId")
    private int productTypeId;
}
