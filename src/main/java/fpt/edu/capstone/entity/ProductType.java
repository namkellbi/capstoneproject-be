package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "ProductType")
@AllArgsConstructor
@NoArgsConstructor
public class ProductType {
    @Id
    @Column(name = "ProductTypeId")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productTypeId;
    @Column(name = "Name")
    private String name;
}
