package fpt.edu.capstone.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "InvoiceDetail")
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class InvoiceDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int invoiceDetailId;
    @Column(name = "InvoiceId")
    private int invoiceId;
    @Column(name = "InvoiceTypeId")
    private int invoiceTypeId;
}
