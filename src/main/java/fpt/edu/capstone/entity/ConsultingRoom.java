package fpt.edu.capstone.entity;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "ConsultingRoom")
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class    ConsultingRoom {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int consultingRoomId;
    @Column(name = "RoomName")
    private String roomName;
    @Column(name = "AbbreviationName")
    private String abbreviationName;
    @Column(name = "BranchId")
    private int branchId;
    @Column(name = "IsAction")
    private boolean isAction;


}
