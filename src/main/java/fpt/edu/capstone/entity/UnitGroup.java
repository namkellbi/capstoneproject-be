package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "UnitGroup")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UnitGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "UnitGroupId")
    private int unitGroupId;
    @Column(name = "Name")
    private String name;
    @Column(name = "IsUse")
    private boolean isUse;
}
