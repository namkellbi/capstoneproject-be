package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "Supplier")
@AllArgsConstructor
@NoArgsConstructor
public class Supplier {
    @Id
    @Column(name = "SupplierId")
    private int supplierId;
    @Column(name = "SupplierName")
    private String supplierName;
    @Column(name = "AbbreviationName")
    private String abbreviationName;
    @Column(name = "Address")
    private String address;
    @Column(name = "PhoneNumber")
    private String phoneNumber;
    @Column(name = "IsUse")
    private boolean isUse;
    @Column(name = "mail")
    private String mail;

}
