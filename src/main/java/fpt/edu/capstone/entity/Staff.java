package fpt.edu.capstone.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "Staff")
@AllArgsConstructor
@NoArgsConstructor
public class Staff {
    @Id
    @Column(name = "StaffId")
    private int staffId;
    @Column(name = "Avatar")
    private String avatar;
    @Column(name = "Education")
    private String education;
    @Column(name = "Certificate")
    private String certificate;
    @Column(name = "BranchId")
    private int branchId;
}
